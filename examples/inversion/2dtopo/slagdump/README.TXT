DCFEMLib->Examples->Inversion->2dTopo->Slagdump
###############################################

This example was friendly provided by the Federal Bureau of Geology and Resources (BGR) Hannover (Germany).
It was measured by Markus Furche and is one of several profiles over a slag dump.
A Wenner array with 2m was applied, the topography was measured by nivellement.
Since an apparent resistivity is not defined before, the input file contains resistances.

The inversion with standard options (bertNew2dTopo slagdump.ohm > bert.cfg) already yields satisfactory
results showing the conductive interior and a resistive hard pan.
