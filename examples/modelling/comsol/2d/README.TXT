This example is about reading in a geometry created and exported by Comsol as .mphtxt file.
Model is a fault zone in central Germany, modelled by J. Loehken.
The Python Script reads in the points and the edges and displays them.
So far there is still no export or meshing due to missing connections.