# -*- coding: utf-8 -*-
"""
Created on Mon Jan 24 20:01:31 2011

@author: Guenther.T
"""

import pygimli as pg
import pybert as pb
import numpy as np
from pygimli.meshtools import readHydrus2dMesh

doshow = False
xbound = 15.
ybound = 15.
mesh = readHydrus2dMesh('MESHTRIA.TXT')
mesh.createNeighbourInfos()

for i, c in enumerate(mesh.cells()):
    c.setMarker(i+1)

x, y, bNodes = [], [], []
for b in mesh.boundaries():
    if not b.leftCell() or not b.rightCell():
        for n in b.nodes():
            if n not in bNodes:
                bNodes.append(n)
                x.append(np.round(n.pos().x()*1e3)/1e3)
                y.append(np.round(n.pos().y()*1e3)/1e3)

x = np.array(x)
y = np.array(y)
xm = np.mean(x)
ym = np.mean(y)
angles = np.array(np.angle(x-xm+(y-ym)*1j))
ind = np.argsort(angles)
#np.plot(x[ind],y[ind],'x-')

y1b = min(y[x == min(x)])
y1t = max(y[x == min(x)])
y2b = min(y[x == max(x)])
y2t = max(y[x == max(x)])
x2t = max(x[y == max(y)])

poly = pg.Mesh(2)
for i in range(len(ind)):
    poly.createNode(bNodes[ind[i]].pos())

for id in range(0, poly.nodeCount()):
    poly.createEdge(poly.node(id), poly.node((id + 1) % poly.nodeCount()),
                    pg.MARKER_BOUND_HOMOGEN_NEUMANN)

n1 = poly.createNode(pg.RVector3(min(x) - xbound, y1t, 0.0))
n2 = poly.createNode(pg.RVector3(min(x) - xbound, y1b - ybound, 0.0))
n3 = poly.createNode(pg.RVector3(max(x) + xbound, y2b - ybound, 0.0))
n4 = poly.createNode(pg.RVector3(x2t + xbound, max(y), 0.0))
#n4 = poly.createNode(g.RVector3(max(x) + xbound, y2t, 0.0))

poly.createEdge(n1, n2, pg.MARKER_BOUND_MIXED)
poly.createEdge(n2, n3, pg.MARKER_BOUND_MIXED)
poly.createEdge(n3, n4, pg.MARKER_BOUND_MIXED)

#poly.createEdge( n1, mesh.createNodeWithCheck( g.RVector3(min(x),y1t,0.) ), g.MARKER_BOUND_HOMOGEN_NEUMANN )
#poly.createEdge( mesh.createNodeWithCheck( g.RVector3(x2t,max(y),0.) ), n4, g.MARKER_BOUND_HOMOGEN_NEUMANN )

#polyCreateEdge(n1,mesh.node(np.nonzero(ind==np.nonzero((x[ind]==min(x))&(y[ind]==y1t))[0])[0]), g.MARKER_BOUND_HOMOGEN_NEUMANN)

i1 = int(np.nonzero((x[ind] == min(x)) & (y[ind] == y1t))[0][0])
i2 = int(np.nonzero((x[ind] == x2t) & (y[ind] == max(y)))[0][0])

poly.createEdge(n1, poly.node(i1), pg.MARKER_BOUND_HOMOGEN_NEUMANN)
poly.createEdge(poly.node(i2), n4, pg.MARKER_BOUND_HOMOGEN_NEUMANN)

poly.exportAsTetgenPolyFile('out.poly')
poly.exportVTK('out.poly')
if doshow: pg.show(poly)

tri = pg.TriangleWrapper(poly)
tri.addRegionMarkerTmp(0, pg.RVector3(mesh.xmin() + 1., mesh.ymin() + 1.), -1)
tri.setSwitches('-pzeAfaq33.8')

mesh2 = pg.Mesh(2)
tri.generate(mesh2)
print(mesh2)

for cell in mesh.cells():
    mesh2.copyCell(cell)

print(mesh2)
if doshow: pg.show( mesh2 )
mesh2.save('mesh.bms')

mesh3 = mesh2.createP2()
mesh3.save('meshFor.bms')
print mesh3

vtk = pg.Mesh(2)
vtk.importVTK('resistivity.vtk')
res = np.array(vtk.exportData('Resistivity'))

nr = np.arange(len(res)) + 1
resmap = np.vstack((nr.T, res.T)).T
cellMap = pg.stdMapF_F()
for key, val in resmap:
    cellMap[key] = val

mesh3.mapCellAttributes(cellMap)

# automatic conductivity prolongation
mesh3.fillEmptyCells(mesh3.findCellByAttribute(0.0), -1.0)

data = pb.DataContainerERT('hydrus2d.shm')
f = pb.DCMultiElectrodeModelling(mesh3, data, True)
f.calculate(data)
print data('u')
data.set('r', data('u'))
data.save('hydrus2d.ohm', 'a b m n r u ', 'x z')
