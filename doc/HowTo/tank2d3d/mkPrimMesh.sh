mkdir -p mesh

LENGTH=0.60325
WIDTH=0.29845
HEIGHT=0.2159
FIRSTEL=0.1651
MESH=mesh/mesh
polyCreateCube $MESH        # a cube centered around the origin
polyTranslate -x 0.5 -z -0.5 $MESH # now z=0 is top and x=0 start
polyScale -x $LENGTH -y $WIDTH -z $HEIGHT $MESH # get dimensions
polyTranslate -x -$FIRSTEL $MESH     # so that first electrode is at x=0
datafile=tankdata.ohm
read line < $datafile
nel=${line%#*}   # remove everything after comment character
head -n $[nel + 2] < $datafile |tail -n $nel > elec.xyz # extract el.pos.
polyAddVIP -f elec.xyz -m -99 $MESH # add electrodes
polyAddVIP -x 0 -z -$HEIGHT -m -999 $MESH # current reference
polyAddVIP -x 0.3 -z -$HEIGHT -m -1000 $MESH # potential calibration
polyConvert -V -o $MESH-poly $MESH

polyRefineVIPS -r 0.001 $MESH # refine about 1/10 el. dist. towards center
tetgen -pazVACq1.2 $MESH # tetrahedralize PLC using tetgen
meshconvert -v -p -it -VBDM -o ${MESH}Prim $MESH.1 # convert in BMS/VTK/MESH format
rm $MESH.1.* $MESH.a.node # remove temporary stuff
