HowTo: Make a regular 3D mesh in inversion

Task: Use a regular 3D (FD-like) discretization for inversion with BERT
Problem: Create mixed mesh of tetraheda (inversion part) and hexahedra (outer box)

BERT 2 supports hexahedral finite element computation both naturally or by
dissecting each hexahedron into 5 or 6 tetrahedra. Whereas the first variant
is restricted to orthogonal hexahedra, the latter could be used for a deformed
regular mesh, e.g. with surface topography.
For inversion in unbounded domains, an outer box around the inversion domain
is needed to ensure the correctness of boundary conditions and thus accuracy.
If the meshes are prolongated regularly, we obtain very ugly cells with bad
numerical behaviour and have a huge number of nodes in the forward computation.
Therefore we want to prolongate the mesh with tetrahedra.

A regular mesh can be created in pygimli using built-in functions.
First, vectors are created (specify min/max/dx) and used to create a mesh:
    x = g.asvector( N.arange( xmin-dx*nb,xmax+dx*(nb+1), dx) )
    y = g.asvector( N.arange( ymin-dx*nb,ymax+dx*(nb+1), dx) )
    z = g.asvector( N.arange( - N.ceil( zmax / dx ), 1. ) * dx )
    mesh = g.Mesh()
    mesh.create3DGrid( x, y, z )

A second mesh is created by H2 refinement of the hexahedra into tetrahedra:
    mesh2 = mesh.createH2() # new mesh
    print mesh, mesh2   # print node/cell/boundary numbers

The outer mesh boundaries are set to marker 1 to be identified later.
    for b in mesh2.boundaries():
        if not ( b.leftCell() and b.rightCell() ):
            b.setMarker(1)
    mesh.save('para')
    mesh.exportVTK('para')

Finally we extract the outer boundaries of the mesh and save it as poly file:
    poly = g.Mesh()
    poly.createMeshByBoundaries( mesh2, mesh2.findBoundaryByMarker( 1 ) );
    poly.exportAsTetgenPolyFile('paraBoundary')

We now create a world (big box with FE modelling boundary markers), merge it
with the parameter outer boundary and add a hole marker in the middle  (in bash):
    polyCreateWorld -x100 -y100 -z50 world    # make big box
    polyMerge world paraBoundary worldSurface # can take a while
    polyAddVIP -x 0 -y 0 -z -0.1 worldSurface # hole marker in the middle
The PLC has now the faces of both and can be meshed with moderate quality (bash)
    tetgen -pazVACq2 worldSurface  # result will be worldSurface.1.*
    meshconvert -vBDM -it -o worldBoundary worldSurface.1 # convert
We now extract the outer surface of the resulting mesh by theirs marker -2 (py):
    worldBoundary = g.Mesh('worldBoundary.bms')
    worldPoly = g.Mesh()
    worldPoly.createMeshByBoundaries( worldBoundary, worldBoundary.findBoundaryByMarker( -2, 0 ) );
    worldPoly.exportAsTetgenPolyFile( "worldBoundary.poly" )
and obtain a triangulated surface mesh of the outer box without the inner.
Therefore we have to merge both surface meshes (bash)
    polyMerge worldBoundary paraBoundary allBoundary

Note that the latter procedure can take really long since intersections
between all face pairs have to be checked. We can avoid this by adding the
option -N to polyMerge, because we know there are no intersections.
As a side effect the nodes at the inner box boundary are doubled and have
to be removed, which we can establish by the script readmypolyfile.py:
    Poly=g.Mesh()
    f=open('allBoundary.poly','r')
    line1=f.readline()
    nnodes=int(line1.split()[0])
    nodes=[]
    for i in range(nnodes):
        pos=f.readline().split()
        p=g.RVector3(float(pos[1]),float(pos[2]),float(pos[3]))
        n=Poly.createNodeWithCheck(p)
        nodes.append(n)
    line2=f.readline()
    nfaces=int(line2.split()[0])
    for i in range(nfaces):
        bla=f.readline()
        ind=f.readline().split()
        fa=Poly.createTriangleFace(nodes[int(ind[1])],nodes[int(ind[2])],nodes[int(ind[3])],0)
        fa.setMarker( -2 ) # for outer boundary (mixed boundary conditions)
    f.close()
    Poly.exportAsTetgenPolyFile('test.poly')

The resulting test.poly can now be meshed using tetgen -Y (keep faces):
    tetgen -pazVACY -q2 test
If there are errors due to intersecting facets they can be identified using
    tetgen -d test
    meshconvert -V -it -o wrong test.1

We observed that tetgen 1.4.3 is doing a good job in contrast to 1.4.2.
Check which version you use (tetgen -h) and make sure you use 1.4.3.
If tetgen meshes correctly, we have two meshes, which have to be merged (PY)
    import pygimli as g
    Outer=g.Mesh('right.bms') # outer box (world)
    Inner=g.Mesh('para.bms')  # inner box (parameters)
    print Outer, Inner
    # set all outer cells to 1 and inner cells to 2
    for c in Outer.cells(): c.setMarker(1)
    for c in Inner.cells(): c.setMarker(2)
    # merge inner mesh into outer
    for c in Inner.cells():
            nodes = g.stdVectorNodes( )
            for i, n in enumerate( c.nodes() ):
                nodes.append( Outer.createNodeWithCheck( n.pos() ) )
            Outer.createCell( nodes, c.marker() );
    print Outer
    Outer.save('mesh')
    Outer.exportVTK('mesh')

And there we go. The resulting mesh.bms can be used for inversion.
The whole script is either in doall.sh calling the individual python files
or in makeall.sh, where the python commands are directly invoked.
