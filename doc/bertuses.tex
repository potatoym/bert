\documentclass[a4paper, DIV13]{scrartcl}
\usepackage[T1]{fontenc} 
%\usepackage{ngerman}
\usepackage[utf8x]{inputenc}
\usepackage[pdftex]{graphicx}
\usepackage{xspace}
\usepackage[colorlinks=true,citecolor=blue]{hyperref}
\usepackage{natbib}

\newcommand{\co}{CO\textsubscript{2}\xspace}

\graphicspath{{pics/}}
% Title Page
\title{The BERT reference book -- usage and development of BERT}
\author{Thomas Günther, Carsten Rücker \& Florian Wagner}
\begin{document}
\maketitle
\parindent0pt

\begin{abstract}
	This paper briefly summarizes the publications that give the background of the software package BERT (Boundless Electrical Resistivity Tomography) as well as the all applications of it under different conditions.
	Most of the publications are articles in peer-reviewed journals but some have so far only be published as extended abstracts or proceedings on international conferences.
	This summary helps BERT users find relevant publications for citing and further reading.
\end{abstract}

\section*{Main methodological development}
\subsection*{Fundamental papers}
The basis of the 3-D finite element computations on irregular tetrahedral meshes was given by \citet{RueckerGueSpi2006}, specifically the use of total and secondary potential approaches.
The comparison with synthetic models (the homogeneous half-space and a conducting half-sphere) provides hints to discretization density.
The recommendation is to use meshes that are refined at the electrodes with 1/10 of the electrode distance along with quadratic shape functions.
It defines the topographic (geometric) effect for non-trivial geometries and investigates the use of different solvers, reordering schemes and preconditioners to solve the sparse linear system of equations arising in every forward calculation.
Applications demonstrated are a measurement in a mining gallery and a 2-D line in a rugged 3D terrain of a volcano.

Along with this part 1, the main BERT reference paper was published as part 2 \citep{GuentherRueSpi2006} with the focus on 3-D topography, but shows how ERT can be done on any geometry.
It describes the triple-grid technique, i.e., to use a coarse inversion grid, a refined and prolonged forward grid and a highly refined for highest efficiency.
It demonstrates the inversion course and derives the system of equations to be solved in every iteration step and provides an efficient conjugate-gradient based solver.
The L-curve is used to determine optimum regularization strength.
This approach is demonstrated on a synthetic case and field data.
The first is a spherical void under a burial mound, the second comprised 3-D inversion of several 2-D ERT profiles over a slag heap.

The first developments have been in C++, but later on Python bindings were added so that the development is done in the higher level language.
All techniques are implemented in the library pyGIMLi \cite{RueckerGueWag2017GaG} including the C++ core to which the applications are linked.
The paper describes joint inversion of ERT with other methods and petrophysical (joint) inversion as well as fully coupled ERT inversion.
Any use and development should be done in pyGIMLi and cite the paper.

Until recently, BERT used smoothness constraints that connect only the neighboring model cells.
However, this methods makes it depending on the mesh and is limited.
\citet{jordi2018GJI} used geostatistical regularization based on correlation matrices to overcome these issues.

\subsection*{Constraints and regions}
The PhD thesis of Carsten Rücker \citep{rueckerdiss} also introduces are generalized minimization approach that is able to use both structural information and prior knowledge of the subsurface resistivity in the inversion.
Structural information can be incorporated by partially deactivating smoothness constraints at known interfaces \citep{guerue06nearsurface}.
This can be done in 2-D imaging by using seismic reflections \citep{GuentherSchMusGri2011NSG} or boreholes \citep{Bazin2013}, but also by 3-D ground-penetrating radar reflections as demonstrated by \citet{DoetschLinPes+2011JAG}.
\cite{bergmann2013combination} use 3-D seismic images to structurally constrain large-scale surface-downhole measurements of a \co storage reservoir.

The minimization approach documented by \citet{rueckerdiss} also allows for treating several regions of the subsurface individually, e.g. by using different transform functions \cite{guenther2008NSGC}.
In the easiest case, the elements of a region can be combined to one unknown as shown by \citet{FlechsigFabRue+2010} on a very sparse large-scale ERT data set.
\citet{DoetschCosGre+2010} used borehole regions to account for fluids in open boreholes that are affecting the ERT data.
A similar approach was applied by \cite{Wagner2015b} to account for different well completion materials used to backfill the annular space between electrodes and surrounding rock formation.
In a very sophisticated example, \citet{CosciaGreLin+2011} used borehole regions, but also geological regions describing the surrounding layers to image and monitor fluid conductivity within a shallow aquifer embedded in a 3-D topography.
This paper provides also the formulation for anisotropic smoothness constraints to enhance preferred directions of higher variability.
Furthermore, it describes the double-logarithmic transformation with upper and lower bounds to restrict the model parameters to a specified range that can be chosen for each region independently.
If the resistivity of a region is known accurately it can also be used as fixed region, i.e., it is considered in the forward calculation but excluded from inversion as used by \citet{Ronczka_2016} for an underwater survey.
\citet{wunderlich2018G} investigated the use of parametric constraints derived from direct push data along with structural prior information from boreholes.

\subsection*{Electrodes}
The concept of point electrodes was later extended to arbitrarily shaped electrodes by applying the so-called complete electrode model, or briefly CEM \cite{RueckerGuen2011}.
CEM includes two additional equations into the boundary value problem, i.e. the shunting of potential by a contact resistance and the integral over the injected current density.
It demonstrates that the length of electrodes can play a role if it is in the magnitude of the electrode distances (particularly true for laboratory or small-scaled measurements) and suggest using a point at 60\% of its length for approximation.
Areal (surface) electrodes are playing a minor role, also line electrodes, although the sensitivity function is affected by the electrode size.
They show how ring electrodes of borehole tools can be modeled.
Finally, they demonstrate the use of steel-cased boreholes for current injection, but also how passive conductive bodies can be modeled by CEM.

The concept of steel-cased boreholes is picked up by \citet{ronczka2015G} comparing different approaches for modeling such long-electrode ERT.
CEM is shown to be rather equivalent to the so far used conductive cell approach, however for both a large numerical effort is needed to achieve accurate results due to the length-width ratio.
They introduce a new modeling approach, the shunt electrode model (SEM), which is much more efficient for elongated thin electrodes.
They also show resolution properties of long electrode surveys and demonstrate that additional surface electrodes are beneficial.

This concept is used in the experiments by \citet{ronczka2015JoAG}, who image a shallow aquifer with both borehole and surface electrodes and do monitoring experiments tracing a shallow saltwater front.
The complete work in this project that is summarized by \citet{guenther2015} is published in the dissertation of Mathias Ronczka \citep{ronczka2016} who deploys the concept to image saltwater intrusion on a several km wide scale in 3-D.
Additionally it shows 2-D and 3-D laboratory experiments.
\cite{Wagner2015b} use the CEM concept to investiage borehole-related imaging artifacts in the context of crosshole CO\textsubscript{2} storage monitoring.


\subsection*{Induced polarization}
The first extension of the DC resistivity to inverting phase shifts measured in frequency domain was published by \cite{martin2013EJoFR} for tree tomography.
Later, \cite{guenther2016JoAG} extended the single-frequency method to the simultaneous inversion of multiple frequencies, followed by a model fit using the Cole-Cole model.
This development was made in a new Python-based SIP manager class that was documented by \citet{GuentherMarRue2016IPWS}.

Measuring IP in the time domain is far more widespread which is why later developments focused on this as well.
The first application that uses a new methodology that is not based on linearized models was presented by \citet{bazin2018NSG} for 2D investigation of black shales.
The technique was applied to 3D IP data by \citet{rossi2018NSG}.


\subsubsection*{Time-lapse inversion}
Early time-lapse applications of BERT are based on individual inversions \citep{BockRegLot+2010,GarreKoeGue+2010}.
\citet{Guenther2011GELMON} gave an overview on different approaches and applications to 1D and 2D data.
The first time-lapse inversionBERT that used a difference inversion scheme within BERT were presented by \citet{bechtold2012VZJ} and \citet{beff2013HaESS}.
In contrast to those models where the difference to a baseline model is regularized, \citet{huebner2015HaESS} and \citet{ronczka2015JoAG} constrained the time-lapse models to the preceding model.

\subsection*{Joint inversion}
Structurally coupled joint inversion goes back to the original paper of \citet{GuentherRuecker2006SAGEEP-Joint} and \citet{GuentherDluHolYa2010SAGEEP}, later examined in more detail by \citet{hellman2017JoAG} and \citet{ronczka2017SE}.
Another way of jointing two different methods is by using petrophysical constraints \cite{RueckerGueWag2017GaG}.

\subsection*{Further papers with methodological impact}

BERT uses a combination of a relative and an absolute noise level for weighting the individual data.
Knowledge on data errors (in the meaning of well data can be fitted) is crucial for choosing optimum regularization.
\citet{UdphuayGueEve+2011} demonstrate how these can be determined from statistical analysis of reciprocal data.
The reciprocal analysis was also used by \citet{GarreKoeGue+2010}, \citet{bechtold2012VZJ} and others for laboratory data.
However, it is still not frequently used for surface ERT data.

\citet{UdphuayGueEve+2011} also shows how appropriate 3-D meshes are generated in case of rough topography from elevation point and additional line information.
\cite{WagnerGuenSch2015} use massively parallelized sensitivity and model resolution calculations for a constructive experimental design algorithm that strives to find optimum electrode locations for target-focused resistivity monitoring.
In \cite{Wagner2015b}, the flexible discretization (electrodes can be placed anywere on the mesh, irrespective of node locations) is used to estimate borehole deviation simultaneously with the underlying resistivity distribution.

\cite{robbins2018JoAG}


\section*{Applications}

\subsection*{Synthetic modellings}
In the methodological papers, there are some synthetic models to prove the numerical methods and to compare to other approaches.
\citet{RueckerGueSpi2006} start with a homogeneous half-space and a half-sphere, both with analytic solutions.
Other examples are a mining gallery and a 3D volcano model.
\citet{GuentherRueSpi2006} use a synthetic burial mound with a cavity inside.

\citet{RueckerGuen2011} prove the CEM method by an ellipsoidal electrode with an analytic solution.
The examples presented are: 1) a 1x1x1\,m model tank with 3\,cm electrodes, 2) plate electrodes, 3) line electrodes, 4) a borehole casing, and 5) ring electrodes of a borehole tool.
The borehole electrode idea was later extended to the SEM model by \citet{ronczka2015G}.
Besides a comparison with other methods, they used three different models simulating salt-water intrusion: 1) a rising layer, 2) a moving block, and 3) a 3D model that is used for testing inversion and resolution.
A similar model with a thin saltwater layer was simulated by \citet{ronczka2015JoAG}.

\citet{garre2012VZJ} used results from hydraulic modelings as synthetic models to evaluate experimental design in soil moisture monitoring.

In the context of waste management, \citet{audebert2014WM} simulated a waste body with an insulating geomembrane to study its influence on the ERT data.
\citet{audebert2014JoAG} carried out inversion of a synthetic model on a waste dump to evaluate the statistical reliability of inversion results.
\citet{roedder2016JoAG} used modeling to study the effect of anisotropy on the apparent resistivity tensor.


\subsection*{Laboratory measurements}
Laboratory measurements have the special feature of Neumann boundary conditions on all boundaries.
The first BERT-based publication was on cylindrical lysimeters \citep{GarreKoeGue+2010} followed by \cite{GarreJavVan+2011}.
\citet{priegnitz2013RoSI} and \citet{FalconSuarez2016} also used cylindrical columns for monitoring purposes.
\citet{BockRegLot+2010} applied BERT for imaging heat transfer in a model tank, similarly to \citet{bechtold2012VZJ} and, \cite{Persson2015}, , \cite{Ronczka_2016}

\cite{FalconSuarez2016}

\subsection*{Cross-hole measurements} 
\citet{DoetschCosGre+2010} studied the effect of borehole fluid on cross-hole ERT.
\citet{CosciaGreLin+2011} installed 18 boreholes in a riverbed to study the interaction between river water and ground water, for which the changing conductivity and level of the Thur river was used as input \citep{CosciaLinGre+2012}.
\citet{beff2013HaESS} used a set of 8 borehole tools with ring electrodes to improve the resolution of soil moisture monitoring.

\cite{SchmidtHattenberger2011} and other GFZ?

\subsection*{Hill-slopes} 
\citet{heinckeetal09jag} applied 2D ERT along with 3D seismic refraction survey to assess slope stability at a Norwegian fjord.
\citet{leslie2013VZJ} investigated forested hill-slopes by parallel ERT profiles.
In a similar setting \citet{HuebnerHelGuen2015} characterize a small hillslope catchment and monitor moisture over half a year.
\cite{Gance_2016} permanently monitored landslides with ERT.
An active timelapse ERT infiltration monitoring has been presented by \citet{huebner2017HaESSa}.

\citet{watlet2018HaESS}


\subsection*{Trees} 
The first application of BERT for imaging trees was done by the HAWK Göttingen group \citep{bieker2010CJoFR, bieker2010AoFS} using the Geotom instrument.
\citet{GoeckeRusWei+2008WA} demonstrated the assessment of tree health by a combination of ERT and sonic tomography.
Also, \citet{guyot2013TP} used BERT to differentiate sapwood from heartwood.
\citet{martin2013EJoFR} showed the additional benefit of IP data for detecting fungus on oak.


\subsection*{Archaeology}
\citet{ullrich2008} gives an overview on the use of the ERT method in archaeology using some BERT examples.
More specifically, \citet{ullrich2009} use ERT along with IP to image ancient slag heaps.

Fischer et al. (2016), AP: doi:10.1002/arp.1542

Ullrich et al. (2016), AP, doi:10.1002/arp.1538

\subsection*{Waste dumps} 
\citet{ClementDesGue2010WM} applied BERT to monitor leachate injection on a waste dump.
Later, \citet{clement2014NSG} combined surface and crosshole ERT on the same subject.
\citet{audebert2014WM} investigated the effect of an insulating geomembrane on waste dumps.

\section*{\co monitoring}
Geological storage of supercritical \co induces a strong resistivity contrast that can be monitored electrically as demonstrated at the Ketzin pilot site in Brandenburg, Germany \citep{Bergmann2016}.
BERT is used for inversion of weekly acquired crosshole data since 2008 \citep{SchmidtHattenberger2011,Schmidt-Hattenberger2013,Schmidt-Hattenberger2014, Schmidt-Hattenberger2016}.
In addition, geoelectrical surface-downhole measurements at the site were acquired and give insights into the main migration direction of the \co plume \citep{bergmann2016fluid}.
Given that the vertical spacing of the borehole electrodes is in the order of meters, whereas the surface electrodes are spaced in the order of kilometers, the unstructured discretization and the triple-grid inversion approach provided a huge computational benefit \citep{Bergmann2012}.
The electrical \co signature was delineated more clearly using seismically-derived structural constraints \citep{bergmann2013combination}.

In the presence of hydraulic conduits, \co can potentially migrate to shallow aquifers and deteriorate groundwater quality.
\cite{auken2014JoAG} investigate the feasibility of ERT to image dissolved \co in shallow aquifers.
Based on comparative 2D and 3D inversions, \cite{commer2016IJoGGC} discuss potential imaging artifacts due to 2D model assumptions when monitoring both dissolved and supercritical \co.

\subsection*{Time-lapse}
ERT is increasingly used for monitoring processes, therefore many of the newest ERT publications use time-lapse inversion.
This holds particularly true for all laboratory publications \cite{GarreKoeGue+2010,priegnitz2013RoSI,bechtold2012VZJ,FalconSuarez2016,Persson2015,Ronczka_2016} 
but also cross-hole measurements \citep{CosciaGreLin+2011,DoetschCosGre+2010,beff2013HaESS,clement2014NSG,SchmidtHattenberger2011}.

\cite{huebner2017HaESSa}


Field-scale \cite{CosciaLinGre+2012,beff2013HaESS,ganz2014VZJ}
\cite{Garre2013}
\cite{niemeyer2017JoH}
\cite{jouen2016ESaPR,watlet2018HaESS,auken2014JoAG}

3D
\cite{commer2016IJoGGC}
\citet{huebner2017HaESSa}	



\section*{Coupled inversion}
Time-lapse electrical resistivity data is increasingly used together with process simulations in coupled inversion schemes to derive hydrological parameters of interest.
BERT is used in a variety of studies to simulate process-based electrical system responses.
\cite{MbohHuiGae2012} for example estimate three topsoil Mualem-van-Genuchten parameters in infiltration experiments by a combination of inflow and electrical resistivity measurements. 
\cite{Doetsch2013510} invert crosshole ERT data for changes in subsurface resistivity and use the retrieved information to constrain a hydrogeological model of the \co storage site at Cranfield, Mississippi, USA.
\cite{Wagner2016} use time-lapse apparent resistivity data along with reservoir pressure and \co arrival times in a fully-coupled approach to estimate possible permeability distributions of the Ketzin storage reservoir.
In most studies, process-based electrical properties are derived by Archie's Law.
\cite{TranDafHub2016a} recently show that additional incorporation of a temperature dependence can lead to an improved estimation of hydrological properties.

\subsection*{Transdimensional problems}
\cite{bievre2018JoAG}
\cite{robbins2018JoAG}


\section*{Standard near-surface investigations}
(and so-far unsorted and unseen papers)

\cite{dlugosch2014NSG},
\cite{flechsig2015IJoES},
\cite{nickschick2015IJoES},
\cite{attwa2014JoAG},
\cite{cassidy2014JoH},
\cite{jiang2015GJI},
\cite{eberle2017JoAG}
\cite{lysdahl2017NSG}


\section*{Data availability}
We always strive for reproducible science that makes it possible for everyone to redo the computations published in a scientific journal. 
Even though the correct code versions might not be available, in general the results should be reproducible.
We try to make the scripts and data available through the BERT repository hosted on \url{https://gitlab.com/resistivity-net/bert}.

\subsection*{Synthetic models}
In the folder examples/modelling there is a majority of the synthetic models.
Starting with the original papers \citet{RueckerGueSpi2006,GuentherRueSpi2006} there are scripts for the 3D modelling cases and the 3D burial mound model.
All examples from the CEM modelling paper \citet{RueckerGuen2011} are under modelling/CEM.
The SEM paper \citet{ronczka2015G} brought along some synthetic models available under modelling/SEM.

\begin{table}[htbp]
	\begin{tabular}{lll}
		Publication & Case & Folder \\
		\citet{RueckerGueSpi2006}   & 3D halfspace & ExMod/3d-halfspace-homogeneous \\
								    & Half-sphere & ExMod/gji166-part1/half-sphere \\
								    & Mining Gallery & ExMod/3d-inside-horizontal-borehole \\
								    & 3D Topography (Merapi)  &  \\
		\citet{GuentherRueSpi2006}  & Burial Mound & ExMod/3d-topography \\
		\citet{RueckerGuen2011}     & Ellipse & ExMod/CEM/ \\
								    & Model Tank & ExMod/CEM/tank  \\
								    & Plate Electrodes & ExMod/CEM/plate\_electrodes  \\
								    & Line electrodes & ExMod/CEM/line\_electrodes  \\
								    & Borehole Casing & ExMod/CEM/borehole\_electrode  \\
								    & Ring electrodes & ExMod/CEM/ring\_electrodes  \\
%		\citet{attwa2014JoAG}       & quasi-1D models \\
		\citet{ronczka2015G}        & Comparison CCM/CEM/SEM & \\
		                            & Rising layer & \\
		                            & Moving block & \\
		                            & 3D model & \\
		                            & Moving block & \\
		\cite{guenther2016JoAG}     & 2D SIP slag heap
	\end{tabular}
	\caption{Synthetic models.}
\end{table}

\subsection*{Field data}
The folder examples/inversion originally contained the data files treated in the tutorial.
However, the sorting into different cases easily allows for adding other data here as well.
Additionally, there is a folder HowTo that explains more complex tasks.
Some field data cannot be openly published due to copyright issues or ongoing publications.
However, they may be available from the contact author upon private request.

\begin{table}[htbp]
	\begin{tabular}{lll}
		Publication & Case & Folder \\
		\citet{GuentherRueSpi2006} & Slagdump & ExInv/2dtopo/slagdump \\ 
		                           &           & ExInv/3dtopo/slagdump \\
		\cite{heinckeetal09jag}  & 2dtopo & ?? \\
		\cite{UdphuayGueEve+2011} & Steep 3D topography & \\
		
		\cite{bechtold2012VZJ} & Model tank & \\
		\cite{martin2013EJoFR} & Oak trees & ?ExInv \\
		\cite{attwa2014JoAG}   & \\
		\citet{ronczka2015JoAG} \\
		
	\end{tabular}
	\caption{Field data}
\end{table}

A map of the field data locations?

%GuentherRueSpi2006, RueckerGueSpi2006, GoeckerRusWei+2008WA, BockRegLot+2010, ClementDesGue2010WM, DoetschCosGre+2010, FlechsigFabRue+2010, GarreKoeGue+2010, heinckeetal09jag, rueckerdiss, CosciaGreLin+2011, GarreJavVan+2011, RueckerGuen2011, SchmidtHattenberger2011, UdphuayGueEve+2011, bechtold2012VZJ, CosciaLinGre+2012, DoetschLinPes+2011JAG, garre2012VZJ, MbohHuiGae2012, Bazin2013, Doetsch2013510, Garré2013, martin2013EJoFR, priegnitz2013RoSI, beff2013HaESS, leslie2013VZJ, attwa2014JoAG, audebert2014WM, audebert2014JoAG, Auken201431, dlugosch2014NSG, cassidy2014JoH, clement2014NSG, ganz2014VZJ, flechsig2015IJoES, huebner2015HaESS, jiang2015GJI, Persson2015, ronczka2015G, ronczka2015JoAG, WagnerGuenSch2015, guenther2015, nickschick2015IJoES, priegnitz2015GJI, FalconSuarez2016, guenther2016JoAG, TranDafHub2016a, Ronczka_2016, roedder2016JoAG, schmidt-hattenberger2016IJoGGC, commer2016IJoGGC, Gance_2016, EberleBasEbe2017, eberle2017JoAG, niemeyer2017JoH


%Not seen yet:
% Fischer et al. (2016), AP: doi:10.1002/arp.1542
% Ullrich et al. (2016), AP, doi:10.1002/arp.1538
% Horna et al. (2015), ZDGG
% Ulrich et al. (2015), JoH
% Viero et al. (2015), Natural Hazards, doi:10.1007/s11069-015-1777-8
% Carriere et al. (2015), EES, doi:10.1007/978-3-642-17435-3_6


\bibliographystyle{apalike}
\bibliography{bert}


\end{document}