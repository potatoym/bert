Todo List for BERT tutorial

Add relevant literature:
	Doetsch et al. (2010) ==> borehole, 3dxh
	Coscia et al. (2011) ==> regions, definition of zweight
	Flechsig et al. (2010) ==> regions
	Udphuay et al. (2011) ==> mesh building, error model
	Kuras et al. (2009) ==> 2dxh (ALERT), timelapse
	Heincke ??
	
	for How-To:
	Garre et al. (2012) ==> Hydrus 

1. Gallery: Basic concepts, unstructured meshes and regularization

2. 2DXH: Different meshes (node,free,reg), Prior information, Regions, Timelapse