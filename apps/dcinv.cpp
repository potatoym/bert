/***************************************************************************
 *   Copyright (C) 2006-2017 by the resistivity.net development team       *
 *   Carsten Rücker carsten@resistivity.net                                *
 *   Thomas Günther thomas@resistivity.net                                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include <bert.h>

#include <optionmap.h>
#include <regionManager.h>
#include <inversion.h>
#include <shape.h>
#include <stopwatch.h>

#include <string>
using namespace GIMLI;

#define vcout if (verbose) std::cout
#define dcout if (debug) std::cout
#define DEBUG if (debug)
#define SAVEALOT if (dosave)

#include <memwatch.h>

//** relative root mean square considering only errors < tolerance
//** to be moved to GIMLi (only for output)
double rrmswitherr(const RVector & a, const RVector & b, const RVector & err){
    double errtol = 10.0;
    IndexArray fi = find(err < errtol);
    RVector a1 = a(fi);
    RVector b1 = b(fi);
    return rrms(a1, b1);
}

int main(int argc, char *argv []) {

    /*! Regularization options; */
    bool lambdaOpt = false, optimizeChi1 = false, localRegularization = false;
    /*! behaviour options; */
    bool isNotReference = false, versionOnly_ = false, useDCFEMLib = false;
    bool isBlocky = false, isRobust = false, overrideError = false, setBackground = false;
    /*! forward options */
    bool singRemoval = false, omitRefineSecMesh = false, p2Sec = false, linearData = false;
    /*! Jacobian options; */
    bool loadSensMat = false, recalcJac = false, doBroydenUpdate = false,  sensOnly_ = false;
    /*! timelapse options */
    bool fullTimelapse = false, fullTLAbsModel = false, fullTLStepModel = false;
    bool fastTimeLapse = false, timeLapseRemoveMisfit = false;

    double lambda = -1.0, lbound = 0.0, ubound = 0.0, zWeight = 1.0, lambdaTimeLapse = 0.0, lambdaIP = -1;
    double errPerc = 3.0, errVolt = 100e-6, defaultCurrent = 100e-3, lambdaDecrease = 1.0, ipAbsErr = 1.0;
    int maxIter = 20, verboseCount = 0, constraintType = 1, timeLapseConstraint = 1;
    int threadCount = 0;

    std::string startModelFilename(NOT_DEFINED), paraMeshFilename(NOT_DEFINED), primPotFileBody(NOT_DEFINED);
    std::string timeLapseFilename(NOT_DEFINED), sensFileName("Sens.mat"), dataFileName;
    std::string resolutionFileName(NOT_DEFINED);
    std::string regionControlFileName(NOT_DEFINED), timeLapseRegionFile (NOT_DEFINED);

    OptionMap oMap;
    oMap.setDescription("Description. DCInv - Inversion of dc resistivity data using BERT\n");
    oMap.addLastArg(dataFileName, "Data file");
    oMap.add(verboseCount,      "v" , "verbose", "Verbose mode.");
    oMap.add(useDCFEMLib,       "1" , "useDCFEMLib", "Use DCFEMLib1::BERT stuff (mesh, sens, primpot).");
    oMap.add(lambdaOpt,         "O" , "OptimizeLambda", "Optimize model smoothness using L-curve.");
    oMap.add(optimizeChi1,      "C" , "OptimizeChi1", "Optimize lambda subject to chi^2=1.");
    oMap.add(overrideError,     "E" , "OverrideError", "Override error given in data file.");
    oMap.add(loadSensMat,       "L" , "LoadSensMatrix", "Load sensitivity matrix (see also -j).");
    oMap.add(isRobust,          "R" , "RobustData", "Robust (L1) data weighting.");
    oMap.add(isBlocky,          "B" , "BlockyModel", "Blocky (L1) model constraints.");
    oMap.add(fullTimelapse,     "T" , "FullTimelapse", "Do full minimization for timelapse.");
    oMap.add(singRemoval,       "S" , "SingularityRemoval", "Use singularity removal (see also -y).");
    oMap.add(recalcJac,         "J" , "RecalcJacobian", "Re-Calculate Sensitivity after each iteration.");
    oMap.add(p2Sec,             "P" , "p2SecMesh", "Use quadratic mesh for forward calculation");
    oMap.add(lambda,            "l:" , "lambda", "Regularization parameter lambda.");
    oMap.add(zWeight,           "z:" , "zWeight", "Weight for vertical smoothness (1=isotrope).");
    oMap.add(constraintType,    "c:" , "constraintType", "Constraint type (0,1,2,10,20).");
    oMap.add(lbound,            "b:" , "lowerBound", "Lower resistivity bound.");
    oMap.add(ubound,            "u:" , "upperBound", "Upper resistivity bound.");
    oMap.add(errPerc,           "e:" , "error", "Percentage error part for -E (see also -x).");
    oMap.add(errVolt,           "x:" , "errorVoltage", "Voltage error part for -X (see also -e).");
    oMap.add(maxIter,           "i:" , "iterations", "Maximum iteration number.");
    oMap.add(sensFileName,      "j:" , "sensFile", "Sensitivity matrix file.");
    oMap.add(paraMeshFilename,  "p:" , "paraMeshFile", "Parameter mesh file.");
    oMap.add(startModelFilename,"s:" , "startModelFile", "Starting model file or global start model resistivity");
    oMap.add(primPotFileBody,   "y:" , "primaryPotFileBody", "File name (without .#) of primary potentials.");
    oMap.add(resolutionFileName,"r:" , "resolutionFile", "File containing resolution model indices or positions.");
    oMap.add(regionControlFileName,  "f:" , "regionControl", "File containing region control.");
    // long-options only (--keyword or --keyword=value)
    oMap.add(versionOnly_,         ""   , "versionLocal", "Show the current version.");
    oMap.add(sensOnly_,            ""   , "sensOnly", "Just calculate and save sensitivity matrix for given mesh and data.");
    oMap.add(localRegularization,  ""   , "localRegularization", "force local regularization");
    oMap.add(isNotReference,       ""   , "isNotReference", "Use the startmodel not as reference model");
    oMap.add(omitRefineSecMesh,    ""   , "omitSecRefine", "Do not refine mesh for forward calculation (only -S)");
    oMap.add(lambdaDecrease,       ":"  , "lambdaDecrease", "factor to decrease lambda in every iteration");
    oMap.add(linearData,           ""   , "linearData", "Linear data transformation.");
    oMap.add(lambdaIP,      ":"  , "lambdaIP", "IP regularization parameter.");
    // timelapse options
    oMap.add(timeLapseFilename,    "t:" , "timeLapseFile", "Time lapse data file");
    oMap.add(timeLapseConstraint,  "q:" , "timeLapseConstraint", "Constraint type for timelapse inversion (0,1,2,10,20).");
    oMap.add(lambdaTimeLapse,      ":"  , "lambdaTimeLapse", "Timelapse regularization parameter.");
    oMap.add(timeLapseRegionFile,  ":"  , "timeLapseRegionFile", "region file for timelapse behaviour");
    oMap.add(fastTimeLapse,        ""   , "fastTimeLapse", "Improve speed for timelapse");
    oMap.add(timeLapseRemoveMisfit,""   , "timeLapseRemoveMisfit", "Remove misfit of t=0 for t>0");
    oMap.add(fullTLAbsModel,       ""   , "fullTimeLapseAbsModel", "Force absolute model calculation during full time lapse");
    oMap.add(fullTLStepModel,      ""   , "fullTimeLapseStepModel", "Constrain against last step model during full time lapse");
    oMap.add(threadCount,          "n:" , "threadCount", "Maximum threads to use");
    oMap.parse(argc, argv);

    if (versionOnly_) {
        std::cout << "GIMLi version " << GIMLI::versionStr() << std::endl;
        return EXIT_SUCCESS;
    }
    bool verbose = (verboseCount > 0), debug = (verboseCount > 1), dosave = (verboseCount > 2);

    if (verbose) setbuf(stdout, NULL);

    if (useDCFEMLib) { //! for working with BERT1 files (sensitivities&potentials)
        if (paraMeshFilename == NOT_DEFINED) paraMeshFilename = "mesh/mesh.bms"; //! use 1/2 mesh for inversion (background region + para region)
        if (sensFileName == "Sens.mat") sensFileName = "sensM/smatrix";
        if (primPotFileBody == NOT_DEFINED) primPotFileBody = "primaryPot/interpolated/potential.P"; //! catch .D (dipole files)
        vcout << "DCFEMLib mode: trying to use " << paraMeshFilename << " , " << sensFileName << " , " << primPotFileBody << std::endl;
        loadSensMat = true;
        singRemoval = true;
        if (lambda <= 0.0 && (! lambdaOpt)) lambda = 20.0; //! only if not set and not to optimize
    }

    //!** most important: data file */
    DataContainerERT dataIn(dataFileName);
    if (verbose) dataIn.showInfos();

    //!** second necessary
    Mesh paraMesh;
    if (paraMeshFilename != NOT_DEFINED) {
        paraMesh.load(paraMeshFilename);
        vcout << "ParaMesh:\t";
        paraMesh.showInfos();
        DEBUG paraMesh.exportVTK("meshPara");
    } else {
        throwError(1, WHERE_AM_I + " no para mesh given.");
    }

MEMINFO

    //!** forward operator with or without singularity removal
    DCMultiElectrodeModelling * f;
    if (singRemoval) {
        f = new DCSRMultiElectrodeModelling(paraMesh, dataIn, debug);
        dynamic_cast< DCSRMultiElectrodeModelling * >(f)->setPrimaryPotFileBody(primPotFileBody);
    } else {
        f = new DCMultiElectrodeModelling(paraMesh, dataIn, debug);
    }
    if (threadCount > 0) f->setThreadCount(threadCount);

    //!** regularization strength;
    if (lambdaOpt && lambda <= 1000.0) lambda = 1000.0; //! only if not set
    if (lambda <= 0.0) lambda = 20.0; //! if not set by hand, default or by lambda-optimization
    if (recalcJac) doBroydenUpdate = false;

    //!** regularization: apply vertical different smoothness globally;
    if (constraintType >= 0) {
        f->regionManager().setConstraintType(constraintType);
        f->regionManager().setZWeight(zWeight);
    }

    //!** regularization: read region control file if existing;
        //!** set default behaviour for BERT 1 regions (only non-Neumann bodies)
    setBackground = (f->regionManager().regionCount() > 1 && !(f->neumann()));
    if (setBackground) f->regionManager().regions()->begin()->second->setBackground(true);

    if (regionControlFileName != NOT_DEFINED) { //** would override settings
        f->regionManager().loadMap(regionControlFileName);
    }

MEMINFO

    //!** secondary mesh for forward calculation according to -S and -P ;
    f->createRefinedForwardMesh(!omitRefineSecMesh, p2Sec);

    vcout << "Secmesh:\t"; f->mesh()->showInfos();
    DEBUG f->mesh()->save("meshSec.bms");
    DEBUG f->mesh()->exportVTK("meshSec");

    //!** auxilliary inversion mesh (for output);
    Mesh paraDomain(f->regionManager().paraDomain());
    vcout << "ParaDomain:\t";
    paraDomain.showInfos();
    paraDomain.save("meshParaDomain.bms");
    DEBUG paraDomain.exportVTK("meshParaDomain");

MEMINFO

    //!** data: check for geometric factors and data range;
    if (!dataIn.allNonZero("k")) {
        std::cout << "Did not find valid k factors. Force computation." << std::endl;
        dataIn.set("k", f->calcGeometricFactor(dataIn));
    }
    vcout << "Data min = " << min(dataIn("rhoa")) << " Ohmm max = " << max(dataIn("rhoa")) << " Ohmm" << std::endl;

    //!** data: estimate error if not already defined properly (should be done by dcedit);
    if (overrideError || !dataIn.allNonZero("err")){
        DCErrorEstimation(dataIn, errPerc, errVolt, defaultCurrent, verbose);
    }
    vcout << "Data error:" << " min = " << min(dataIn("err")) * 100 << "%" << " max = " << max(dataIn("err")) * 100 << "%" << std::endl;

    //!** starting model priorities:
    //!** median of the apparent resistivities->get from region infos-> read from file;
    double rhoStart = max(median(dataIn("rhoa")), lbound * 1.01);
    if (ubound > lbound) rhoStart = min(rhoStart, ubound *0.99);
    vcout << "Starting resistivity = " << rhoStart << " Ohmm." << std::endl;
    RVector startModel(f->regionManager().parameterCount(), rhoStart);

    if (regionControlFileName != NOT_DEFINED) {
        RVector regionStartVector(f->regionManager().createStartVector());

        //** check if startvector is unequal defaults
        if (::fabs(sum(regionStartVector) - double(regionStartVector.size())) > TOLERANCE){
            startModel = regionStartVector;
        }
    }
    if (startModelFilename != NOT_DEFINED) {
        if (fileExist(startModelFilename)){
            load(startModel, startModelFilename);
        } else {
            startModel.fill(toDouble(startModelFilename));
        }
    }
    f->setStartModel(startModel);

    //!** Define transformation functions by logLU model and log apparent resistivity;
    RTrans * dataTrans  = new RTransLog();
    RTrans * modelTrans = new RTransLogLU(lbound, ubound);

MEMINFO

    //!** Initialize inversion with data and forward operator
    RInversion inv(dataIn("rhoa"), *f, verbose, dosave);
    //!** Set options for inversion
    inv.saveModelHistory(true);           //! save model_1.vector etc. (only default for dosave mode)
    if (! linearData) inv.setTransData(*dataTrans); //! Data transformation
    inv.setTransModel(*modelTrans);       //! Model transformation
    inv.setRelativeError(dataIn("err"));   //! Error model
    inv.setMaxIter(maxIter);              //! Maximum iteration number
    inv.setLambda(lambda);                //! Regularization strength
    if (lambdaDecrease < 1.0) inv.setLambdaFactor(lambdaDecrease); //! change lambda by factor
    inv.setOptimizeLambda(lambdaOpt);     //! Optimize lambda using L-curve
    inv.setRobustData(isRobust);          //! Robust (IRLS) data reweighting
    inv.setBlockyModel(isBlocky);         //! Blocky (IRLS) constraint reweighting
    inv.setRecalcJacobian(recalcJac);     //! Recalculate Jacobain in every inversion
    inv.setBroydenUpdate(doBroydenUpdate);//! do broyden update instead
    inv.setModel(startModel);             //! starting model
    inv.setLocalRegularization(localRegularization);
    if (! isNotReference){ inv.setReferenceModel(f->startModel()); }

    //!** check whether sensmatrix can be loaded
    RMatrix S;
    if (loadSensMat) {
        try {
            load(S, sensFileName);
            f->setJacobian(&S);
            vcout << "S loaded " << std::endl;
        } catch (std::exception & e) {
            std::cout << "Standard exception: " << e.what()  << std::endl;
            // if not->proceed at else AND save matrix (-L saves)
        }
    } else if (sensOnly_){ //** macht nur Sinn ohne loadSensMat
        Stopwatch swatch(true);
        inv.checkJacobian();
        std::cout << "Senscalc: " << swatch.duration() << std::endl;
        save(f->jacobianRef(), "Sens-0.mat");
        saveVec(paraDomain.cellMarkers(), "paraDomain.idx", Ascii);
        paraDomain.addExportData("startmodel", startModel(paraDomain.cellMarkers()));
        exportSensMatrixDC("sensonly", paraDomain, f->jacobianRef(),
                           paraDomain.cellMarkers());
        return EXIT_SUCCESS;
    }

MEMINFO

    //!** Run Inversion, either by optimizing chi^2=1 or not;
    RVector model;
    if (optimizeChi1) {
        model = inv.runChi1(0.1);
    } else {
        model = inv.run();
    }

MEMINFO

    std::string commStr = "I=" + toStr(inv.iter()) + " lam=" + toStr(lambda) + " wz=" + toStr(zWeight) +
                          " rrms=" + toStr(inv.relrms()) + "% chi^2=" + toStr(inv.chi2());
    paraDomain.setCommentString(GIMLI::versionStr() + ": " + commStr);

    //!** Save and export results(resistivity lin. and log., coverage)
    RVector eModel(model(paraDomain.cellMarkers()));
    save(eModel, "resistivity");
    save(inv.response(), "response");
    DEBUG save(model, "model_iter.final");
    paraDomain.addExportData("Resistivity/Ohmm" , eModel);
    paraDomain.addExportData("Resistivity(log10)", log10(abs(eModel) + 1e-16));

    // RVector coverageVector(coverageDCtrans(*f->jacobian(), 1.0 / inv.response(), 1.0 / model));

    // if (paraDomain.cellCount() == model.size()) {
    //     coverageVector /= paraDomain.cellSizes();
    // } else {
    //     RVector modelCellSizes(coverageVector.size(), 0.0);
    //     for (size_t i = 0; i < paraDomain.cellCount(); i ++){
    //         Cell *c = &paraDomain.cell(i);
    //         modelCellSizes[c->marker()] += c->shape().domainSize();
    //     }
    //     if (min(modelCellSizes) > TOLERANCE){
    //         coverageVector /= modelCellSizes;
    //     } else {
    //         std::cout << "Coverage fails:" << paraDomain.cellCount() << " " << model.size() << std::endl;
    //     }
    // }
    // //!** export coverage
    // RVector mycoverage(log10(coverageVector(paraDomain.cellMarkers())));
    RVector mycoverage(log10(createCoverage(*f->jacobian(), 
                                            paraDomain, inv.response(), model)));
    save(mycoverage, "coverage");
    paraDomain.addData("coverage(log10)", mycoverage);

    //!** save final sensmatrix for outer computations (use -vvv);
    SAVEALOT save(*f->jacobian(), "Sens.mat");
    RVector reweighting = inv.error() / dataIn("err");
    if (isRobust) { // what the hell was my idea about that?
        save(reweighting, "reweighting");
        save(inv.error(), "final_error");
    }

    inv.saveModelHistory(false); // for IP and timelapse inversions

    //!** IP linearized inversion if ip data are present;
    if (max(dataIn("ip")) > min(dataIn("ip"))) {
        vcout << "\n*** Processing ip data ******************* " << std::endl;
        vcout << "Data:";
        vcout << " min=" << min(dataIn("ip")) << " mrad"
              << " max=" << max(dataIn("ip")) << " mrad" << std::endl;

        //! imaginary apparent resistivity
        RVector rhoaImag(abs(dataIn("rhoa") * sin(dataIn("ip") / 1000)));

        //! linear modelling operator using the amplitude jacobian
        LinearModelling fIP(paraMesh, f->jacobian(), verbose);
        RInversion invIP(rhoaImag, fIP, debug);

        RVector ipError;
        if (dataIn.haveData("iperr")) { //! phase error present
            dcout << "Using IP error given in file" << std::endl;
            ipError = abs(dataIn.get("iperr") / dataIn("ip"));
            SAVEALOT save(ipError, "ipErrorRel");
            invIP.setRelativeError(ipError);
            vcout << "ip error:";
                vcout << " min=" << min(ipError)*100 << "%"
                      << " max=" << max(ipError)*100 << "%" << std::endl;

        } else { //! default: 1 mrad
            dcout << "Estimating IP error with " << ipAbsErr << " mrad." << std::endl;
            ipError = abs(rhoaImag) / (abs(dataIn("ip")) + TOLERANCE) * ipAbsErr;
            SAVEALOT save(ipError, "ipErrorAbs");
            vcout << "ip error:";
                vcout << " min=" << min(ipError) << " mrad"
                      << " max=" << max(ipError) << " rmad" << std::endl;
            invIP.setAbsoluteError(ipError);
        }

        if (setBackground) fIP.regionManager().regions()->begin()->second->setBackground(true);
        //! IP (imaginary resistivity) inversion using fIP

        RTransLog rhoaImagTrans;
        invIP.setTransModel(rhoaImagTrans);

        invIP.setRecalcJacobian(false);
        invIP.setBlockyModel(isBlocky);
        invIP.setRobustData(isRobust);

        if (lambdaIP <= 0) lambdaIP = lambda;  //** not given explicitly
        invIP.setLambda(lambdaIP);
        //invIP.setConstraintMatrix(inv.constraintMatrix()); //** really necessary? obviously not

        invIP.stopAtChi1(false);
        invIP.setModel(RVector(model.size(), median(rhoaImag)));

        RVector modelImag(invIP.run());
        if (!debug) invIP.echoStatus();

        RVector phase = angle(modelImag, model) * 1000;
        vcout << "Resulting phase: min=" << min(phase)
              << " max=" << max(phase) << " mrad." << std::endl;

        RVector aphase = angle(invIP.response(), inv.response()) * 1000;
        save(aphase, "responsePhase");
        save(aphase, "aphase.vec");

        RVector phaseModel(phase(paraDomain.cellMarkers()));
        paraDomain.addExportData("Phase/mrad", phaseModel); //! add to vtk data
        save(phaseModel, "phase");
    } else {
        vcout << "IP min=" << min(dataIn("ip"))
              << " max=" << max(dataIn("ip")) << std::endl;
    }

    SAVEALOT for (size_t i = 0; i < inv.modelHistory().size(); i ++)
            paraDomain.addExportData("Resistivity(log10)_" + toStr(i),
                                      RVector(log10(abs(inv.modelHistory()[i]) + 1e-16)
                                             ) (paraDomain.cellMarkers()));
    paraDomain.exportVTK("dcinv.result");

    // T: soll mal ins inversion.cpp oder so (Übergabe paraDomain)
    //!** resolution from file either containing indices or positions;
    if (resolutionFileName != NOT_DEFINED) {
        std::fstream resFile;
        std::string myline;
        int num;
        RVector resolution(model.size());
        R3Vector midpoints = paraDomain.cellCenter();
        if (openInFile(resolutionFileName.c_str(), &resFile)) {
            vcout << "processing resolution file " << resolutionFileName << std::endl;
            while (! resFile.eof()) {
                std::vector< std::string > row = getNonEmptyRow(resFile);
                num = -1;
                if (row.size() > 1) { //** position given
                    RVector3 pos;
                    for (size_t i = 0 ; i < min(row.size(),(size_t)3) ; i++) pos[i] = toFloat(row[i]);
                    Cell *cell = paraDomain.findCell(pos);
                    if (cell) {
                        num = cell->marker();
                    } else {
                        vcout << "Warning: model id for position " << pos << " not found!" << std::endl;
                    }
                } else if (row.size() == 1) { //** number given
                    num = toInt(row[0]);
                    if (num >= (int)model.size()){
                        throwError(1, "No such cell index: " + str(num) + " >= " + str(model.size()));
                    }
                }
                if (num >= 0) {
                    resolution = inv.modelCellResolution(num);
                    save(resolution, "cellResolution-"  + toStr(num));
                    vcout << "resolution for cell " << num << "(" << midpoints[num] << ") "
                        << resolution.size() << "/" << resolution(paraDomain.cellMarkers()).size() << std::endl;
                    paraDomain.addExportData("cellResolution-"  + toStr(num),
                                              resolution(paraDomain.cellMarkers()));
                }
            }
            resFile.close();
            paraDomain.exportVTK("resolution");
        }
    }

    //!** Timelapse inversion - do be done more generally in another class file
    if (timeLapseFilename != NOT_DEFINED) {
        f->clearConstraints(); // warum lsch ich die?
        RVector misfit(inv.response() / dataIn("rhoa"));
        if (lambdaTimeLapse <= 0.0) lambdaTimeLapse = lambda; //default: no change
        if (timeLapseConstraint >= 0) {
            vcout << "Setting constraint type " << timeLapseConstraint << " for timelapse" << std::endl;
            f->regionManager().setConstraintType(timeLapseConstraint);
        }
        if (timeLapseRegionFile != NOT_DEFINED) { //!* region file for timelapse behaviour
            vcout << "Loading region file for time lapse " << timeLapseRegionFile << std::endl;
            f->regionManager().loadMap(timeLapseRegionFile); // does recount & createPD
            f->createRefinedForwardMesh(!omitRefineSecMesh, p2Sec);
        }
        inv.saveModelHistory(false);
//inv.setRobustData(false); //** at any rate, if -R then use reweighting
        inv.setOptimizeLambda(false);
        if (maxIter < 1) inv.setMaxIter(5);
        //!* open timelapse file and process data;
        std::string timeFileName;
        std::fstream timeFile;

        if (openInFile(timeLapseFilename.c_str(), & timeFile)) {
            uint count=0;
            uint skipTimelapsData=0;
            RVector fitVector(6); //iter, lambda, chi^2, rmsAll, rmsErr
            RMatrix fitMatrix;
            fitVector[0] = count;
            fitVector[1] = double(inv.iter());
            fitVector[2] = inv.lambda();
            fitVector[3] = inv.chi2();
            fitVector[4] = inv.relrms();
            fitVector[5] = rrmswitherr(inv.data(), inv.response(), inv.error()) * 100.0;
            fitMatrix.push_back(fitVector);
            saveMatrixRow(fitMatrix, "timelapse.log");

            vcout << "Opening timeseries file" << std::endl;
            RMatrix absoluteModel, diffModel, ipModels;
            absoluteModel.push_back(model);
            RVector response(inv.response());
            RVector newModel(model);
            RVector relModel(model);
            std::vector< std::string > row;

            while (!timeFile.eof()){

                row = getNonEmptyRow(timeFile);
                if (row.size() > 0) {
                    timeFileName = row[0];
                } else {
                    //! no more new entrys in timeLapseFilename
                    break;
                }
                if (count < skipTimelapsData) {
                    count ++;
                    continue;
                }

                DataContainerERT dataTmp;

                if (timeFileName.size() > 0) {
                    vcout << "------------------------------------------" << std::endl;
                    vcout << "Processing time step file " << timeFileName << std::endl;
                    dataTmp.load(timeFileName);
                    vcout << "data: min = " << min(dataTmp("rhoa")) << " max = " << max(dataTmp("rhoa")) << std::endl;
                    RVector error;

                    if (dataIn.size() != dataTmp.size() && ! fullTimelapse) {
                        std::cerr << "Data size differs! Deleting sensitivity matrix and force fullTimelapse" << std::endl;
                        fullTimelapse = true;
                        fastTimeLapse = false;
                    }

                    if (fullTimelapse) { //! Model-based: full inversion with reference model
                        // save and load Jacobian of zero model? Jacobian recalc vector (1 3 6)
                        if (fastTimeLapse) { // identical data sets and valid rhoa and k (check!!)
                            vcout << "Using fast timelapse option." << std::endl;
                            inv.setRecalcJacobian(false);
                            response = inv.response();
                        } else {
                            f->clearJacobian();
                            if (!dataTmp.allNonZero("k")) {
                                std::cout << "Did not find valid k factors. Force computation." << std::endl;
                                dataTmp.set("k", f->calcGeometricFactor(dataTmp));
                            }
                        }
                        vcout << "k: min = " << min(dataTmp("k")) << " max = " << max(dataTmp("k")) << std::endl;
                        f->setData(dataTmp);
                        if (fullTLAbsModel) model = startModel;
                        else if (fullTLStepModel) model = newModel;

                        if (! fastTimeLapse) response = f->response(model); //** new data-new response

                        inv.setModel(model);
                        inv.setReferenceModel(model);
                        inv.setResponse(response);
                        inv.setLambda(lambdaTimeLapse); //** reset after possible lambdaDecrease

                        if (! dataTmp.haveData("rhoa")) dataTmp.set("rhoa", dataTmp("r") * dataTmp("k"));
                        if (timeLapseRemoveMisfit) { //** remove systematic misfit after LaBrecque et al. (1996)
                            if (dataTmp.size() == misfit.size()) {
                                vcout << "correcting data with misfit from initial frame" << std::endl;
                                dataTmp.set("rhoa", dataTmp("rhoa") * misfit);
                            } else {
                                vcout << "misfit size not matching! cannot correct." << std::endl;
                            }
                        }
                        inv.setData(dataTmp("rhoa"));
                        vcout << "data: min = " << min(dataTmp("rhoa")) << " max:" << max(dataTmp("rhoa")) << std::endl;

                        error = dataTmp("err");
                        if (fastTimeLapse && isRobust) error *= reweighting; //! take robust scheme from 0-data
                        inv.setRelativeError(error);

                        newModel = inv.run();
                        relModel = newModel / model;

                        if (debug) { //** save timelapse response and error
                            save(inv.response(), "response_tl_"+ toStr(count));
                            save(inv.error(), "error.vector_tl_"+ toStr(count) );
                        }
                        fitVector[0] = double(count + 1);
                        fitVector[1] = double(inv.iter());
                        fitVector[2] = inv.lambda();
                        fitVector[3] = inv.chi2();
                        fitVector[4] = inv.relrms();
                        fitVector[5] = rrmswitherr(inv.data(), inv.response(), inv.error()) * 100.0;
                        fitMatrix.push_back(fitVector);
                        saveMatrixRow(fitMatrix, "timelapse.log");
                    } else { //! Data-based: only one inverse step !fullTimelapse
                        inv.setModel(model);
                        inv.setReferenceModel(model);
                        RVector dData(dataIn.size());
                        //! use mean error (important for inactive base data)
                        error = (dataIn("err") + dataTmp("err")) / 2.0 ;
                        inv.setError(error);
                        if (max(abs(dataTmp("rhoa"))) > 0.0) {
                                dData = abs(dataTmp("rhoa") / dataIn("rhoa"));
                        } else {
                            if (dataIn.allNonZero("r")) dData = abs(dataTmp("r") / dataIn("r"));
                            else dData = abs(dataTmp("r") / dataIn("rhoa") * dataIn("k"));
                        }
                        DEBUG save(dData, "dData1");
                        relModel = inv.invSubStep(log(dData)); //**log difference;
                        newModel = model * exp(relModel);
                    }

                    absoluteModel.push_back(newModel);
                    count ++;
                    paraDomain.clearExportData();
                    commStr = "I=" + toStr(inv.iter()) + " lam=" + toStr(lambdaTimeLapse) + " wz=" + toStr(zWeight) +
                          " rrms=" + toStr(inv.relrms()) + "% chi^2=" + toStr(inv.chi2());
                    paraDomain.setCommentString(GIMLI::versionStr() + ": " + commStr);
                    RVector pmodel( newModel( paraDomain.cellMarkers() ));
                    paraDomain.addExportData("Resistivity/Ohmm", pmodel);
                    paraDomain.addExportData("Resistivity(log10)",  log10( pmodel + 1e-16));
                    paraDomain.addExportData("ratio", pmodel / model( paraDomain.cellMarkers() ) );
                    
                    //resultsToShow.insert(std::make_pair("Log10(resistivity)", log10(abs(absoluteModel.back()) + 1e-16)));
                    vcout << "Write dcinv.result_" + toStr(count) << std::endl;
                    paraDomain.exportVTK("dcinv.result_" + toStr(count));

                    diffModel.push_back((relModel - 1.0) * 100.0); //** relative difference in %

                    if (max(dataTmp("ip")) > min(dataTmp("ip"))) {
                        ipModels.push_back(inv.invSubStep(dataTmp("ip") ));
                    }
                }  // if (timeFileName.size() > 0) {

            } // while (! timeFile.eof()) {
            timeFile.close();

            //** for use in CylBERT or other applications;
            save(absoluteModel, "modelAbs");
            save(diffModel, "modelDiff");
            if (ipModels.rows() > 0) save(ipModels, "ipModels");
        } else { // if (openInFile(timeLapseFilename.c_str(), &timeFile))
            vcout << "Could not read timesteps file " << timeLapseFilename << std::endl;
        }
    }  // if (timeLapseFilename != NOT_DEFINED)

    delete f;
    delete dataTrans;
    delete modelTrans;

    return EXIT_SUCCESS;
}
