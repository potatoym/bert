// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#include <dcfemlib.h>
#include <longoptions.h>
#include <iostream>

#include <domain3d.h>
#include <domain2d.h>

using namespace std;
using namespace DCFEMLib;

int main( int argc, char * argv[] ){

  LongOptionsList lOpt;
  lOpt.setLastArg("[-NBSs:x:y:z:X:Y:Z:i:n:] polygon-filename");
  lOpt.setDescription( (string) "Add queue of n vertices/nodes to the polygon from startPoint(x,y,z) to the endPoint(X,Y,Z). Default marker is set to -99." );
  lOpt.insert( "help", no_argument, 'h', "This help" );
  lOpt.insert( "verbose", no_argument, 'v', "Verbose mode" );
  lOpt.insert( "nodesOnly", no_argument, 'N', "********DEPRECATED******** use -m0" );
  lOpt.insert( "reverse", no_argument, 'R', "Reverse direction for surfaceNode switch [1]" );
  lOpt.insert( "surfaceNodes", no_argument, 'S', "Create nodes at surface (marker -1), with tapemeasure distance (2d only). Overrides -XYZ-options, uses spacing[-s]" );
  lOpt.insert( "spacing", required_argument, 's', "double: spacing between vertices [1.0]. Works only with surfaceNode option." );
  lOpt.insert( "xstart", required_argument, 'x', "double: xStart [0.0]" );
  lOpt.insert( "ystart", required_argument, 'y', "double: yStart [0.0]" );
  lOpt.insert( "zstart", required_argument, 'z', "double: zStart [0.0]" );
  lOpt.insert( "Xend", required_argument, 'X', "double: xEnd [0.0]" );
  lOpt.insert( "Yend", required_argument, 'Y', "double: yEnd [0.0]" );
  lOpt.insert( "Zend", required_argument, 'Z', "double: zEnd [0.0]" );
  lOpt.insert( "interface", required_argument, 'i', "list of vertices/nodes for an interface (2D only)" );
  lOpt.insert( "nVertices", required_argument, 'n', "int: Number of vertices/nodes [0]" );
    lOpt.insert( "marker", required_argument, 'm', "int: marker for the nodes [-99] respectively the interface" );

  bool help = false, verbose = false, surfaceNodes = false;

  double x = 0.0, y = 0.0, z = 0.0, dx = 0;
  double X = 0.0, Y = 0.0, Z = 0.0, spacing = 1.0;
  int nElecs = 0, direction = 1, marker = -99;
  string interfaceFileName = NOT_DEFINED;

  int option_char = 0, option_index = 0, tracedepth = 0;
  while ( ( option_char = getopt_long( argc, argv, "?hvS"
				       "R" // reverse
				       "s:" //spacing
				       "i:" //interface
                                       "m:" // marker
				       "x:y:z:"
				       "X:Y:Z:n:", lOpt.long_options(), & option_index ) ) != EOF){
    switch ( option_char ) {
    case '?': lOpt.printHelp( argv[0] ); return 1; break;
    case 'h': lOpt.printHelp( argv[0] ); return 1; break;
    case 'v': verbose = true; break;
    case 'R': direction = -1; break;
    case 'S': surfaceNodes = true; break;
    case 's': spacing = atof( optarg ); break;
    case 'x': x = atof( optarg ); break;
    case 'y': y = atof( optarg ); break;
    case 'z': z = atof( optarg ); break;
    case 'X': X = atof( optarg ); break;
    case 'Y': Y = atof( optarg ); break;
    case 'Z': Z = atof( optarg ); break;
    case 'n': nElecs = atoi( optarg ); break;
    case 'm': marker = atoi( optarg ); break;
    case 'i': interfaceFileName = optarg; break;
    default : cerr << "default value not defined " << (char)option_char << endl;
    }
  }
  if ( argc < 2 ) { lOpt.printHelp( argv[0] ); return 1; }

  string worldName( argv[ argc-1 ] );
  string worldFileName( worldName.substr( 0, worldName.rfind( ".poly" ) ) + ".poly" );

  int dimension = findDomainDimension( worldFileName );

    Domain2D world2;
    Domain3D world3;
    switch ( dimension ){
    case 2:
        world2.load( worldFileName );
        if ( interfaceFileName != NOT_DEFINED ){
            if ( marker == -99 ) marker = 1;
            fstream file; if ( !openInFile( interfaceFileName, &file, true ) ){ return 1; }
            vector < string > row;
            vector < Node * > interface;
            while ( !file.eof() ){
                row = getNonEmptyRow( file );
                switch( row.size() ){
                    case 2: interface.push_back( world2.createVIP( RealPos( toDouble( row[ 0 ] ), toDouble( row[ 1 ] ) ), 1 ) ); break;
                    case 3: interface.push_back( world2.createVIP( RealPos( toDouble( row[ 0 ] ), toDouble( row[ 1 ] ) ), 1 ) ); break;
                }
            }
            file.close();

            for ( uint i = 0; i < interface.size() - 1; i ++ ){
                world2.createEdge( *interface[ i ], *interface[ i + 1 ], marker  );
            }
        } else if ( surfaceNodes ){
            world2.createLineOfVIPsSurface( RealPos( x, z ), spacing, nElecs, marker, HOMOGEN_NEUMANN_BC, direction );
        } else {
            if ( nElecs == 0 ){
                nElecs = (int)rint(RealPos( X, Z ).distance( RealPos( x, z ) ) / spacing )+ 1;
            }

            world2.createLineOfVIPs( RealPos( x, z ), RealPos( X, Z ), nElecs, marker );
        }
        world2.save( worldFileName );
        break;
  case 3:
    world3.load( worldFileName );
    if ( nElecs == 0 ){
      nElecs = (int)rint(RealPos( X, Y, Z ).distance( RealPos( x, y, z ) ) / spacing) +1;
    }
    world3.createLineOfVIPs( RealPos( x, y, z ), RealPos( X, Y, Z ), nElecs, marker );
    world3.save( worldFileName );
    break;
  default:
    cerr << WHERE_AM_I << " Dimension: " << dimension << " undefined." << endl;
  }

  return 0;

}
