// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#include <ctime>
#include <dcfemlib.h>
using namespace DCFEMLib;

#include <mesh2d.h>
#include <mesh3d.h>
using namespace MyMesh;

#include <longoptions.h>

#include <iostream>
#include <fstream>
#include <cstdio>

using namespace std;

int collectGetFemData( const BaseMesh & mesh, RVector & data, const string & filename ){
  data.resize( mesh.nodeCount() );

  vector < vector< double > > dataMap;

  for ( int i = 0; i < mesh.nodeCount(); i ++ ){
    dataMap.push_back( *new vector< double > );
  }
  fstream file; openInFile( filename, & file );

  char strDummy[128];
  file >> strDummy >> strDummy >> strDummy >> strDummy; //% GETFEM++ DATA FILE
  file >> strDummy >> strDummy >> strDummy; //BEGIN DATA ELEMENT
  file >> strDummy >> strDummy >> strDummy; //N = 3
  file >> strDummy >> strDummy >> strDummy; //P = 1
  file >> strDummy >> strDummy >> strDummy; //K = 1

  double fltDummy, dataVal;
  for ( int i = 0; i < mesh.cellCount(); i ++ ){
    file >> strDummy >> strDummy >> strDummy; // DIM = 3;
    for ( int j = 0; j < mesh.cell( i ).nodeCount(); j ++ ){
      file >> fltDummy >> fltDummy >> fltDummy >> dataVal;
      dataMap[ mesh.cell( i ).node( j ).id() ].push_back( dataVal );
    }
  }

  for ( size_t i = 0; i < dataMap.size(); i ++ ){
    data[ i ] =  dataMap[ i ][ 0 ];
  }

  data.save("testdata.inpterp");
  //  file << "END DATA ELEMENT" << endl;
  file.close();
  return 1;
}

int saveGetFemInterpol( const BaseMesh & mesh, const RVector & data, const string & filename ){
  bool noData = false;
  if ( data.size() == 0 ) {
    noData = true;
  } else {
    if ( data.size() != mesh.nodeCount() ){
      cerr << WHERE_AM_I << " Data dimension invalid: " << data.size()
	   << "/" << mesh.nodeCount() << endl;
      return 0;
    }
  }
  fstream file; openOutFile( filename, & file);
  file << "BEGIN DATA ELEMENT" << endl;
  file << "N = 3" << endl;
  file << "P = 1" << endl;
  file << "K = 1" << endl;

  for ( int i = 0; i < mesh.cellCount(); i ++ ){
    file << "DIM = 3" << endl;
    for ( int j = 0; j < mesh.cell( i ).nodeCount(); j ++ ){
      file << mesh.cell( i ).node( j ).x() << "\t"
	   << mesh.cell( i ).node( j ).y() << "\t"
	   << mesh.cell( i ).node( j ).z() << "\t" ;
      if ( noData ) {
	file << "0.0" << endl;
      } else {
	file << data[ mesh.cell( i ).node( j ).id() ] << endl;
      }
    }
   file << endl;
  }

  file << "END DATA ELEMENT" << endl;
  file.close();
  return 1;
}

int saveMidCellVal( const BaseMesh & mesh, const RVector & data, const string & filenamebody ){
  if ( data.size() != mesh.cellCount() ){
    cerr << WHERE_AM_I << " Data dimension invalid: " << data.size()
 	 << "/" << mesh.cellCount() << endl;
    return 0;
  }

  RVector ipData;
  ipData.load( "ip_model.vector", Ascii, false, false );
  bool isip = ( ipData.size() == mesh.cellCount() );
  if ( isip ) cout << "Found IP model. Saving." << endl;
  else cout << "mesh size=" << mesh.cellCount() << " ip size =" << ipData.size() << endl;

  string filename( filenamebody );
  if ( mesh.dim() == 2 ) filename += ".xyr";
  if ( mesh.dim() == 3 ) filename += ".xyzr";

  fstream file; openOutFile( filename, & file);
  RealPos cellMiddle;
  RealPos output;

  for ( int i = 0, imax = mesh.cellCount(); i < imax; i ++){
    cellMiddle = mesh.cell( i ).middlePos();

    file << cellMiddle.x() << "\t"
	 << cellMiddle.y() << "\t";
    if ( mesh.dim() == 3 ) file << cellMiddle.z() << "\t";
    file << data[ i ];
    if ( isip ) file << "\t" << ipData[ i ];
    file << endl;
  }
  file.close();
  return 1;
}

int main( int argc, char *argv[] ){
//  clock();

  LongOptionsList lOpt;
  lOpt.setLastArg( "meshfile" );
  lOpt.setDescription( "Convert mesh from one format to another and add data to it" );
  lOpt.insert( "help", no_argument, 'h', "This help" );
  lOpt.insert( "verbose", no_argument, 'v', "Verbose mode" );
  lOpt.insert( "dimension", required_argument, 'd' , "mesh dimension [3]" );
  lOpt.insert( "log-scale-data", no_argument, 'l' , "logarithmic data scaling [false]" );
  lOpt.insert( "binarySave", no_argument, 'B' , "Save mymesh binary." );
  lOpt.insert( "facesOnly", no_argument, 'F' , "Save faces only." );
  lOpt.insert( "asciiData", no_argument, 'A' , "Ascii data *.xyr [false]" );
  lOpt.insert( "check", no_argument, 'C' , "check mesh consistency (if each node is part of the mesh)" );
  //  lOpt.insert( "outModell-Matlab", no_argument, 'm' , "Output matlab input." );
  lOpt.insert( "oldOutDCFEMLib", no_argument, 'Y' , "Output DCFEMLib file. (old use D)" );
  lOpt.insert( "outDCFEMLib", no_argument, 'D' , "Output DCFEMLib file." );
  //  lOpt.insert( "outGmv", no_argument, 'V' , "Output GMV file." );
  lOpt.insert( "outVTK", no_argument, 'T' , "Output VTK unstructured-grid file." );
  lOpt.insert( "vtkElecsList", no_argument, 'E' , "Generate list of electrodes in vtk-mode." );
  lOpt.insert( "outGeomview", no_argument, 'G' , "Output Geomview file." );
  lOpt.insert( "outMedit", no_argument, 'M' , "Output Medit file." );
  lOpt.insert( "outAscii", no_argument, 'm' , "Output xy[z] midcell data." );
  lOpt.insert( "outGetFEM", no_argument, 'I' , "Output getfem++ interpolations." );
  lOpt.insert( "outputFile", required_argument, 'o' , "Output file name" );
  lOpt.insert( "refine", required_argument, 'r' , "Global refinement of the output mesh. The number of iterations. i.e -r2 ( 2 times global refinement" );
  lOpt.insert( "p2refine", no_argument, 'p' , "Generate p2 refined mesh." );
  lOpt.insert( "shrink", required_argument, 's' , "Shrink value [1.0]" );
  lOpt.insert( "data", required_argument, 'b' , "Data file name" );
  lOpt.insert( "inputformat", required_argument, 'i' , "Input style: i.e. Grummp[G], Tetgen[T] or DCFEMMesh[D], autodetect not yet." );
  lOpt.insert( "getfemInterpolFile", required_argument, 'g' , "Getfem interpolate file name" );
  lOpt.insert( "attributeMap", required_argument, 'a', "Attribute map" );

  string meshname;
  if ( argc < 2 ) lOpt.printHelp( argv[0] ); else meshname = argv[ argc - 1 ];

  //** define default values
  bool help = false, verbose = false, logScaleData = false, mymeshbin = false;
  bool geomview = false, gmv = false, medit = false, myfemlib = false, middleCellVal = false,
        getfem = false, facesonly = false;

  bool vtk = false, refinep2 = false, asciiData = false, vtkElecsList = false, check = false;
  int solver = 0, debug = 0, dimension = 3, refineIterations = 0 ;
  double shrink = 1.0;
  string outputname = NOT_DEFINED;
  string inputStyle = NOT_DEFINED;
  string datafilename = NOT_DEFINED;
  string getFemInterpolfilename = NOT_DEFINED;
  string attributemap = NOT_DEFINED;

  int option_char = 0, option_index;
  while ( ( option_char = getopt_long( argc, argv, "?"
				       "h"
				       "v"
				       "B" // save mymesh binary
                                       "C" // check
                                       "F" // faces only
				       "p" // generate p2
				       "l"
				       "G"
				       "M"
				       "I"
				       "T" // VTK-file
				       "m" // saveMidCellVal
				       "A" // ascii data
				       "V"
				       "E" //vtkElecsList
				       "D" //DCFEMLib out
				       "Y" //DCFEMLib out
				       "o:"
				       "d:"
				       "r:" // iterations of gloabel refinement
				       "s:"
				       "i:"
				       "g:" // getfeminterpolate file
				       "a:" // attruibute map
				       "b:" // databody
				       , lOpt.long_options(), & option_index ) ) != EOF){
    switch ( option_char ) {
    case '?': lOpt.printHelp( argv[0] ); return 1; break;
    case 'h': lOpt.printHelp( argv[0] ); return 1; break;
    case 'v': verbose = true; break;
    case 'A': asciiData = true; break;
    case 'B': mymeshbin = true; break;
    case 'G': geomview = true; break;
    case 'M': medit = true; break;
    case 'F': facesonly = true; break;
    case 'C': check = true; break;
    case 'I': getfem = true; break;
    case 'T': vtk = true; break;
    case 'V': vtk = true; break;
    case 'E': vtkElecsList = true; break;
    case 'm': middleCellVal = true; break;
    case 'Y': myfemlib = true; break;
    case 'D': myfemlib = true; break;
    case 'p': refinep2 = true; break;
    case 'o': outputname = optarg; break;
    case 'd': dimension = atoi( optarg ); break;
    case 'r': refineIterations = atoi( optarg ); break;
    case 's': shrink = atof( optarg ); break;
    case 'i': inputStyle = optarg; break;
    case 'l': logScaleData = true; break;
    case 'b': datafilename = optarg; break;
    case 'a': attributemap = optarg; break;
    case 'g': getFemInterpolfilename = optarg; break;
    default : cerr << WHERE_AM_I << " undefined option: " << option_char << endl;
    }
  }

  if ( outputname == NOT_DEFINED ){
    outputname = meshname;
  }

  if ( verbose ){
    cout << "Inputformat = " << inputStyle << endl;
    cout << "Dimension = " << dimension << endl;
    cout << "Mesh = " << meshname << endl;
    cout << "OutputFile = " << outputname << endl;
    cout << "Shrink = " << shrink << endl;
    cout << "Refinements steps = " << refineIterations << endl;
    cout << "Attribute Map = " << attributemap << endl;
    cout << "Data = " << datafilename << endl;
    cout << "AsciiData = " << asciiData << endl;
  }

  RVector data;
  if ( datafilename != NOT_DEFINED ) {
    if ( asciiData )  {
      if ( verbose ) cout << "load ascii .. " << endl;
      data.load( datafilename, Ascii );
    } else{
      data.load( datafilename );
    }
    if ( verbose ) cout << " ... finished. min=" << min(data) << " max=" << max(data) << endl;
  }


  BaseMesh *mesh=0, *outmesh=0;
  switch ( dimension ){
  case 2:
    mesh = new Mesh2D();
    outmesh = new Mesh2D();
    break;
  case 3:
    mesh = new Mesh3D();
    outmesh = new Mesh3D();
    break;
  }

//     std::cout << "mesh: " << mesh << std::endl;
//     std::cout << "outmesh: " << outmesh<< std::endl;
  mesh->load( meshname, inputStyle );
  if ( verbose ) mesh->showInfos();

  if ( check ) {
    for ( int i = 0; i < mesh->nodeCount(); i ++ ){
      if ( mesh->node( i ).cellSet().size() == 0 ){
        std::cout << mesh->node( i ) << std::endl;
        std::cout << "is not part of the mesh" << std::endl;
      }
    }
  }

  if ( attributemap != NOT_DEFINED ) mesh->mapCellAttributes( loadFloatMap( attributemap ) );

  for ( int i = 0; i < refineIterations; i ++ ){
    outmesh->createH2Mesh( *mesh );
    if ( verbose ) outmesh->showInfos();
    *mesh = *outmesh;
  }

  if ( refinep2 ){
    outmesh->createP2Mesh( *mesh );
    if ( verbose ) outmesh->showInfos();
    *mesh = *outmesh;
  }

  if ( geomview ){
    if ( verbose ){ cout << "Save OOGL_OFF ... "; }
    mesh->saveOOGL_Off( outputname, shrink );
    if ( verbose ){ cout << " ... done." << endl; }
  }
  if ( medit ){
    if ( verbose ){ cout << "Save INRIAMesh ... "; }
    dynamic_cast<Mesh3D*>(mesh)->saveINRIAMeshFile( outputname, data, logScaleData );
    if ( verbose ){ cout << " ... done." << endl; }
  }
  if ( vtk ){
    if ( verbose ){ cout << " Save VTK-file (" << outputname+".vtk" << ")... "; }
//     for ( int i = 0; i <     mesh->cellCount(); i ++ ){

//       if ( mesh->cell(i).averagePos().y() >0  && mesh->cell(i).attribute() == 1.0 ){
// 	mesh->cell(i).setAttribute(3);
//       }
//     }
    if ( facesonly ) {
        mesh->deleteAllCells();
    }

    mesh->saveVTKUnstructured( outputname, data, logScaleData );

    if ( verbose ){ cout << " ... done." << endl; }
  }
  if ( vtkElecsList ){
    vector < RealPos > ePos( mesh->positions( mesh->getAllMarkedNodes( -99 ) ) );

    Mesh3D meshElecs;
    for ( uint i = 0; i < ePos.size(); i ++ ) meshElecs.createNode( ePos[i] );
    if ( ePos.size() == 0 ){
      cerr << WHERE_AM_I << " no electrode Infos found in " << meshname << endl;
    } else {
      meshElecs.saveVTKUnstructured( outputname+".elecs" );
    }
  }

  if ( gmv ){
    if ( verbose ){ cout << "Save GMVMesh ... "; }
    mesh->saveGMVFile( outputname );
    if ( verbose ){ cout << " ... done." << endl; }
  }
  if ( myfemlib ){
    if ( verbose ){ cout << "Save DCFEMLib ... "; }
    if ( mymeshbin ) {
      mesh->save( outputname, Binary );
    } else mesh->save( outputname, Ascii );
    if ( verbose ){ cout << " ... done." << endl; }
  }

  if ( middleCellVal ){
    if ( verbose ){ cout << "Save Ascii ... "; }
    saveMidCellVal( *mesh, data, outputname );
    if ( verbose ){ cout << " ... done." << endl; }
  }

//  exit( 0 ); // destructor mesh buggy
  return 0;
}

/*
$Log: meshconvert.cpp,v $
Revision 1.22  2011/08/30 14:09:51  carsten
fix memory problem within mesh-copy

Revision 1.21  2010/06/22 14:42:47  thomas
*** empty log message ***

Revision 1.20  2010/06/18 10:17:59  carsten
win32 compatibility commit

Revision 1.19  2008/11/21 07:51:57  thomas
only changes in documentation

Revision 1.18  2008/10/05 13:25:00  carsten
*** empty log message ***

Revision 1.17  2008/02/13 12:37:15  carsten
*** empty log message ***

Revision 1.16  2007/08/16 14:45:59  thomas
ip added to xy(z)r file

Revision 1.15  2007/05/09 12:54:41  thomas
*** empty log message ***

Revision 1.14  2006/10/06 16:53:39  carsten
*** empty log message ***

Revision 1.13  2006/08/27 18:58:03  carsten
*** empty log message ***

Revision 1.12  2005/06/21 14:53:59  carsten
*** empty log message ***

Revision 1.11  2005/06/13 16:00:00  carsten
*** empty log message ***

Revision 1.10  2005/06/01 18:26:51  carsten
*** empty log message ***

Revision 1.9  2005/05/31 10:50:04  carsten
*** empty log message ***

Revision 1.8  2005/05/03 15:55:34  carsten
*** empty log message ***

Revision 1.7  2005/04/07 11:41:08  carsten
*** empty log message ***

Revision 1.6  2005/03/20 22:51:59  carsten
*** empty log message ***

Revision 1.5  2005/03/11 17:49:11  carsten
*** empty log message ***

Revision 1.4  2005/02/18 14:06:19  carsten
*** empty log message ***

Revision 1.3  2005/02/16 19:36:38  carsten
*** empty log message ***

Revision 1.2  2005/02/10 14:07:59  carsten
*** empty log message ***

*/
