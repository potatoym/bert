// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#include <dcfemlib.h>
using namespace DCFEMLib;

#include <longoptions.h>
#include <electrodeconfig.h>

#include <elements.h>
#include <mesh2d.h>
#include <trianglewrapper.h>
#include <mesh3d.h>
#include <domain3d.h>

#include <interpolate.h>

using namespace MyMesh;

using namespace std;

int loadPosListLocal( vector< RealPos > & posList, vector< int > & markerList, const string & fname );
int loadPolygonsLocal( const string & fname, vector < vector< RealPos > > & polygons );

int main(int argc, char *argv[]){
  setbuf( stdout, NULL );
//   RVector fineTopo1;
//   Mesh2D meshCoarse1; meshCoarse1.load("meshCoarse.bms" );
//   Mesh2D meshFine1; meshFine1.load("mesFine.bms" );
//   RVector coarseTopo1; coarseTopo1.load("coarseTopo", Ascii );
//   interpolateFrom2( meshCoarse1, coarseTopo1, meshFine1, fineTopo1, false );
//
// //     vector < RealPos > fineMeshNodes1 = meshFine1.nodePositions();
// //   vector < vector < Node * > > vectorSamplingNodes1 = findMeshEntitiesOld( meshCoarse1, fineMeshNodes1, true );
//
//  // interpolate( vectorSamplingNodes1, coarseTopo1, fineMeshNodes1, fineTopo1 );
//
//   exit(0);:q



  LongOptionsList lOpt;
  lOpt.setLastArg("dat-file");
  lOpt.setDescription( (string) "Create a surface mesh from (x,y,z) list" );
  lOpt.insert( "help", no_argument, 'h', "This help" );
  lOpt.insert( "verbose", no_argument, 'v', "Verbose mode" );
  lOpt.insert( "smooth2D", no_argument, 'S', "Smooth 2d surface [false]" );
  lOpt.insert( "meshinput", no_argument, 'M', "Meshinput [false]" );
  lOpt.insert( "localCoords", no_argument, 'L', "translate the coordinates to orign [false]" );
  lOpt.insert( "ignoreElectrodeTopo", no_argument, 'I', "Ignore electrode topo use additional points instead [false]" );
  lOpt.insert( "output", required_argument, 'o', "Output file name [out]" );
  lOpt.insert( "quality", required_argument, 'q', "Quality for finer-grid [30 = very bad]" );
  lOpt.insert( "boundary", required_argument, 'b', "Boundary in \% [25]" );
  lOpt.insert( "paraBoundary", required_argument, 'p', "Boundary in \% [5]" );
  lOpt.insert( "dx", required_argument, 'd', "dx for local refinement [0.0]" );
  lOpt.insert( "maxTriangleArea", required_argument, 'm', "Maximum triangle area for parametric surface grid [0.0]" );
  lOpt.insert( "additionalPoints", required_argument, 'a', "additional points for topography [none]" );
  lOpt.insert( "additionalPolygon", required_argument, 'P', "additional polygons for topography [none]" );


  if ( argc == 1 ) { lOpt.printHelp( argv[0] ); return 1; }

  //** define default values
  bool help = false, verbose = false, save = false,
      smooth = false, meshinput = false, ignoreETopo = false ;
  bool datTopoOutput = false, localCoord = false;
  double quality = 30.0, boundary = 25.0, boundaryPara = 0.0, dx = 0.0;
  double maxTriArea = 0.0;
  //  char quietTri = 'Q';
  string outfilename = "out";
  string additionalPoints = NOT_DEFINED;
  string additionalPolygons = NOT_DEFINED;

  int option_char = 0, option_index = 0, tracedepth = 0;
  while ( ( option_char = getopt_long( argc, argv, "?hv"
                                        "L" // localCoords
                                        "S" // smoothMesh
				       "M" // meshinput
				       "I" // ignoreETopo
				       "q:" // quality
				       "o:" // output
				       "b:" // boundary
				       "p:" // Paraboundary
				       "a:" // additionalPoints
				       "m:" // maxTriArea
				       "d:" // dx
				       "P:" // AddPolygons
				       , lOpt.long_options(), & option_index ) ) != EOF){
    switch ( option_char ) {
    case '?': lOpt.printHelp( argv[0] ); return 1; break;
    case 'h': lOpt.printHelp( argv[0] ); return 1; break;
    case 'v': if ( verbose ) save = true; else verbose = true; break;
    case 'S': smooth = true; break;
    case 'M': meshinput = true; break;
    case 'L': localCoord = true; break;
    case 'I': ignoreETopo = true; break;
    case 'q': quality = atof( optarg ); break;
    case 'b': boundary = atof( optarg ); break;
    case 'a': additionalPoints = optarg; datTopoOutput = true; break;
    case 'p': boundaryPara = atof( optarg ); break;
    case 'd': dx = atof( optarg ); break;
    case 'm': maxTriArea = atof( optarg ); break;
    case 'o': outfilename = optarg; break;
    case 'P': additionalPolygons = optarg; break;
    default : cerr << WHERE_AM_I << " undefined option: " << option_char << endl;
    }
  }

  if ( outfilename == NOT_DEFINED ) { lOpt.printHelp( argv[0] ); return 1; }

  string topofilename = argv[ argc - 1 ];
  std::vector < RealPos > topoList, electrodePosList;
  std::vector < std::vector < RealPos > > polygonsVector;
  std::vector < int > markerList;

  bool isTopo = false;

  if ( meshinput ){
    isTopo = true;
    Mesh3D topoInmesh; topoInmesh.load( topofilename );
    if ( verbose ) topoInmesh.showInfos();
    SetpBounds facesAtNode;
    for ( int i = 0; i < topoInmesh.nodeCount(); i ++ ){
      facesAtNode = topoInmesh.node( i ).boundarySet();

      for ( SetpBounds::iterator it = facesAtNode.begin(); it != facesAtNode.end(); it ++ ){
	if ( ( (*it)->marker() < 100 && (*it)->marker() > 0 ) ||
	     (*it)->marker() == -1 || (*it)->marker() == -99 ){
	  topoList.push_back( topoInmesh.node( i ).pos() );
	  markerList.push_back( topoInmesh.node( i ).marker() );
	  //	  electrodePosList.push_back( topoInmesh.node( i ).pos() );
	  break;
	}
      }
    }
  } else { //** if no mesh input;
    loadPosListLocal( topoList, markerList, topofilename );

    for ( uint i = 0; i < topoList.size(); i ++ ){
      //      if ( markerList[i] == -99 )
      electrodePosList.push_back( topoList[ i ] );
    }

    //** check if really topography;
    double zStart = topoList[ 0 ].z();
    for ( size_t i = 1; i < topoList.size(); i ++ ){
      if ( topoList[ i ].z() != zStart ){
	isTopo = true;
	break; //** everything is fine;
      }
    }

    if ( !isTopo || ignoreETopo ){
      if ( verbose ) cout << " No topography found ! " << endl;
      topoList.clear();
      markerList.clear();
    }

    if ( additionalPoints != NOT_DEFINED ){
      loadPosListLocal( topoList, markerList, additionalPoints );
      isTopo = true;
      if ( verbose ) cout << topoList.size() << " additional points loaded." << endl;
    }
    if ( additionalPolygons != NOT_DEFINED ){
      loadPolygonsLocal( additionalPolygons, polygonsVector );
      if ( verbose ) cout << polygonsVector.size() << " additional polygons loaded." << endl;
    }
  } // if no mesh-input

  if ( markerList.size() != topoList.size() ) markerList.resize( topoList.size(), 0 );

  Domain2D polyCoarse;
  int marker = 0;
  //** add additional polygons to the coarse input
  for ( uint i = 0; i < polygonsVector.size(); i ++ ){
    polyCoarse.createPolygon( polygonsVector[ i ], 100, false );
  }

  Node *tmp;
  for ( size_t i = 0; i < topoList.size(); i ++ ){
    marker = markerList[ i ];
    if ( marker != -99 ) marker = -1;
    tmp = polyCoarse.createVIP( RealPos( topoList[ i ].x(), topoList[ i ].y(), topoList[ i ].z() ), marker, 1e-3 );
  }

  double xmin = 0, xmax = 0, ymin = 0, ymax = 0, zmin = 0, zmax = 0;
  if ( topoList.size() > 0 ) {
    findMinMaxVals( topoList, xmin, xmax, ymin, ymax, zmin, zmax );
  }
  double border = max( (xmax - xmin) * boundary / 100.0, (ymax - ymin) * boundary / 100.0);

    RealPos localTrans;
    RealPos coarseLocalTrans, fineLocalTrans;
    if ( localCoord ) {
        localTrans = RealPos( xmin, ymin, 0.0 );
    } else {
        localTrans = RealPos( 0.0, 0.0, 0.0 );
    }
    coarseLocalTrans = localTrans;
    fineLocalTrans = localTrans;

    if ( verbose ){
        cout << "translate: " <<  localTrans << endl;
        cout << "border = "   << border << endl;
        cout << "Coarse bb min: " << RealPos(xmin, ymin, zmin )
                << "max: " <<  RealPos( xmax, ymax, zmax) << endl;
    }

  Node *n1 = polyCoarse.createVIP( RealPos( xmin - border, ymin - border, zmin ), 1 );
  Node *n2 = polyCoarse.createVIP( RealPos( xmax + border, ymin - border, zmin ), 2 );
  Node *n3 = polyCoarse.createVIP( RealPos( xmax + border, ymax + border, zmin ), 3 );
  Node *n4 = polyCoarse.createVIP( RealPos( xmin - border, ymax + border, zmin ), 4 );

    if ( boundary > 0.0 ) {
  polyCoarse.createEdge( *n1, *n2, 12 );
  polyCoarse.createEdge( *n2, *n3, 23 );
  polyCoarse.createEdge( *n3, *n4, 34 );
  polyCoarse.createEdge( *n4, *n1, 41 );
    }

    if ( save ) {
        polyCoarse.save( outfilename + "-PolyCoarseFlath", Ascii );
        polyCoarse.saveVTK( outfilename + "-PolyCoarseFlath" );
    }

    Mesh2D meshCoarse( polyCoarse.createMesh( 0.0, verbose) );

    if ( save ) {
        meshCoarse.saveVTK( outfilename + "-MeshCoarseFlath" );
    }

  RVector coarseTopo( meshCoarse.nodeCount(), 0.0 );
  for ( int i = 0; i < polyCoarse.nodeCount(); i ++ ) coarseTopo[ i ] = polyCoarse.node( i ).z();

  if ( polyCoarse.nodeCount() != meshCoarse.nodeCount() ) {
    cerr << WHERE_AM_I << " this should not happen. " << polyCoarse.nodeCount() << " " << meshCoarse.nodeCount() << endl
	 << " try to fix" << endl;

    for ( int i = polyCoarse.nodeCount(); i < meshCoarse.nodeCount(); i ++ ){

      coarseTopo[ i ] = zmin; // ** funktioniert nur wenn die neuen Knoten auf einer vorhandenen Edge liegen.

      for ( int j = 0; j < polyCoarse.polygonCount(); j ++ ){
	if (polyCoarse.polygon( j ).size() > 1 )	{

	  RealPos p0( polyCoarse.polygon( j )[ 0 ]->pos() ); p0[ 2 ]=0.0;
	  RealPos p1( polyCoarse.polygon( j )[ 1 ]->pos() ); p1[ 2 ]=0.0;

	  if ( Line( p0, p1 ).touch(meshCoarse.node( i ).pos() ) == 3 ){

	    RealPos touchPoint( Line( polyCoarse.polygon( j )[ 0 ]->pos(),
				      polyCoarse.polygon( j )[ 1 ]->pos() ).intersect(
				      Line( meshCoarse.node( i ).pos(),
					    meshCoarse.node( i ).pos() + RealPos( 0.0, 0.0, 10000.0 ) ) ) );

// 	  cout << polyCoarse.polygon( j )[ 0 ]->pos()<< " " << polyCoarse.polygon( j )[ 1 ]->pos() << endl;
// 	  cout << meshCoarse.node( i ).pos()<< " " << meshCoarse.node( i ).pos() + RealPos( 0.0, 0.0, 10000.0 ) << endl;
//	  cout << touchPoint << endl;
	    if ( touchPoint.valid() ){
	      coarseTopo[ i ] = touchPoint.z();
	      break;
	    }
	  }
	} else  {
	  cout << j << " " << polyCoarse.polygon( j ).size() << endl;
	}
      }

      //      exit(1);
    }
  }



  if ( save ){
    for ( int i = 0; i < coarseTopo.size(); i ++ )  meshCoarse.node( i ).setZ( coarseTopo[ i ] );
    polyCoarse.saveVTK( outfilename + "-PolyCoarse" );
    meshCoarse.saveVTK( outfilename + "-MeshCoarse" );
    for ( int i = 0; i < coarseTopo.size(); i ++ ) meshCoarse.node( i ).setZ( 0.0 );
  }

  //! ** start ** create fine-mesh

    Domain2D polyFine;
    //** add additional polygons to the fine input
    for ( uint i = 0; i < polygonsVector.size(); i ++ ){
        polyFine.createPolygon( polygonsVector[ i ], 100, false );
    }

    for ( int i = 0; i < polyCoarse.nodeCount(); i ++ ){
        if ( ( polyCoarse.node( i ).marker() > 0 && polyCoarse.node( i ).marker() < 42 ) ) {
            polyFine.createVIP( polyCoarse.node( i ).pos() - coarseLocalTrans,
                                polyCoarse.node( i ).marker(), 1e-3 );
        } else {
            if ( meshinput ){
    	       polyFine.createVIP( polyCoarse.node( i ).pos(), polyCoarse.node( i ).marker(), 1e-3  );
            }
        }
    }

  for ( uint i = 0; i < electrodePosList.size(); i ++ ) polyFine.createVIP( electrodePosList[ i ], -99, 1e-3  );

    if ( boundary > 0.0 ) {
  for ( int i = 0; i < polyFine.nodeCount(); i ++ ){
    if ( polyFine.node( i ).marker() == 1 ) n1 = &polyFine.node( i );
    else if ( polyFine.node( i ).marker() == 2 ) n2 = &polyFine.node( i );
    else if ( polyFine.node( i ).marker() == 3 ) n3 = &polyFine.node( i );
    else if ( polyFine.node( i ).marker() == 4 ) n4 = &polyFine.node( i );
  }

  polyFine.createPolygon( polyFine.findAllNodesBetween( n1, n2, 12 ) );
  polyFine.createPolygon( polyFine.findAllNodesBetween( n2, n3, 23 ) );
  polyFine.createPolygon( polyFine.findAllNodesBetween( n3, n4, 34 ) );
  polyFine.createPolygon( polyFine.findAllNodesBetween( n4, n1, 41 ) );

  n1->setMarker( 1 ); n2->setMarker( 2 ); n3->setMarker( 3 ); n4->setMarker( 4 );
  if ( dx > 0 ){
    for ( int i = 0; i < polyFine.nodeCount(); i ++ ){
      if ( polyFine.node( i ).marker() == -99 ){
	//(*inmesh).createNode(  (*inmesh).node( i ).pos() + RealPos( dx, 0.0, 0.0) );
	//(*inmesh).createNode(  (*inmesh).node( i ).pos() - RealPos( dx, 0.0, 0.0) );
	//(*inmesh).createNode(  (*inmesh).node( i ).pos() + RealPos( 0.0, dx, 0.0) );
	polyFine.createVIP( polyFine.node( i ).pos() - RealPos( 0.0, dx, 0.0) );
      }
    }
  }
    }

  if ( boundaryPara > 0 ){
    double xElmin = 9e99, xElmax = -9e99, yElmin = 9e99, yElmax = -9e99, zElmin = 9e99, zElmax = -9e99;
    if ( electrodePosList.size() > 0 ) {
      findMinMaxVals( electrodePosList, xElmin, xElmax, yElmin, yElmax, zElmin, zElmax );
      if ( verbose ){
        std::cout << "Electrode bb: " << RealPos( xElmin, yElmin, zElmin ) << " " <<
                RealPos( xElmax, yElmax, zElmax ) << std::endl;
      }
    } else {
      cerr << WHERE_AM_I << "not electrodes are defined" << endl;
      return 1;
    }

    double maxSpan = max( xElmax - xElmin, yElmax - yElmin );
    double borderPara = maxSpan * boundaryPara / 100.0;

    Node *n5 = polyFine.createVIP( RealPos( xElmin - borderPara, yElmin - borderPara ), 5 );
    Node *n6 = polyFine.createVIP( RealPos( xElmax + borderPara, yElmin - borderPara ), 6 );
    Node *n7 = polyFine.createVIP( RealPos( xElmax + borderPara, yElmax + borderPara ), 7 );
    Node *n8 = polyFine.createVIP( RealPos( xElmin - borderPara, yElmax + borderPara ), 8 );
    polyFine.createEdge( *n5, *n6, 56 );
    polyFine.createEdge( *n6, *n7, 67 );
    polyFine.createEdge( *n7, *n8, 78 );
    polyFine.createEdge( *n8, *n5, 85 );
    polyFine.createRegionMarker( n5->pos() + RealPos( 1.0, 1.0 ), 2, maxTriArea );
    polyFine.createRegionMarker( n5->pos() - RealPos( 1.0, 1.0 ), 1, 0.0 );
  }

    Mesh2D meshFine( polyFine.createMesh( quality, verbose ) );
    if ( smooth && !meshinput ){
//      meshFine.improveMeshQuality( true, true, 1, 5);
      //      outmeshFine.improveMeshQuality( true, true, 0, 5);
    }
  //  meshFine.save("meshFine.bms");
  //  TriangleWrapper triangleFine( inmeshFine, outmeshFine );
  //  triangleFine.saveInputPolyFile("testF.poly");
  //   sprintf(strdummy, "-%cpzeAq%fa", quietTri, quality);
  //   triangleFine.setSwitches(strdummy);
  //   if ( ! triangleFine.generateMesh() ) cerr << "No mesh generated" << endl;
    meshCoarse.translate( coarseLocalTrans * -1.0 );
    meshFine.translate( fineLocalTrans * -1.0 );

    if ( save ){
        polyFine.saveVTK( outfilename + "-PolyFineFlath" );
        meshFine.saveVTK( outfilename + "-MeshFineFlath" );
        meshFine.save( outfilename + "-MeshFineFlath.bms" );
        meshCoarse.saveVTK( outfilename + "-MeshCoarseTrans" );
        meshCoarse.save( outfilename + "-MeshCoarseTrans.bms" );
        meshFine.save( outfilename + "-MeshFineFlath.bms" );
        coarseTopo.save( outfilename + "-coarseTopo.z", Ascii );
    }

    RVector fineTopo;

    interpolateFrom2( meshCoarse, coarseTopo, meshFine, fineTopo, false );

    if ( save ) {
        fineTopo.save( outfilename + "-fineTopo.z", Ascii );
    }

//   vector < RealPos > fineMeshNodes = meshFine.nodePositions();
//   vector < vector < Node * > > vectorSamplingNodes = findMeshEntitiesOld( meshCoarse, fineMeshNodes, verbose );
//
//   interpolate( vectorSamplingNodes, coarseTopo, fineMeshNodes, fineTopo );

  //meshFine.translate( localTrans );

  Domain3D worldFine; worldFine.addSurface( meshFine, 0.0 );

  for ( int i = 0; i < worldFine.nodeCount(); i ++ ) worldFine.node( i ).setZ( fineTopo[ i ] );

  //  worldFine.translate( RealPos( 0.0, 0.0, zmin ) );
  worldFine.saveTetgenPolyFile( outfilename + "Fine.poly" );
  if ( save )worldFine.saveVTK( outfilename + "PolyFine" );

  if ( datTopoOutput && ! meshinput ){
    vector < RealPos > vecSourcePos( worldFine.positions( worldFine.getAllMarkedNodes( -99 ) ) );

    ElectrodeConfigVector profile;
    profile.load( topofilename );

    if ( vecSourcePos.size() == profile.electrodeCount() ){
      for ( uint i = 0; i < vecSourcePos.size(); i ++ ){
	profile.pElectrodePos( i )->setZ( vecSourcePos[ i ].z() );
      }
    }
    profile.save( topofilename + ".topo", profile.inputFormatString() );
  }

  return 0;
}

int loadPolygonsLocal( const string & filename, vector < vector< RealPos > > & polygons ){
  fstream file; if ( !openInFile( filename, &file ) ) return -1;

  polygons.clear();
  std::vector < RealPos > poly;

  vector < string > row;
  int count = 0;
  while( !file.eof() ){
    row = getRowSubstrings( file );

    switch ( row.size() ){
    case 0:
      if ( poly.size() > 1 ){
	polygons.push_back( poly );
	poly.clear();
      }
      break;
    case 2: poly.push_back( RealPos( toDouble( row[ 0 ] ), toDouble( row[ 1 ] ) ) ); break;
    case 3: poly.push_back( RealPos( toDouble( row[ 0 ] ), toDouble( row[ 1 ] ), toDouble( row[ 2 ] ) ) ); break;
    default:
      if ( row.size() > 0 ){
	cerr << WHERE_AM_I << " column: "<< count << ", format unknown: "  << row.size() << endl;
  	for ( uint i = 0; i < row.size(); i ++ ) cout << row[i] << " ";
	cout << endl;
      }
      return -1;
    }
    count++;
  }
  file.close();

  if ( poly.size() > 1 ){
    polygons.push_back( poly );
    poly.clear();
  }

  return 1;
}

int loadPosListLocal( vector< RealPos > & posList, vector< int > & markerList, const string & fname ){

  fstream file; if ( !openInFile( fname, & file ) ) {  return 0; }

  vector < string > row = getRowSubstrings( file );
  int columnCount = row.size();
  file.seekg( 0 );

  vector < RealPos > newTopo;
  std::set < RealPos, lesserPos > newTopoSet;
  //   posList.clear();
//   markerList.clear();

  double x = 0.0, y = 0.0, z = 0.0;
  int iDummy = 0;
  ElectrodeConfigVector profiles;

  if ( columnCount == 1 ){
    profiles.load( fname );

    posList = profiles.electrodePositions();
    for ( size_t i = 0; i < posList.size(); i ++ ){
      markerList.push_back( -99 );
    }
    return 1;
  } else {
    while ( !file.eof() ){
      row = getNonEmptyRow( file );
      if ( row.size() == 0 && file.eof() ) break;

      switch ( row.size() ){
      case 1:
	cout << row[ 0 ] << endl;
	break;
      case 2: // Simple List: x z
        newTopoSet.insert( RealPos( toDouble( row[ 0 ] ), 0.0, toDouble( row[ 1 ] ) ) );
        break;
      case 3: // Simple List: x y z
	//newTopo.push_back( RealPos( toDouble( row[ 0 ] ), toDouble( row[ 1 ] ), toDouble( row[ 2 ] ) ) );
        newTopoSet.insert( RealPos( toDouble( row[ 0 ] ), toDouble( row[ 1 ] ), toDouble( row[ 2 ] ) ) );
            break;
      case 4: // MyMesh node-file: x y z marker;
        newTopoSet.insert( RealPos( toDouble( row[ 0 ] ), toDouble( row[ 1 ] ), toDouble( row[ 2 ] ) ) );
	if ( toInt( row[ 3 ] ) == 0 ) {
	  markerList.push_back( -1 );
	} else {
	  markerList.push_back( toInt( row[ 3 ] ) );
	}
	break;
      default:
	cerr << WHERE_AM_I << " format unknown:" << row.size() << endl;
      }
    }
  }
  file.close();

  posList.resize( newTopoSet.size() );
  std::copy( newTopoSet.begin(), newTopoSet.end(), posList.begin() );

  return 1;
  //** brute force search for duplicate vertieces;
  bool duplicate;
  for ( uint i = 0; i < newTopo.size(); i ++ ){
    duplicate = false;
    for ( uint j = 0; j < posList.size(); j ++ ){
      if ( newTopo[ i ] == posList[ j ] ) {
	duplicate = true;
	break;
      }
    }
    if ( !duplicate){
      posList.push_back( newTopo[ i ] );
      markerList.push_back( -1 );
    }
  }
  return 1;
}

/*
$Log: createSurface.cpp,v $
Revision 1.41  2009/11/17 11:44:41  thomas
minor changes in invert, createSurface, tutorial and codeblocks files resulting in release 1.0.3

Revision 1.40  2009/04/02 16:33:15  carsten
*** empty log message ***

Revision 1.39  2009/02/25 07:00:14  thomas
*** empty log message ***

Revision 1.38  2009/02/11 14:37:50  carsten
*** empty log message ***

Revision 1.37  2009/01/30 16:54:26  carsten
*** empty log message ***

Revision 1.36  2008/11/21 07:51:57  thomas
only changes in documentation

Revision 1.35  2008/10/05 13:25:00  carsten
*** empty log message ***

Revision 1.34  2008/05/19 14:52:28  carsten
Add cholmodwrapper, interpolate V.2 in createSurface

Revision 1.33  2008/04/15 16:53:04  carsten
*** empty log message ***

Revision 1.32  2008/04/15 12:57:18  carsten
*** empty log message ***

Revision 1.31  2007/02/12 13:46:40  carsten
*** empty log message ***

Revision 1.30  2006/11/08 21:07:31  carsten
*** empty log message ***

Revision 1.29  2006/11/08 19:53:24  carsten
*** empty log message ***

Revision 1.28  2006/10/26 18:28:00  carsten
*** empty log message ***

Revision 1.27  2006/10/06 16:53:39  carsten
*** empty log message ***

Revision 1.26  2006/08/09 19:10:15  carsten
*** empty log message ***

Revision 1.25  2006/07/28 16:24:51  carsten
*** empty log message ***

Revision 1.24  2006/05/08 16:25:59  carsten
*** empty log message ***

Revision 1.23  2006/02/25 18:46:44  carsten
*** empty log message ***

Revision 1.21  2005/10/24 09:52:08  carsten
*** empty log message ***

Revision 1.20  2005/10/18 16:29:56  carsten
*** empty log message ***

Revision 1.19  2005/10/18 15:59:04  carsten
*** empty log message ***

Revision 1.18  2005/10/17 11:56:58  carsten
*** empty log message ***

Revision 1.17  2005/09/26 17:18:29  carsten
*** empty log message ***

Revision 1.16  2005/09/08 11:25:15  carsten
*** empty log message ***

Revision 1.15  2005/08/30 15:50:07  carsten
*** empty log message ***

Revision 1.14  2005/08/30 15:43:28  carsten
*** empty log message ***

Revision 1.13  2005/08/09 18:13:43  carsten
*** empty log message ***

Revision 1.12  2005/07/14 15:03:37  carsten
*** empty log message ***

Revision 1.11  2005/07/13 13:36:49  carsten
*** empty log message ***

Revision 1.10  2005/06/01 18:26:51  carsten
*** empty log message ***

Revision 1.9  2005/05/31 10:50:04  carsten
*** empty log message ***

Revision 1.8  2005/05/03 15:55:34  carsten
*** empty log message ***

Revision 1.7  2005/04/07 11:41:08  carsten
*** empty log message ***

Revision 1.6  2005/04/01 15:18:28  carsten
*** empty log message ***

Revision 1.5  2005/03/30 18:37:14  carsten
*** empty log message ***

Revision 1.4  2005/03/21 18:07:28  carsten
*** empty log message ***

Revision 1.3  2005/03/17 18:54:34  carsten
*** empty log message ***

Revision 1.1  2005/03/15 16:08:55  carsten
*** empty log message ***

*/
