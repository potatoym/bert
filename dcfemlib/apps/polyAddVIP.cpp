// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#include <dcfemlib.h>
#include <longoptions.h>
#include <iostream>

#include <domain2d.h>
#include <domain3d.h>

using namespace std;
using namespace DCFEMLib;

int main( int argc, char * argv[] ){

  LongOptionsList lOpt;
  lOpt.setLastArg("[-RBHx:y:z:m:a:f:] polygon-filename");
  lOpt.setDescription( (string) "Adds a (V)ery (I)mportant (P)oint (a fixed vertex) at the position(x,y,z) with an optional marker" );
  lOpt.insert( "help", no_argument, 'h', "This help" );
  lOpt.insert( "verbose", no_argument, 'v', "Verbose mode" );
  lOpt.insert( "isRegionMarker", no_argument, 'R', "bool: VIP is region marker" );
  lOpt.insert( "isBoundaryMarker", no_argument, 'B', "bool: VIP is boundarymarker for whole domain, no koords needed" );
  lOpt.insert( "isHole", no_argument, 'H', "bool: VIP is hole marker" );
  lOpt.insert( "xpos", required_argument, 'x', "double: xpos [0.0]" );
  lOpt.insert( "ypos", required_argument, 'y', "double: ypos [0.0]" );
  lOpt.insert( "zpos", required_argument, 'z', "double: zpos [0.0]" );
  lOpt.insert( "marker", required_argument, 'm', "int: marker for VIP(s)" );
  lOpt.insert( "area", required_argument, 'a', "double: maximum area (only for region marker)" );
  lOpt.insert( "file", required_argument, 'f', "string: file with ascii (space separated) list of VIPs" );

  bool help = false, verbose = false, isRegion = false, isHole = false, isBoundary = false;

  double x = 0.0, y = 0.0, z = 0.0, area = 0.0, tolerance = 1e-6;
  int marker = 0;
  string vipFilename = NOT_DEFINED;
  int option_char = 0, option_index = 0, tracedepth = 0;
  while ( ( option_char = getopt_long( argc, argv, "?hvRHB"
				       "x:y:z:"
				       "a:"
				       "f:" // vipfilename
				       "m:", lOpt.long_options(), & option_index ) ) != EOF){
    switch ( option_char ) {
    case '?': lOpt.printHelp( argv[0] ); return 1; break;
    case 'h': lOpt.printHelp( argv[0] ); return 1; break;
    case 'v': verbose = true; break;
    case 'R': isRegion = true; break;
    case 'H': isHole = true; break;
    case 'B': isBoundary = true; break;
    case 'x': x = atof( optarg ); break;
    case 'y': y = atof( optarg ); break;
    case 'z': z = atof( optarg ); break;
    case 'a': area = atof( optarg ); break;
    case 'm': marker = atoi( optarg ); break;
    case 'f': vipFilename = optarg; break;
    default : cerr << "default value not defined" << endl;
    }
  }
  if ( argc < 2 ) { lOpt.printHelp( argv[0] ); return 1; }

  string worldName( argv[ argc-1 ] );
  string worldFileName( worldName.substr( 0, worldName.rfind( ".poly" ) ) + ".poly" );

  int dimension = findDomainDimension( worldFileName );

  BaseMesh *world;
  switch ( dimension ){
  case 2:
    world = new Domain2D();
    break;
  case 3:
    world = new Domain3D();
    break;
  default: return -1;
  }

  world->load( worldFileName );
  //world->load( worldFileName, 1e-6 );

  if ( isBoundary ){
    switch ( dimension ){
    case 2:
      TO_IMPL
//       for ( int i = 0; i < dynamic_cast< Domain2D *>( world )->edgeCount(); i ++ ){
// 	dynamic_cast< Domain2D *>( world )->edge( i ).setMarker( marker );
//       }
    case 3:
      for ( int i = 0; i < dynamic_cast< Domain3D *>( world )->facetCount(); i ++ ){
	dynamic_cast< Domain3D *>( world )->pFacet( i )->setBoundaryMarker( marker );
      }
      break;
    }
  } else {
    if ( vipFilename != NOT_DEFINED ){
      fstream file; if ( !openInFile( vipFilename, &file, true ) ){ return 1; }
      vector < string > row;
      while ( !file.eof() ){
	row = getNonEmptyRow( file );
	switch( row.size() ){
	case 0:
	  break;
	case 2:
	  world->createVIP( RealPos( toDouble( row[ 0 ] ), toDouble( row[ 1 ] ) ), marker, tolerance);
	  break;
	case 3:
	  switch ( dimension ){
	  case 2:
	    world->createVIP( RealPos( toDouble( row[ 0 ] ), toDouble( row[ 1 ] ) ), toInt( row[ 2 ] ), tolerance );
	    break;
	  case 3:
	    world->createVIP( RealPos( toDouble( row[ 0 ] ), toDouble( row[ 1 ] ), toDouble( row[ 2 ] ) ).round( 1e-6 ), marker, tolerance );
	    break;
	  } break;
	case 4:
	  world->createVIP( RealPos( toDouble( row[ 0 ] ), toDouble( row[ 1 ] ), toDouble( row[ 2 ] ) ), toInt( row[ 3 ] ), tolerance );
	  break;
	default:
	  cerr << WHERE_AM_I << " format unknown: " <<  row.size() << endl; return 1;
	  break;
	}
      }
      file.close();
    } else {
        RealPos pos( x, y, z );
        if ( dimension == 2 ){
            if ( fabs( z ) > TOLERANCE ) pos.setY( z );
        }
        if ( isHole ){
            world->createHole( pos );
        } else if ( isRegion ){
            world->createRegionMarker( pos, (double)marker, area );
        } else {
            world->createVIP( pos, marker, tolerance);
        }
    }
  }
  world->save( worldFileName );

  return 0;

}
