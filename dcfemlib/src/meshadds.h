// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#ifndef MESH_ADDS__H
#define MESH_ADDS__H MESH_ADDS__H

#include "mesh2d.h"
#include "domain2d.h"
#include "polygon.h"

int createCircle( Domain2D & mesh, const RealPos & pos, double radiusA, double radiusB,
		  int segments, double offset, int direction, bool edges, int marker = 0, double dx = 0 );

DLLEXPORT Polygon findAllNodesBetween( Mesh2D & mesh, Node * pNodeStart, Node * pNodeEnd, int marker );

void createEdges( Mesh2D & mesh, const Polygon & line, int marker );


/*! Convert tape measure coordinates into cartesian. Tape measure coordinates (tpm) are a vector of \RealPos, where the \RealPos.x() stands for the incremental distance of all preceding points and \RealPos.y() for a given topography. The first tpm[ 0 ].x() must 0.0. A cartesian offset can be given. ATM only conversion for 2D given., */
vector < RealPos > tapeMeasureToCartesian( const vector < RealPos > &tpm, const RealPos & cartOffset = RealPos( 0, 0, 0 ) );

/*! Convert cartesian coordinates to tape measure. The tape measure coordinates starts with (0.0, cart[0].y() ) */
vector < RealPos > cartesianToTapeMeasure( const vector < RealPos > & cart );


#endif

/*
$Log: meshadds.h,v $
Revision 1.5  2007/10/04 07:21:59  thomas
BERT + polytools codeblocks (dllexport)

Revision 1.4  2005/10/14 15:31:37  carsten
*** empty log message ***

Revision 1.3  2005/07/01 15:11:28  carsten
*** empty log message ***

Revision 1.2  2005/03/20 22:51:59  carsten
*** empty log message ***

Revision 1.1  2005/01/04 19:22:30  carsten
add in cvs

*/
