set(libdcfemlib_TARGET_NAME dcfemlib)

file (GLOB SOURCE_FILES RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}" *.cpp)
file (GLOB HEADER_FILES RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}" *.h)

###########################################################################
#                       SHARED LIBRARY SET UP
###########################################################################
add_library(${libdcfemlib_TARGET_NAME} SHARED ${SOURCE_FILES} ${HEADER_FILES_VAR})

include (GenerateExportHeader)

generate_export_header( ${libdcfemlib_TARGET_NAME}
             BASE_NAME ${libdcfemlib_TARGET_NAME}
             EXPORT_MACRO_NAME DLLEXPORT
             EXPORT_FILE_NAME ${libdcfemlib_TARGET_NAME}_export.h
             STATIC_DEFINE BERT_BUILT_AS_STATIC
)

set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Wno-unused-but-set-variable")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Wno-vla")
#set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Wno-sign-compare")
#set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Wno-return-type")



find_path (Triangle_INCLUDE_DIR triangle.h
    ${EXTERNAL_DIR}/include
)

find_library(Triangle_LIBRARIES
    NAMES
                triangle
                libtriangle
    PATHS
                ${Triangle_PREFIX_PATH}
                ${EXTERNAL_DIR}
    PATH_SUFFIXES
        lib
)

if (NOT Triangle_LIBRARIES OR NOT Triangle_INCLUDE_DIR)

    set (TRIANGLE_BUILD_DIR ${CMAKE_CURRENT_BINARY_DIR}/triangle)

    message(STATUS "Building Triangle: ${TRIANGLE_BUILD_DIR}")
    if(NOT EXISTS ${TRIANGLE_BUILD_DIR}/triangle.zip)
        file(DOWNLOAD http://www.netlib.org/voronoi/triangle.zip
            ${TRIANGLE_BUILD_DIR}/triangle.zip)
    endif()
    execute_process(COMMAND cmake -E tar -xz triangle.zip WORKING_DIRECTORY ${TRIANGLE_BUILD_DIR})
    execute_process(COMMAND sed -i -e "s/CC = cc/CC = gcc -fPIC/g" makefile WORKING_DIRECTORY ${TRIANGLE_BUILD_DIR})
    execute_process(COMMAND make trilibrary WORKING_DIRECTORY ${TRIANGLE_BUILD_DIR})
    execute_process(COMMAND ar cqs libtriangle.a triangle.o WORKING_DIRECTORY ${TRIANGLE_BUILD_DIR})

    find_path (Triangle_INCLUDE_DIR triangle.h
        ${TRIANGLE_BUILD_DIR}
    )
    find_library(Triangle_LIBRARIES libtriangle.a ${TRIANGLE_BUILD_DIR}
    )
endif()

message(STATUS "Triangle found: ${Triangle_INCLUDE_DIR} ${Triangle_LIBRARIES}")



include_directories(${Triangle_INCLUDE_DIR})
target_link_libraries(${libdcfemlib_TARGET_NAME} ${Triangle_LIBRARIES})
add_definitions(-DHAVE_TRIANGLE)




if(WIN32)
    add_definitions(-DMINGW)
    add_definitions(-DBUILDING_DLL=1)
endif(WIN32)


set(DCFEMLIB_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR})

target_link_libraries(${libdcfemlib_TARGET_NAME})

#add_dependencies(bert1 ${libdcfemlib_TARGET_NAME})

install(TARGETS ${libdcfemlib_TARGET_NAME} DESTINATION ${LIBRARY_INSTALL_DIR})
