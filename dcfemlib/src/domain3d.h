// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#ifndef DOMAIN3D__H
#define DOMAIN3D__H DOMAIN3D__H

#include "dcfemlib.h"
using namespace DCFEMLib;

#include "elements.h"
#include "mesh3d.h"

using namespace MyMesh;

#include "facet.h"

class Domain2D;

class DLLEXPORT Domain3D : public Mesh3D {
public:
  Domain3D( bool verbose = false ) : Mesh3D(), verbose_( verbose ) { }

  Domain3D( const string & filename )
    : Mesh3D() {
    load( filename );
    verbose_ = false;
  }

  Domain3D( const Domain3D & domain );

  ~Domain3D() {
    clear();
  }

  virtual void clear(){
    vecpFacets_.clear();
    vecpHoles_.clear();
    BaseMesh::clear();
  }

  DLLEXPORT virtual int load( const string & filename, int fromone, double snapTol = 1e-12 );
  virtual int load( const string & filename, double snapTol ){ return load( filename, 0, snapTol ); }
  virtual int load( const string & filename ){ return load( filename, 0 ); }
  DLLEXPORT virtual int save( const string & filename, int fromone );
  virtual int save( const string & filename ){ return save( filename, 0 ); }

  void hackClearDomain();
  int hackLoadTrianglePolyFile( const string & filename, int fromOne = 0 );

//   void insertRegionMarker( const RealPos & pos, double attribute, double dx = 0.0 ){
//     regions_.push_back( new RegionMarker( pos, attribute, dx ) );
//   }
//   void insertRegionMarker( const RegionMarker & marker ){ regions_.push_back( new RegionMarker( marker ) ); }
//   int regionCount() const { return regions_.size(); }
//   RegionMarker & region( int i ) const { return * regions_[ i ]; }
//   RegionMarker * pRegion( int i ) { return regions_[ i ]; }
//   vector < RegionMarker * > & regions() { return regions_ ; }

  void insertHole( const RegionMarker & marker ){ vecpHoles_.push_back( new RegionMarker( marker.pos(), -1, 0.0 ) ); }
  void insertHole( const RealPos & pos ){ vecpHoles_.push_back( new RegionMarker( pos, -1, 0.0 ) ); }
  int holeCount() const { return vecpHoles_.size(); }
  RegionMarker & hole( int i ) const { return * vecpHoles_[ i ]; }
  vector < RegionMarker * > & holes() { return vecpHoles_ ; }

  void createHole( const RegionMarker & hole ) { vecpHoles_.push_back( new RegionMarker( hole ) ); }
  void createHole( const RealPos & hole ) { vecpHoles_.push_back( new RegionMarker( hole ) ); }

  Facet * insertFacet( const Facet & facet ){ vecpFacets_.push_back( new Facet( facet ) ); return vecpFacets_.back(); }
  void insertpFacet( Facet * facet ){ vecpFacets_.push_back( facet ); }
  void insertFacets( const vector < Facet > & facets ){
    for ( int i = 0, imax = facets.size(); i < imax; i++ ) insertFacet( facets[ i ] );
  }
  Facet * createFacet( Node & n1, Node & n2, Node & n3, int bc ){
    return insertFacet( Facet( &n1, &n2, &n3, 1, -1, bc ) );
  }
  Facet * createFacet( Node & n1, Node & n2, Node & n3, Node & n4, int bc ){
    Polygon poly;
    poly.push_back( &n1 ); poly.push_back( &n2 );
    poly.push_back( &n3 ); poly.push_back( &n4 );
    return createFacet( poly, 0, 0, bc );
  }

  Facet * createFacet( const Polygon & polygon, int left = 0, int right = 0, int boundary = 0 ){
    return insertFacet( Facet( polygon, left, right, boundary ) );
  }

  Facet * createFacet( const Domain2D & d2, int marker = 0);
  
  int triangulateFacetOld( Facet & facet, double quality, bool verbose );
  int triangulateFacetOld( Facet & facet, bool verbose = false){ return triangulateFacetOld( facet, 0.0, verbose ); }

  int triangulateFacet( Facet & facet, bool verbose = false );

  int facetCount() const { return vecpFacets_.size(); }
  Facet & facet( int i ) const { return *vecpFacets_[ i ]; }
  Facet * pFacet( int i ){ return vecpFacets_[ i ]; }

  

  /*! This Method inserts a Node as a Very Important Point VIP on this domain. The point, respectivly node, will become a fixed node at the mesh. */
  void insertVIP( Node * node );

  /*! This Method creates a Very Important Point VIP at this domain. The point, respectivly node, will become a fixed node at the mesh.
   A pointer to the created node returns. */
  Node * createVIP( const RealPos & pos, int marker, double tolerance = TOLERANCE);

  Node * createVIP( const RealPos & pos, double tolerance ){ return createVIP( pos, 0, tolerance ); }

  Node * createVIP( const RealPos & pos ){ return createVIP( pos, 0, TOLERANCE ); }

  void createLineOfVIPs( const RealPos & start, const RealPos & end, int electrodeCount, int marker = 0 );

  void closeSurface( Node * n0, Node * n1, Node * n2, Node * n3, double zmin, int boundary = -2, int region = 1, double maxArea = 0.0 );

  /*! This Method inserts a Node at this domain. The node will become a fixed node at the mesh. If the node touch a polygon or facet the node will linked with this entity.*/
  void insertNode( Node * node );
  /*! This Method inserts a free Node at this domain. No check performed.*/
  void insertFreeNode( Node & node );
  /*! This Method inserts the Node at Polygon poly. No check performed.*/
  void insertNodeOnPolygon( Polygon & poly, Node & node );
  /*! This Method inserts the Node at Facet facet. No check performed.*/
  void insertNodeOnFacet( Facet & facet, Node & node );

  set < Node * > insertLine( const Line & line );
  void insertPlane( const Plane & plane );
  Polygon splitFacetWithLine( Facet & Facet, const Line & line );
  int splitFacetWithEdge( Facet & facet, const Polygon & edge );

  Node * createNodeOnFacet( Facet & facet, double x, double y, double z, int marker = 0,
			    double dx = 0.0, double dy = 0.0, double dz = 0.0 );

  Node * createNodeOnFacet( Facet & facet, RealPos pos, int marker = 0,
			    double dx = 0.0, double dy = 0.0, double dz = 0.0 ){
    return createNodeOnFacet( facet, pos.x(), pos.y(), pos.z(), marker, dx, dy, dz );
  }

  Node * createFreeNode( double x, double y, double z, int marker = 0,
			 double dx = 0.0, double dy = 0.0, double dz = 0.0 );

  /*!Check each facet. See the valid method. Returns 1 if all facets are valid else returns 0. If the verbose mode activated the method will givs warnings if a non-valid facet found.*/
  int checkAllFacets( bool verbose = true );

  /*! \it{Temp Workaround! Better system will come later.} \n
    Insert a copy of subdomain into this domain. Warning no check performed. \n
    Only use this method if you are sure that there is no intersection of edges or facets between this domain and subdomain. That means, the subdomain
    has located complete inside or outside this domain.*/
  void add( const Domain3D & subdomain );

  /*! \it{Temp Workaround! Better system will come later.} \n
    Insert a copy of subdomain into this domain. 1 check performed.\n
    This and Subdomain should consist only one polygone.
    Use this method if you are sure that just 1 facet of subdomain touch only 1 facet of this domain, no intersection of edges or facets.*/
  void link( const Domain3D & subdomain, double tolerance = TOLERANCE );

  /*! */
  void link( const Plane & plane );

  /*!*/
  void link( const Facet & facet, double tolerance = TOLERANCE );

  virtual void merge( const BaseMesh & subdomain, double tolerance = TOLERANCE, bool check = true ){
    merge( dynamic_cast< const Domain3D & >( subdomain), tolerance, check ); }
  void merge( const Domain3D & subdomain, double tolerance = TOLERANCE, bool check = true );
  void merge( const Mesh2D & mesh, int bc = 0 );


  /*!Returns a pointer to the Node at position pos is there no Node the method returns NULL.*/
  Node * nodeAt( const RealPos & pos, double tolerance = TOLERANCE );

  void switchXY();
  void closeFacets( Polygon & front, Polygon & back, int left, int right, int marker, bool close = false );

  Facet * createSimpleMainDomain( const RealPos & start, const RealPos & end );
  Facet * createSandbox( const RealPos & start, const RealPos & end, double region, double dx = 0 );

  void createBoundary( Facet & facet, const RealPos & start, const RealPos & end,
		       double region, double dx = 0 );

  int createTopographySurface( const Mesh3D & surfacemesh, double layer1 );
  void addSurface( const Mesh2D & surfacemesh, double z = 0.0 );

  DLLEXPORT int saveTetgenPolyFile( const string & filename, bool checkValid = true );
  int saveTetgenNodeFile( const string & filename );
  int saveTrianglePolyFile( const string & filename, bool checkValid = true );
  int saveGRUMMPBdryFile( const string & filename, bool checkValid = true );
  int saveOOGL_OFFFile( const string & filename );
  int saveVTK( const string & filename, bool verbose = false );
  int saveSTL( const string & filename, bool verbose = false );
  int saveSTLOld( const string & filename, bool verbose = false );
  int savePLC( const string & filename ){ return saveTetgenPolyFile(filename, true); }
  int loadWaveFrontObj( const string & filename );
  int loadSTL( const string & filename );
  int loadSTLBinary( const string & filename );

  void stretchOut( const Polygon & front, double dx, double dy, double dz );
  Polygon copyPolygon( const Polygon & poly );
  DLLEXPORT Polygon findAllNodesBetween( Node * pNodeStart, Node * pNodeEnd, int marker = -1, double tol = 1e-6);
  DLLEXPORT Polygon findAllNodesBetween( const RealPos & start, const RealPos & end );

    //template < class Mat > void transform( const Mat & mat ){
    virtual void transform( double mat[ 4 ][ 4 ] ){
        //std::cout << "Domain3d::transform " << std::endl;
        BaseMesh::transform( mat );

        for ( int i = 0, imax = regionCount(); i < imax; i ++ ) region( i ).pos().transform( mat );
        for ( int i = 0, imax = holeCount();   i < imax; i ++ ) hole( i ).pos().transform( mat );
        for ( int i = 0, imax = facetCount();  i < imax; i ++ ) facet( i ).transformHoles( mat );
    }

  void setPos( const RealPos & pos );

  void insertLayer( const Facet & surface, double depth, int region );

protected:
  vector< Facet * > vecpFacets_;
  //  vector< RegionMarker * > regions_;
  vector< RegionMarker * > vecpHoles_ ;
  RealPos pos_;
  bool verbose_;
};


DLLEXPORT Domain3D create3DWorld( const RealPos & size, int marker = 1, double area = 0);
DLLEXPORT Domain3D createCube( const RealPos & pos, int marker = 1, double area = 0, bool hole = false, bool close = true );
DLLEXPORT Domain3D createSphere( const RealPos & pos, int marker = 1, double radius = 1, double area = 0, bool hole = false );
DLLEXPORT Domain3D createCylinder( const RealPos & pos, int marker = 1, int nSegments = 12, double area = 0, bool hole = false, bool close = true );

DLLEXPORT void createCuboid( Domain3D & domain, int region, int regionleft, double dx, bool hole );

// vector< Facet > closeFacets( Polygon & front, Polygon & back, int left, int right, int marker, bool close = false );

DLLEXPORT Polygon extrude( Domain3D & domain, Polygon & vertsFront, double xoff, double yoff, double zoff );

DLLEXPORT void switchDirection( Polygon & polygone );

//void createCuboid( Domain3D & domain, int region = 1, int regionRight = -1, double dx = 0.0, bool hole = false );
DLLEXPORT void createZylinder( Domain3D & domain, int segments, int left, int right, int region, double dx = 0 );

DLLEXPORT int createSurfaceLayer( Mesh3D & polynodes, Domain3D & facets, Mesh2D & surfacemesh, double layer1 );

DLLEXPORT Polygon lookingForSubNodes( Mesh2D & source, Mesh3D & target, int edgemarker, Node * startnode, Node * endnode);


#endif // DOMAIN3D__H
/*
$Log: domain3d.h,v $
Revision 1.28  2011/01/24 11:13:05  thomas
added -O option into polyCreateCube
changed some example files (3dtank and acucar)

Revision 1.27  2010/02/08 17:38:33  carsten
*** empty log message ***

Revision 1.26  2010/01/31 10:49:54  carsten
*** empty log message ***

Revision 1.25  2009/02/03 10:15:00  carsten
*** empty log message ***

Revision 1.24  2009/02/02 10:43:04  carsten
*** empty log message ***

Revision 1.23  2008/10/05 13:25:00  carsten
*** empty log message ***

Revision 1.22  2008/02/28 10:47:33  carsten
*** empty log message ***

Revision 1.21  2007/10/04 07:21:59  thomas
BERT + polytools codeblocks (dllexport)

Revision 1.20  2007/09/13 15:47:14  carsten
*** empty log message ***

Revision 1.19  2007/09/11 17:28:58  carsten
*** empty log message ***

Revision 1.18  2007/04/04 16:53:54  carsten
*** empty log message ***

Revision 1.17  2007/02/21 19:44:05  carsten
*** empty log message ***

Revision 1.16  2006/12/03 18:46:03  carsten
*** empty log message ***

Revision 1.15  2006/08/27 18:58:03  carsten
*** empty log message ***

Revision 1.14  2006/06/06 09:41:49  carsten
*** empty log message ***

Revision 1.13  2006/04/17 20:58:03  carsten
*** empty log message ***

Revision 1.12  2006/02/06 14:12:34  carsten
*** empty log message ***

Revision 1.11  2006/01/31 18:27:58  carsten
*** empty log message ***

Revision 1.10  2006/01/22 21:02:06  carsten
*** empty log message ***

Revision 1.9  2005/07/28 19:43:43  carsten
*** empty log message ***

Revision 1.8  2005/07/14 15:03:37  carsten
*** empty log message ***

Revision 1.7  2005/07/01 15:11:28  carsten
*** empty log message ***

Revision 1.6  2005/06/21 14:53:59  carsten
*** empty log message ***

Revision 1.5  2005/03/29 16:57:46  carsten
add inversion tools

Revision 1.4  2005/03/20 22:51:59  carsten
*** empty log message ***

Revision 1.3  2005/03/08 16:04:45  carsten
*** empty log message ***

Revision 1.2  2005/02/22 21:51:43  carsten
*** empty log message ***

Revision 1.1  2005/02/07 13:33:13  carsten
*** empty log message ***

*/
