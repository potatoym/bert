// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//  
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//  

#include "facet.h"
#include "numfunct.h"
#include "stlmatrix.h"
#include "solver.h"
#include "mesh2d.h"
#include "domain2d.h"
using namespace MyVec;

#include <map>
using namespace std;

namespace MyMesh{

#include "setalgorithm.h"
ostream & operator << (ostream & str, Facet & facet){
  str << &facet  << endl;
  for (size_t i = 0; i < facet.size(); i ++){
    for (size_t j = 0; j < facet[ i ].size(); j ++){
      cout << facet[ i ][ j ]->id() << "\t";
    }
    cout << endl;
  }
  return str;
}

Facet::Facet(Node * v1, Node * v2, Node * v3, Node * v4, int left, int right, int boundary){
  Polygon poly;
  poly.push_back(v1);
  poly.push_back(v2);
  poly.push_back(v3);
  poly.push_back(v4);
  leftmarker_ = left;
  rightmarker_ = right;
  boundarymarker_ = boundary;
  push_back(poly);
}

Facet::Facet(vector < Polygon > & polygonVector, int left, int right, int boundary){
  // bedingung: all polygone sind colinear und die summe aller polygon.knoten ist coplanar
  leftmarker_ = left;
  rightmarker_ = right;
  boundarymarker_ = boundary;

  set < Node * > nodeSet;
  set < Edge * > edgeSet;
  for (int i = 0, imax = polygonVector.size(); i < imax; i ++){
    nodeSet.insert(polygonVector[ i ][ 0 ]);
    edgeSet.insert(new Edge(*polygonVector[ i ][ 0 ], *polygonVector[ i ][ 1 ]));
  }
  for (set< Node * >::iterator it = nodeSet.begin(); it != nodeSet.end(); it ++){
    //    cout << (*it)->id() << "\t" << (*it)->fromSet().size() + (*it)->toSet().size() << "\t";
  } 
  //  cout << endl;
  for (set< Edge *>::iterator it = edgeSet.begin(); it != edgeSet.end(); it ++){
    //cout << (*it)->nodeA().id() << " " << (*it)->nodeB().id()<< endl;
  } 

  Polygon newPoly;

  Edge * selectedEdge, * oldEdge;

  for (set< Edge *>::iterator it = edgeSet.begin(); it != edgeSet.end(); it ++){
    if (((*it)->nodeA().fromSet().size() + (*it)->nodeA().toSet().size()) == 2){ 
      //	 ((*it)->nodeB().fromSet().size() + (*it)->nodeB().toSet().size()) == 2 ){
      selectedEdge = (*it);
      newPoly.push_back(selectedEdge->pNodeA()); 
      newPoly.push_back(selectedEdge->pNodeB()); 
      edgeSet.erase(selectedEdge);
      break;
    }
  }
  if (newPoly.size() < 1) cerr << WHERE_AM_I << " keinen Einstiegspunkt gefunden " << endl;

  SetpEdges tmpEdges;
  double angle = 0.0;

  bool found = false;
  while(edgeSet.size() > 0){
    //cout << newPoly << endl;
    tmpEdges = newPoly.back()->toSet() + newPoly.back()->fromSet();
    tmpEdges.erase(selectedEdge);
    oldEdge = selectedEdge;
    if (tmpEdges.size() > 1){
      // cout << " Entscheidung treffen Last: " << oldEdge->pNodeA()->id()
      //	   << "--" << oldEdge->pNodeB()->id() << "\t" << newPoly.back()->id() <<endl;
      //      selectedEdge = *tmpEdges.begin();

      selectedEdge = *tmpEdges.begin();
      for (SetpEdges::iterator it2 = tmpEdges.begin(); it2 != tmpEdges.end(); it2 ++){
	angle = oldEdge->pNodeB()->angle(*oldEdge->pNodeA(), *(*it2)->pNodeA());
	//	cout << oldEdge->pNodeA()->id() << "-"
	//    << oldEdge->pNodeB()->id() << "-"
	//    << (*it2)->pNodeA()->id() << " " << angle << endl;
	if (isnan(angle)) {
	  angle = oldEdge->pNodeB()->angle(*oldEdge->pNodeA(), *(*it2)->pNodeB());
	  // cout << oldEdge->pNodeA()->id() << "-"
	  //    << oldEdge->pNodeB()->id() << "-"
	  //    << (*it2)->pNodeB()->id() << " " << angle << endl;
	}
	if (fabs(((angle /PI_) - rint(angle /PI_ ))) < TOLERANCE){
	  selectedEdge = *it2;
	  //	  cout << "selected: " << selectedEdge->pNodeA()->id() 
	  //   << "--" << selectedEdge->pNodeB()->id() << endl;
	}
      }
      if (newPoly.back() == selectedEdge->pNodeA())
	newPoly.push_back( selectedEdge->pNodeB());
      else newPoly.push_back( selectedEdge->pNodeA());
      edgeSet.erase(selectedEdge);
      
    } else {
      selectedEdge = *tmpEdges.begin();
      if (newPoly.back() == selectedEdge->pNodeA())
	newPoly.push_back( selectedEdge->pNodeB());
      else newPoly.push_back( selectedEdge->pNodeA());
      edgeSet.erase(selectedEdge);
    }
    
    for (size_t i = 0; i < newPoly.size() - 1; i ++){
      if (newPoly[ i ] == newPoly.back()){
	newPoly.pop_back();
	found = true;
	break;
      }
    }
    if (found) break;
  }
  //  cout << newPoly << endl;
  this->push_back(newPoly);
}

Facet::Facet(Node * v1, Node * v2, Node * v3, int left, int right, int marker){
  Polygon poly;
  poly.push_back(v1);
  poly.push_back(v2);
  poly.push_back(v3);
  leftmarker_ = left;
  rightmarker_ = right;
  boundarymarker_ = marker;
  push_back(poly);
}

bool Facet::operator == (const Facet & facet){
  return compare(facet);
}
bool Facet::operator != (const Facet & facet){
  return !compare(facet);
}

bool Facet::compare(const Facet & facet, double tol){
  //** Test if every def. Point of this Facet is duplicated in facet.
  //** Q&D zu aufwendig, geht bestimmt auch schneller
  set < Node *> p1 = collectAllNodes();
  set < Node *> p2 = facet.collectAllNodes();

  for (set < Node * >::iterator it1 = p1.begin(); it1 != p1.end(); it1 ++){ 
    for (set < Node * >::iterator it2 = p2.begin(); it2 != p2.end(); it2 ++){ 
      if (!((*it1)->pos() == (*it2)->pos())) return false;
    }
  }
  return true;
}

set < Node * > Facet::collectAllNodes() const {
  set < Node * > nodeSet;
  for (int i = 0, imax = (*this).size(); i < imax; i ++){
    for (int j = 0, jmax = (*this)[ i ].size(); j < jmax; j ++){
      nodeSet.insert((*this)[ i ][ j ]);
    }
  }
  return nodeSet;
}

RealPos Facet::findNodeVarZ(double xp, double yp){
  double x0 = (*this)[ 0 ][ 0 ]->x(), x1 = (*this)[ 0 ][ 1 ]->x(), x2 = (*this)[ 0 ][ 2 ]->x();
  double y0 = (*this)[ 0 ][ 0 ]->y(), y1 = (*this)[ 0 ][ 1 ]->y(), y2 = (*this)[ 0 ][ 2 ]->y();
  double z0 = (*this)[ 0 ][ 0 ]->z(), z1 = (*this)[ 0 ][ 1 ]->z(), z2 = (*this)[ 0 ][ 2 ]->z();

  STLMatrix A(2, 2);
  A[ 0 ][ 0 ] = x1 - x0;  A[ 0 ][ 1 ] = x2 - x0;
  A[ 1 ][ 0 ] = y1 - y0;  A[ 1 ][ 1 ] = y2 - y0;
  STLVector b(2);
  b[ 0 ] = xp - x0;
  b[ 1 ] = yp - y0;

  STLVector x(2);
  solveGaussDirect(A, x, b);

  double s = x[ 0 ], t = x[ 1 ];
  
  double zp = z0 + t * (z1 - z0) + s * (z2 - z0);

  return RealPos(xp, yp, zp);
}

Plane Facet::plane() const { 
  
  if ((*this).size() > 0 && (*this)[ 0 ].size() > 1){
    //  Triangle tritest(*(*this)[ 0 ][ 0 ], *(*this)[ 0 ][ 1 ], *(*this)[ 0 ][ 2 ]);
    
      Line line((*this)[ 0 ][ 0 ]->pos()-(*this)[ 0 ][ 0 ]->pos(), (*this)[ 0 ][ 1 ]->pos() -(*this)[ 0 ][ 0 ]->pos());
    
    for (uint i = 0; i < this->size(); i ++){
      for (uint j = 0; j < (*this)[ i ].size(); j ++){
//           std::cout <<line.distance((*this)[ i ][ j ]->pos() -(*this)[ 0 ][ 0 ]->pos())<<" "<< TOLERANCE *1e8<< std::endl;
//           std::cout <<line << " " << (*this)[ i ][ j ]->pos()<<" "<< TOLERANCE << std::endl;
          if (line.distance((*this)[ i ][ j ]->pos() -(*this)[ 0 ][ 0 ]->pos()) > TOLERANCE *1e8){
	  return Plane((*this)[ 0 ][ 0 ]->pos(), (*this)[ 0 ][ 1 ]->pos(), (*this)[ i ][ j ]->pos()); 
	}
      }
    }
  }
  //  cerr << WHERE_AM_I << " no plane found for this facet." << endl;
  return Plane();
}

bool Facet::valid(double tol) const {
  if ((*this)[ 0 ].size() < 2) return false;
  if ((*this)[ 0 ].size() == 2){
    //** Sonderfall f�r VIP;
    if ((*this)[ 0 ][ 0 ] == (*this)[ 0 ][ 1 ]) {
      return true;
    }
    return false;
  }
  return isCoplanar(tol);
}

bool Facet::isCoplanar(double tol, bool verbose) const {
  Plane plane(this->plane());
  for (int i = 0, imax = this->size(); i < imax; i ++){
    for (int j = 0, jmax = (*this)[ i ].size(); j < jmax; j ++){
      if (!plane.touch((*this)[ i ][ j ]->pos(), 1e-6)){
	if (verbose) cout <<	WHERE_AM_I << (*this)[i][j]->pos() 
			    << "\t" << plane << "\t" << plane.distance((*this)[i][j]->pos())<< endl;

// 	cout << (*this)[0][0]->pos() << (*this)[0][1]->pos() << (*this)[0][2]->pos() << endl;
// 	fstream file; openOutFile("facet.poly", &file);
// 	file << "3 1 0 1" << endl;
// 	for (int k = 0; k < 3; k ++){
// 	  file << k << "\t" 
// 	       <<(*this)[0][k]->x() << "\t" 
// 	       <<(*this)[0][k]->y() << "\t" 
// 	       <<(*this)[0][k]->z() << "\t" << 0 << endl;
// 	}
// 	file << "1\t1" << endl;
// 	file << "1\t0\t1" << endl;
// 	file << "3\t0\t1\t2" << endl;
// 	file << "0" << endl;
// 	file << "0" << endl;

// 	file.close();
	
// 	exit(0);
	return false;
      }
    }
  }
  return true;
}

bool Facet::insertNode(Node * node, bool verbose ){
    int position = 0;
//     cout << WHERE_AM_I << (*this) << " " << this->size() << " "  << " " << node->pos() << "  "<< node->id() << endl;
  
    //** for each polygon at this facet check if it touch the node;
    for (int i = 0, imax = this->size(); i < imax; i ++){
//        cout << WHERE_AM_I << (*this)[ i ].touch(node->pos()) << endl;
       
        if ((position = (*this)[ i ].touch(node->pos())) != -1){
//         cout << "insert node at position " << position << endl;
            (*this)[ i ].insertNode(node, position);
            return true;
        }
    }

    if (this->touch(node->pos(), false, 1e-6)){  
//          cout << "insert free node into facet " << endl;
        insertFreeNode(node, verbose);
        return true;
    }
    return false;
}

void Facet::snap(Node * node, bool verbose ){
  //if (verbose) std::cout << "dist 0:" << this->plane().distance(node->pos()) << std::endl;
  node->setPos(node->pos() - this->plane().norm() * this->plane().distance(node->pos()));
  //if (verbose) std::cout << "dist 1:" << this->plane().distance(node->pos()) << std::endl;
}

void Facet::insertFreeNode(Node * node, bool verbose){
  Polygon poly;
  //** doppelt fuer Tetgens Single Vertex Polygon;
  poly.push_back(node);
  poly.push_back(node);
  this->push_back(poly);
  this->snap(node, verbose);
}

void Facet::setBoundaryMarkerGivenByNodes(){
  //** new marker = largest nodemarker;
  double tmp = 0, boundary = 1e99;

  for (int i = 0, imax = size(); i < imax; i ++){
    for (int j = 0, jmax = (*this)[ i ].size(); j < jmax; j ++){
      if ((tmp = (*this)[i][j]->marker()) < boundary) boundary = tmp;
    }
  }
  setBoundaryMarker((int)boundary);
}

void Facet::switchDirection(){
  for (int i = 0, imax = this->size(); i < imax ; i ++) (*this)[i].switchDirection();
}

bool Facet::contains(Node * node){
  for (int i = 0, imax = this->size(); i < imax; i ++){
    if ((*this)[ i ].contains(node)) return true;
  }
  return false;
}

bool Facet::touch(const Facet & facet, double tol){
    
    Plane plane(this->plane());
    Plane planeNeg(plane.norm() * -1.0, plane.d() * -1.0);

    if (plane != facet.plane() && planeNeg != facet.plane()) return false;
  
  //if (plane.norm() != facet.plane().norm() && plane.norm() != facet.plane().norm() * -1.0) return false;

/*  std::cout << (*this)[ 0 ].size() << " " ;
  for (int i = 0, imax = (*this)[ 0 ].size(); i < imax ; i++){
    std::cout << (*this)[ 0 ][ i ]->id() << " " << (*this)[ 0 ][ i ]->pos(); 
  }
  std::cout << std::endl;

  std::cout << facet[ 0 ].size() << " " ;
     
  for (int i = 0, imax = facet[ 0 ].size(); i < imax ; i++){
    std::cout << facet[ 0 ][ i ]->id() << " " << facet[ 0 ][ i ]->pos();
  }
  std::cout << std::endl;*/
  
    for (int i = 0, imax = facet[ 0 ].size(); i < imax ; i++){
        if (!this->touch(facet[ 0 ][ i ]->pos(), 1, 1e-6)) {
/*       std::cout << "dist to faect:" << this->plane().distance(facet[ 0 ][ i ]->pos()) << std::endl;
       std::cout << this->touch(facet[ 0 ][ i ]->pos(), 1e-6) << std::endl;*/
        return false;
        }
    }
    return true;
}

bool Facet::touch(const RealPos & pos, bool withBoundary, double tol){
  //std::cout << "check: facet.touch(pos)-1: " << pos << std::endl;
  Plane plane(this->plane());
//   std::cout << "check: " << fabs(plane.distance(pos)) << std::endl;
//   std::cout << "check: " << plane << std::endl;
  if (!plane.touch(pos, tol)) return false;
//   std::cout << "check: facet.touch(pos): " << pos << std::endl;
  uint polyCount = (*this).size();   

  //** check if the pos touch the boundary of this facet
  for (uint j = 0; j < polyCount; j++){
    //     cout << plane << pos << (*this)[ j ].touch(pos) << endl;
    //    cout << WHERE_AM_I << plane << pos << (*this)[ j ].touch(pos) << endl;
    if ((*this)[ j ].touch(pos) > -1){
      if (withBoundary) return true; else return false;
    }
  }
  
  //** check if the pos touch the area of this facet
  RealPos start, next;
  double angleSum = 0.0;
  double sign = 0.0;
  int polyNodeCount = 0;
  bool isClosed = false;
  double angle = 0.0;
  
  //** for all edges of all polygons
  for (uint j = 0; j < polyCount; j++){
    polyNodeCount = (*this)[ j ].size();

    if (polyNodeCount > 2){
      isClosed = true;
      if (isClosed){
	for (uint i = 0, imax = polyNodeCount; i < imax; i++){
	  start = (*this)[ j ][ i ]->pos(); 
	  if (i < (*this)[ j ].size() - 1) next = (*this)[ j ][ i + 1 ]->pos(); 
	  else next = (*this)[ j ][ 0 ]->pos();
	  
	  //** If we are on a node, consider this inside;
	  if (pos == next || pos == start) {
          if (withBoundary) {
              return true; 
          } else {
              return false;
          }
      } 

	  angle = pos.angle(start, next); 
	  if (angle > tol){
	    if (pos.isClockwise(start, next)) sign = -1.0; else sign = 1.0;
	    angleSum +=  angle * sign;
	  }
	  //	  cout << WHERE_AM_I << start<< pos << next << angleSum *180.0/PI_ << endl;
	}
      }
    }
  }

  //std::cout << fmod(fabs(angleSum), PI_ * 2.0)  << "\t" << angleSum << "\t" << PI_ * 2.0 << endl;
  //** fmod fails ??;
  if (fabs((fabs(angleSum) / (PI_ * 2.0)) - rint(fabs(angleSum) / (PI_ * 2.0) )) < tol *1e4&&
       fabs(angleSum) > tol * 1e4){
    //        cout << WHERE_AM_I << " touch"<< endl;
    return true;
  } else {
//     std::cout << fabs((fabs(angleSum) / (PI_ * 2.0)) - rint(fabs(angleSum) / (PI_ * 2.0) )) 
// 	      << " " <<  fabs(angleSum) << " "<< tol  << " " << tol*1e4<< std::endl;
    
//    cout << WHERE_AM_I << " don't touch"<< endl;
    return false;
  }
}

list< RealPos > Facet::intersect(const Line & line, double tol){
  Plane plane(this->plane()); 

  list< RealPos > listIntersecPos;
  RealPos start, next, iPos;
  if ((iPos = plane.intersect(line, tol)).valid()){
    if (this->touch(iPos, tol)){ 
      //** there is only one intersectionpoint, the line is not coplanar to this facet
      listIntersecPos.push_back(iPos);
      return listIntersecPos;
    }
  }

  bool isHole = true;
  Line edge;
  int position = 0;
  map < double, RealPos > sortedIPos; 
  size_t polyCount = this->size(), polyNodeCount = 0; 
  //** for each polygon;
  for (size_t i = 0; i < polyCount; i++){
    polyNodeCount = (*this)[ i ].size();
    //** for each edge;
    for (size_t j = 0; j < polyNodeCount; j++){
      start = (*this)[ i ][ j ]->pos(); 
      if (j < (*this)[ i ].size() - 1) next = (*this)[ i ][ j + 1 ]->pos(); 
      else next = (*this)[ i ][ 0 ]->pos();
      
      if (start != next){
	edge.setPositions(start, next);
	if ((iPos = line.intersect(edge)).valid()){
	  position = edge.touch(iPos);
 	  if (position > 1 && position < 5){
	    sortedIPos[ rint(line.t(iPos) / (TOLERANCE * 1e-2)) * (TOLERANCE * 1e-2) ] = iPos;
	    //	    listIntersecPos.push_back(iPos.round(TOLERANCE *1e-2));
 	  }
 	}
      }
    }
  }
  for (map< double, RealPos >::iterator it = sortedIPos.begin(); it != sortedIPos.end(); it ++){
    listIntersecPos.push_back((*it).second);
  }
  for (list< RealPos >::iterator it = listIntersecPos.begin(); it != listIntersecPos.end(); it ++){
    cout << (*it) << endl;
  }
  return listIntersecPos;
}

list< RealPos > Facet::intersect(const Plane & plane, double tol){
  list < RealPos > listIntersecPos;
  TO_IMPL;
  return listIntersecPos;
}

void Facet::insert(const Line & line){
  TO_IMPL  
}

Polygon Facet::tangent(const Facet & face){
  TO_IMPL
    Polygon tangent;
  //   if ((*this) == face){
  
  //   }
   return tangent;
}

Mesh2D Facet::triangulate(double quality){
  Domain2D domain;
  
  map < Node *, Node * > nodeMap;

  for (int i = 0, imax = (*this).size(); i < imax; i ++){
    for (int j = 0, jmax = (*this)[ i ].size(); j < jmax; j ++){
      nodeMap[ (*this)[ i ][ j ] ] = domain.createVIP((*this)[ i ][ j ]->pos()) ;
    }
  }

  for (int i = 0, imax = (*this).size(); i < imax; i ++){
    if ((*this)[ i ].size() > 2){
      for (int j = 0, jmax = (*this)[ i ].size()-1; j < jmax; j ++){
	domain.createEdge(*nodeMap[ (*this)[ i ][ j ] ], *nodeMap[ (*this)[ i ][ j + 1] ], 0);
      }
      domain.createEdge(*nodeMap[ (*this)[ i ].back() ], *nodeMap[ (*this)[ i ].front() ], 0);
    }
  }
  for (int i = 0, imax = holeCount(); i < imax; i ++){
    domain.createHole(this->hole(i));
  }

  Plane facetPlane(this->plane()); 
  RealPos planeNormTarget(0.0, 0.0, 1.0);
  RealPos planeNorm(domain.norm());
  RealPos rot(planeNorm.cross(planeNormTarget));
  double phix = asin(rot.x()), phiy = asin(rot.y()), phiz = asin(rot.z());
  
  for (int i = 0; i < domain.nodeCount(); i ++){
    domain.node(i).pos().translate(this->plane().x0() * -1.0);
  }

  int count = 0;
  vector <RealPos > rotHistory;
  while (domain.norm() != planeNormTarget && domain.norm() != planeNormTarget*-1.0){
    count++;
    rot = domain.norm().cross(planeNormTarget);
//     cout << count << " " << rot << endl;
//     cout << domain.norm() << " " << planeNormTarget << endl;
    rotHistory.push_back(rot);
    phix = asin(rot.x()), phiy = asin(rot.y()), phiz = asin(rot.z());
    for (int i = 0; i < domain.nodeCount(); i ++){
      domain.node(i).pos().rotate(phix, phiy, phiz);
    }
    if (count > 16) {
      cerr << WHERE_AM_I << " something goes wrong" << endl;
      exit(0);
    }
  }
  //  domain.save("testFacet");
  Mesh2D mesh;

  if (domain.nodeCount() == 3){
    mesh.createNode(domain.node(0)); mesh.createNode(domain.node(1));  mesh.createNode(domain.node(2));
    mesh.createTriangle(mesh.node(0), mesh.node(1), mesh.node(2), mesh.cellCount(), 0.0);
  } else {
    mesh = domain.createMesh(quality);
  }
  //  mesh.save("testFacet");

  for (uint i = 0; i < rotHistory.size(); i ++){
    rot = rotHistory[ rotHistory.size() -1 - i ];
    phix = asin(rot.x()), phiy = asin(rot.y()), phiz = asin(rot.z());
    //    cout << rot << endl;

    for (int j = 0; j < mesh.nodeCount(); j ++){
      mesh.node(j).pos().rotateZ(-phiz);
      mesh.node(j).pos().rotateY(-phiy);
      mesh.node(j).pos().rotateX(-phix);
    }
  }

  for (int i = 0; i < mesh.nodeCount(); i ++){
    mesh.node(i).pos().translate(this->plane().x0());
  }

  return mesh;
}

} //namespace MyMesh

/*
$Log: facet.cpp,v $
Revision 1.16  2009/02/02 10:43:04  carsten
*** empty log message ***

Revision 1.15  2008/10/05 13:25:00  carsten
*** empty log message ***

Revision 1.14  2008/02/28 11:52:51  carsten
*** empty log message ***

Revision 1.13  2008/02/28 10:47:33  carsten
*** empty log message ***

Revision 1.12  2007/09/12 21:31:55  carsten
*** empty log message ***

Revision 1.11  2006/08/21 15:21:12  carsten
*** empty log message ***

Revision 1.10  2006/06/06 09:41:49  carsten
*** empty log message ***

Revision 1.9  2006/05/08 20:23:06  carsten
*** empty log message ***

Revision 1.8  2006/04/17 20:58:03  carsten
*** empty log message ***

Revision 1.7  2005/10/12 12:35:21  thomas
*** empty log message ***

Revision 1.6  2005/08/30 15:43:28  carsten
*** empty log message ***

Revision 1.5  2005/08/12 16:43:20  carsten
*** empty log message ***

Revision 1.4  2005/08/09 18:13:44  carsten
*** empty log message ***

Revision 1.3  2005/07/28 19:43:43  carsten
*** empty log message ***

Revision 1.2  2005/03/08 16:04:45  carsten
*** empty log message ***

Revision 1.1  2005/02/07 13:33:13  carsten
*** empty log message ***

*/
