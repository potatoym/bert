// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#include "sensitivity.h"

#include <set>

// #ifdef HAVE_LIBBOOST_THREAD
// #include <boost/thread.hpp>
// #endif

using namespace std;

typedef real_ REAL_;

int findModelCount( const BaseMesh & mesh ){
  set < int > attributeSet;
  for ( int i = 0, imax = mesh.cellCount(); i < imax; i ++ ){
    if ( (int)mesh.cell( i ).attribute() > 1 ){
      attributeSet.insert( (int)mesh.cell( i ).attribute() );
    }
  }
  return attributeSet.size();
}


void createSensitivityMatrixCapsulate( MyMatrix & S, const BaseMesh & mesh,
				       const MyMatrix & potMatrix,
				       const string & potFileBody,
				       const ElectrodeConfigVector & data,
				       const RVector & model,
				       const string & sensFileBody,
				       bool holdInMem, double maxMem, bool verbose,
				       int firstSensValue, int lastSensValue ){

  if ( verbose ) cout << "Calculate sensitivity matrix ..." << endl;

  int nElecs = data.electrodePositions().size();
  int nModel = findModelCount( mesh );
  int nData = data.size();

  if ( firstSensValue == -1 ) firstSensValue = 0;
  if ( lastSensValue == -1 ) lastSensValue = nData;
  if ( maxMem == -1 ) maxMem = 800;

  Stopwatch swatch; swatch.start();

  vector < double > kValues, weights;

  if ( mesh.dim() == 2 ){
    initKWaveList( mesh, kValues, weights, false );
  } else{
    kValues.push_back( 0.0 );  weights.push_back( 1.0 );
  }

  double SmemSize = (double)nData * (double)nModel * sizeof( REAL_ ) / 1024 / 1024;
  double FEMmemSize = (double)nElecs * (double)mesh.nodeCount() * (double)kValues.size() * sizeof( REAL_ ) / 1024 / 1024;

  bool outOfCore = false;
  bool newStyle = true;

  if ( maxMem < ( SmemSize + FEMmemSize ) && SmemSize < maxMem ){
    cout << "Calculate sensitivity matrix column based out of core" << endl;
    outOfCore = true;
  } else if ( SmemSize > maxMem ) {
    if ( mesh.dim() == 3  && FEMmemSize < maxMem ){
      outOfCore = true;
    } else {
      // wegen der convertierung von colbased > rowbased
      cout << "To few memory to calculate sensitivity matrix column based. Use row based." << endl;
      cout << "Maxmem: "<< maxMem << " SmemSize: " << SmemSize  << " FEMmemSize: " << FEMmemSize << endl;
      newStyle = false;
    }
  }

  if ( newStyle ){

    if ( potMatrix.size() != nElecs * kValues.size() ){

      const_cast< MyMatrix & >( potMatrix ).resize( nElecs * kValues.size() );
      MyMatrix potTmp;

      for ( uint i = 0; i < weights.size(); i ++ ){
        if ( potFileBody != NOT_DEFINED ){
	  if ( weights.size() == 1 ){
            load( nElecs, const_cast< MyMatrix & >(potMatrix), potFileBody, ".pot", -1, false );
	  } else{
	    load( nElecs, potTmp, potFileBody, ".pot", i, false );
	    for ( int j = 0; j < nElecs; j ++ ) const_cast< MyMatrix & >(potMatrix)[ j + i * nElecs ] = potTmp[ j ];
	  }
	} else {
	  cerr << WHERE_AM_I << " potFileBody == NOT_DEFINED no potentials for sensitivity calculation." << endl;
	}
      }
    }

    createSensitivityColMT( mesh, data, potMatrix, S, outOfCore, verbose );

    if ( outOfCore && mesh.dim() == 3 && SmemSize > maxMem ){
      cout << endl << "3D outOfCore col-based, read appropriate with sensmatdroptol " << endl;
    } else {
      if ( outOfCore ) {
	const_cast< MyMatrix & >(potMatrix).clear();
	S.resize( nData, RVector( nModel, 0.0 ) );

	RVector SColTmp;
	for ( int i = 0; i < nModel; i ++ ){
	  SColTmp.load( ( sensFileBody + "Col." + toStr( i ) ) );
	  for ( int j = 0; j < nData; j ++ ){
	    S[ j ][ i ] = SColTmp[ j ];
	  }
	}
      }

      if ( model.size() == S[ 0 ].size() ){
	for ( int i = 0; i < nData; i ++ ) S[ i ] /= model * model;
      }

      if ( verbose ) cout << "Save sensitivity matrix: " << endl;
      for ( int i = 0; i < nData; i ++ ) S[ i ].save( sensFileBody + "." + toStr( i ) );
    }
    if ( verbose ) cout << "\rSensitivity created: " << swatch.duration() << " seconds" << endl;
    swatch.stop();
    //  exit(1);
    return;
  }


  //*** ab hier old-style row-based, seeeehhhhhhrrrrr langsam
  size_t start = 0, end = 0;

  switch ( mesh.dim() ){
  case 2:{
    //** clustering atm disabled;
    // int clustersize = (int)floor( maxMem * 1024.0 * 1024.0 / sizeof( REAL_) / nModel );
    // int clustercount = (int)ceil( (double)nData / clustersize );
    //     if ( clustercount > 1 ){
    //       cout << " Clustercount: " << clustercount << " clustersize = " << clustersize << endl;
    //     }

    //     for ( int cluster = 0; cluster < clustercount; cluster ++ ){
    //       start = clustersize * cluster; end = start + clustersize;
    //      if ( end > nData ) end = nData;
    start = 0;
    end = nData;

    //** allocate sensM;
    if ( ( SmemSize + FEMmemSize ) < maxMem ){
      S.clear();
      S.resize( end-start, RVector( nModel, 0.0 ) );
    } else {
      S.resize( end-start, RVector( nModel, 0.0 ) );
      cerr << WHERE_AM_I << "Warning!! possible to less memory for sensititivity matrix needed: " << SmemSize + FEMmemSize  << endl;
    }

    MyMatrix tmpPotMatrix;
    for ( uint i = 0; i < weights.size(); i ++ ){

      if ( potFileBody != NOT_DEFINED ){
	load( nElecs, tmpPotMatrix, potFileBody, ".pot", i, false );
      } else {
	if ( potMatrix.size() == nElecs * weights.size() ) {
 	  tmpPotMatrix.clear();
 	  tmpPotMatrix.reserve( nElecs );

	  for ( int j = 0; j < nElecs; j ++ ){
	    tmpPotMatrix[ j ] = potMatrix[ i * (nElecs) + j ];
	  }

	} else {
	  cerr << WHERE_AM_I << " no valid potentials found " << endl;
	}
      }

      if ( verbose ) cout << "\r" << i << "/" << weights.size() -1 ;

      createSensitivityMatrix( S, // smatrix
			       mesh, // mesh
			       tmpPotMatrix, // pots
			       data, // config
			       model,
			       nModel,
			       NOT_DEFINED, // smatrix save filename
			       true, false, start, end-1, 2.0 * weights[ i ] );
      // holdInMem, verbose, first, last, weight for 2.5D
    }
    if ( sensFileBody != NOT_DEFINED ){
      if ( verbose ) cout << "Save sensitivity matrix:" << start << " " << end << endl;

      for ( size_t j = start; j < end; j ++ ){
	S[ j - start ].save( sensFileBody + "." + toStr( j ) );
      }
    }

    break;
  } // case 2
  case 3:
    if ( potFileBody != NOT_DEFINED ){
      if ( FEMmemSize < maxMem ){
	load( nElecs, const_cast< MyMatrix & >(potMatrix), potFileBody, ".pot" );
      }
    }
    createSensitivityMatrix( S, mesh, potMatrix, data, model, nModel, sensFileBody,
			     holdInMem, verbose, firstSensValue, lastSensValue, 1.0 );

    break;
  }

  if ( verbose ) cout << "\rSensitivity created: " << swatch.duration() << " seconds" << endl;
  swatch.stop();

}

void createSensitivityMatrix( MyMatrix & S, const BaseMesh & mesh, const MyMatrix & pot,
			      const ElectrodeConfigVector & data,
			      const RVector & model, int nModel,
			      const string & filebody,
			      bool holdInMem, bool verbose,
			      int firstSensValue, int lastSensValue, double weight ){

  Stopwatch swatch; swatch.start();
  if ( verbose ) cout << "create model sensitivity matrix ..." << endl;


  ///** Q&D
  bool loadPotMatrix = false;
  string potMatrixFilename = "primaryPot/interpolated/potential.P.";

  if ( pot.size() != data.electrodeCount() ){
    loadPotMatrix = true;
    cerr << " potentialMatrix.size() != electrodeConfig.electrodeCount(), force loading potmatrix: " << potMatrixFilename << endl;
  } else {
    if ( pot[ 0 ].size() < mesh.nodeCount() ){
      cerr << " potentialMatrix[ 0 ].size() < mesh.nodeCount() " << endl;
    }
  }

  vector < int > cellMapIndex = mesh.cellIndex();

  //** zaehle die verschiedenen Parameter;
  int nData = data.size();
  int nNodes = mesh.nodeCount();
  RVector p1( nNodes ), p2( p1 ), tmpVec;
  RVector allSensitivity( mesh.cellCount(), 0.0 );
  RVector modelSensitivity( nModel, 0.0 );

  RVector boundarySens( nData ), allSens( nData ), paraSens( nData );
  double sumSensSec = 0.0, sumSensPara = 0.0;

  int vcountmax = (long)rint(nData / 100), vcount = 0;
  if ( verbose ) cout << "\r0 %";
  int a = 0, b = 0, m = 0, n = 0;

  bool renewSens = false;

  RVector renewSensVec;
  //renewSens = renewSensVec.load("renewsens.vec", Ascii );
  if ( renewSens ){
    cout << "Found renewsens.vec, starting renew some sensitivities. " << endl;
    firstSensValue = 0;
    lastSensValue = renewSensVec.size();
  }

  int sensData = 0;

  for ( int i = firstSensValue, vcount = 0; i <= lastSensValue; i ++, vcount ++  ){
    if ( verbose ){
      if ( vcount == vcountmax ) { cout << "\r" << (int)ceil( ( i * 100.0 ) / ( nData ) ) << " %"; vcount = 0; }
    }
    sensData = i;
    if ( renewSens ) sensData = (int)renewSensVec[ i ];
    a = data[ sensData ].a(); b = data[ sensData ].b();
    m = data[ sensData ].m(); n = data[ sensData ].n();

    //    cout << sensData << " " << a << " " << b << " " << m << " " << n << endl;

    p1.clear(); p2.clear();

    if ( !loadPotMatrix ){
      if ( a > -1 ) p1 = pot[ a ];
      if ( b > -1 ) p1 -= pot[ b ];
      if ( m > -1 ) p2 = pot[ m ];
      if ( n > -1 ) p2 -= pot[ n ];
    } else {
      if ( a > -1 ) p1.load( potMatrixFilename + toStr( a ) + ".pot" );
      if ( b > -1 ) {
	tmpVec.load( potMatrixFilename + toStr( b ) + ".pot" );
	p1 -= tmpVec;
      }
      if ( m > -1 ) p2.load( potMatrixFilename + toStr( m ) + ".pot" );
      if ( n > -1 ) {
	tmpVec.load( potMatrixFilename + toStr( n ) + ".pot" );
	p2 -= tmpVec;
      }
      //       cout << i << "/" << lastSensValue << " eta: "
      // 	   << swatch.duration() << " "
      // 	   << swatch.duration() / ( (i+1) - firstSensValue )  * (lastSensValue- i) / 3600 << " h" << endl;
    }

    sensitivityDCFEM( mesh, p1, p2, allSensitivity, false );
    allSensitivity *= data[ sensData ].k();

    modelSensitivity.clear();
    for ( int j = 0, jmax = mesh.cellCount(); j < jmax; j ++ ){
      if ( cellMapIndex[ j ] > 1 ){
	modelSensitivity[ cellMapIndex[ j ] - 2 ] += allSensitivity[ j ];
      }
    }
    if ( model.size() > 0 ) modelSensitivity /= ( model * model );

    //model.save("model", Ascii );
    //allSensitivity.save("sens.mat", Ascii );
    //cout << sum( allSensitivity ) << endl;
    //exit(1);

    sumSensSec = sum( allSensitivity );
    sumSensPara = sum( modelSensitivity );

    boundarySens[ sensData ] = sumSensSec - sumSensPara;
    allSens[ sensData ] = sumSensSec;
    paraSens[ sensData ] = sumSensPara;

    if ( filebody != NOT_DEFINED ) modelSensitivity.save( filebody + "." + toStr( sensData ) );

    if ( verbose ) {
      cout << "\r" << sensData << "/" << lastSensValue
	   << " k =  " << data[ sensData ].k()
	   << " sumSens( all ) = " << sumSensSec
	   << " sumSens( Para ) = " << sumSensPara
           << " t: " << swatch.duration() << " eta: "
	   << swatch.duration() / ( (sensData+1) - firstSensValue )  * (lastSensValue- sensData) / 3600 << " h" << endl;
    }
    if ( holdInMem && filebody == NOT_DEFINED ){
      if ( S.size() <= (size_t)sensData - firstSensValue ) {
	cerr << WHERE_AM_I << " sensMatrix not allocated: " << S.size()
	     <<" / "<< (size_t)sensData - firstSensValue << endl; exit( 1 );
      } else {
	S[ sensData - firstSensValue ] += modelSensitivity * weight;
      }
    }
  }

  if ( verbose ) cout << endl << swatch.duration() << " time " << endl;
  if ( verbose ) cout << endl << "... finish." << endl;
  if ( lastSensValue != (int)nData -1 ) {
    cout << "sensitivitys from " << firstSensValue << " to "
	 << lastSensValue << " generated." << endl;
    //    exit( 0 );
  }
  if ( renewSens ) exit(0);
}

void createSensitivityCol( const BaseMesh & mesh, const ElectrodeConfigVector & data,
			   const vector < RVector > & pots, vector < RVector > & S, bool outOfCore, bool verbose ){

  string filebody( "sensM/smatrix" );

  vector < BaseElement * > para( mesh.findpCellAttribute( 2.0 , -1) );

  vector < int > cellMapIndex( para.size() );

  int nModel = 0;
  for ( int i = 0, imax = para.size(); i < imax; i ++ ) {
    cellMapIndex[ i ] =(int)para[ i ]->attribute();
    nModel = max( nModel, (int)para[ i ]->attribute() );
  }
  nModel -= 1;

  vector < vector < BaseElement * > > modelCells( nModel );
  for ( int i = 0, imax = para.size(); i < imax; i ++ ) {
    if ( (cellMapIndex[ i ] - 2) < 0 || (cellMapIndex[ i ] - 2) > nModel-1 ){
      cerr << WHERE_AM_I << " somenthing wrong." << (cellMapIndex[ i ] - 2) << " " << nModel << endl;
      exit( 0 );
    }
    modelCells[ cellMapIndex[ i ] - 2 ].push_back( para[ i ] );
  }


  int nData = data.size();
  int nElecs = data.electrodeCount();

  if ( !outOfCore ){
    if ( (long)S.size() != nData ) S.resize( nData, RVector( nModel, 0.0 ) );
    for ( uint i = 0; i < S.size(); i ++ ) S[ i ] *= 0.0;
  }

  vector < double > kValues, weights;
  if ( mesh.dim() == 2 ){
    initKWaveList( mesh, kValues, weights, false );
  } else{
    kValues.push_back( 0.0 );
    weights.push_back( 0.5 );
  }

  ElementMatrix S_i;

  Stopwatch swatch; swatch.start();

  for ( uint modelIdx = 0; modelIdx < modelCells.size(); modelIdx ++ ) {
    RVector SColTmp( nData, 0.0 );
    if ( verbose ) cout << "\r" << modelIdx << "/" << modelCells.size() - 1;
    for ( uint cellIdx = 0; cellIdx < modelCells[ modelIdx ].size(); cellIdx ++ ) {
      BaseElement * cell = modelCells[ modelIdx ][ cellIdx ];

      S_i.ux2uy2uz2( *cell );
      int cellNodeCount = cell->nodeCount();

      for ( int dataIdx = 0; dataIdx < nData; dataIdx ++  ){
	int a = data[ dataIdx ].a(), b = data[ dataIdx ].b();
	int m = data[ dataIdx ].m(), n = data[ dataIdx ].n();
	double k = data[ dataIdx ].k( );
	register double SValTmp = 0.0, sum = 0.0;
	double tmpPotA = 0.0, tmpPotM = 0.0, a_ij = 0.0;
	double tmp = 0.0;

	for ( uint kIdx = 0; kIdx < kValues.size(); kIdx ++ ){
	  tmpPotA = 0.0; tmpPotM = 0.0; sum = 0.0;
	  for ( int i = 0; i < cellNodeCount; i ++ ){

	    if ( a > -1 ) tmpPotA = pots[ a + nElecs * kIdx ][ S_i.i( i ) ];
	    if ( b > -1 ) tmpPotA -= pots[ b + nElecs * kIdx ][ S_i.i( i ) ];

	    for ( int j = 0; j < cellNodeCount; j ++ ){
	      a_ij = S_i.at( i, j );

	      if ( m > -1 ) tmpPotM = pots[ m + nElecs * kIdx ][ S_i.i( j ) ];
	      if ( n > -1 ) tmpPotM -= pots[ n + nElecs * kIdx ][ S_i.i( j ) ];

	      sum += a_ij * tmpPotA * tmpPotM;
	    }
	  }
	  SValTmp += sum * ( k * 2.0 * weights[ kIdx ] );
	}	// for each k
	SColTmp[ dataIdx ] += SValTmp;
      } // for each data
    } // for each cell per model

    if ( outOfCore ){
      SColTmp.save( ( filebody + "Col." + toStr( modelIdx ) ) );


      if ( verbose ) {
// 	cout << "\r" << sensData << "/" << lastSensValue
// 	     << " k =  " << data[ sensData ].k()
// 	     << " sumSens( all ) = " << sumSensSec
// 	     << " sumSens( Para ) = " << sumSensPara
// 	     << " t: " << swatch.duration() << " eta: "
	cout << "\t\t" << swatch.duration() / ( modelIdx )  * (modelCells.size() - modelIdx ) / 3600 << " h" ;
      }
    } else {
      for ( int dataIdx = 0; dataIdx < nData; dataIdx ++  ){
 	S[ dataIdx ][ modelIdx ] = SColTmp[ dataIdx ];
      }
    }
    //    exit(0);
  } // for each model
}

class CalcOneSensCol{
public:
  CalcOneSensCol( std::vector < RVector > & S, std::vector < std::vector < BaseElement * > > & modelCells,
		  const ElectrodeConfigVector & data,
		  const std::vector < RVector > & pots, std::vector < double > & weights,
		  bool ooc,  bool verbose )
    : S_( &S ), modelCells_( &modelCells ), data_( &data ), pots_( &pots ), weights_( &weights ),
      verbose_( verbose ), outOfCore_( ooc ) {

    S_i_ = new ElementMatrix();
    start_ = 0;
    end_ = 0;
    nElecs_ = data_->electrodeCount();
    nData_ = data_->size();
    filebody_ = "sensM/smatrix";
  }

  void operator ()(){
    calc();
  }
  ~CalcOneSensCol(){ }

  void setRange( uint modelStart, uint modelEnd ){  start_ = modelStart;  end_ = modelEnd;  }

  void calc(){
    for ( uint modelIdx = start_; modelIdx < end_; modelIdx ++ ) {
      RVector SColTmp( nData_, 0.0 );
      if ( verbose_ ) cout << "\r" << modelIdx << "/" << modelCells_->size() - 1;

      for ( uint cellIdx = 0; cellIdx < (*modelCells_)[ modelIdx ].size(); cellIdx ++ ) {
	BaseElement * cell = (*modelCells_)[ modelIdx ][ cellIdx ];

	S_i_->ux2uy2uz2( *cell );
	int cellNodeCount = cell->nodeCount();

	for ( uint dataIdx = 0; dataIdx < nData_; dataIdx ++  ){

	  int a = (*data_)[ dataIdx ].a(), b = (*data_)[ dataIdx ].b();
	  int m = (*data_)[ dataIdx ].m(), n = (*data_)[ dataIdx ].n();

	  double k = (*data_)[ dataIdx ].k( );

	  register double SValTmp = 0.0, sum = 0.0;
	  double tmpPotA = 0.0, tmpPotM = 0.0, a_ij = 0.0, tmp = 0.0;

	  for ( uint kIdx = 0; kIdx < weights_->size(); kIdx ++ ){
	    tmpPotA = 0.0; tmpPotM = 0.0; sum = 0.0;
	    for ( int i = 0; i < cellNodeCount; i ++ ){

	      if ( a > -1 ) tmpPotA = (*pots_)[ a + nElecs_ * kIdx ][ S_i_->i( i ) ];
	      if ( b > -1 ) tmpPotA -= (*pots_)[ b + nElecs_ * kIdx ][ S_i_->i( i ) ];

	      for ( int j = 0; j < cellNodeCount; j ++ ){
		a_ij = S_i_->at( i, j );

		if ( m > -1 ) tmpPotM = (*pots_)[ m + nElecs_ * kIdx ][ S_i_->i( j ) ];
		if ( n > -1 ) tmpPotM -= (*pots_)[ n + nElecs_ * kIdx ][ S_i_->i( j ) ];

		sum += a_ij * tmpPotA * tmpPotM;
	      }
	    }
	    SValTmp += sum * ( k * 2.0 * (*weights_)[ kIdx ] );
	  }	// for each k
	  SColTmp[ dataIdx ] += SValTmp;
	} // for each data
      } // for each cell per model
      if ( outOfCore_ ){
	SColTmp.save( ( filebody_ + "Col." + toStr( modelIdx ) ) );

	if ( verbose_ ) {
	  // 	cout << "\r" << sensData << "/" << lastSensValue
	  // 	     << " k =  " << data[ sensData ].k()
	  // 	     << " sumSens( all ) = " << sumSensSec
	  // 	     << " sumSens( Para ) = " << sumSensPara
	  // 	     << " t: " << swatch.duration() << " eta: "
	  //	  cout << "\t\t" << swatch.duration() / ( modelIdx )  * (modelCells.size() - modelIdx ) / 3600 << " h" ;
	}
      } else {
	for ( uint dataIdx = 0; dataIdx < nData_; dataIdx ++  ){
	  (*S_)[ dataIdx ][ modelIdx ] = SColTmp[ dataIdx ];
	}
      }
    }
  }

protected:
  vector < RVector > * S_;
  std::vector < std::vector < BaseElement * > > * modelCells_;
  const ElectrodeConfigVector   * data_;
  const std::vector < RVector > * pots_;
  std::vector < double >  * weights_;

  std::string filebody_;
  ElementMatrix *S_i_;

  bool verbose_;
  bool outOfCore_;

  uint start_;
  uint end_;

  uint nData_;
  uint nElecs_;

};

void createSensitivityColMT( const BaseMesh & mesh, const ElectrodeConfigVector & data,
			     const vector < RVector > & pots, vector < RVector > & S, bool outOfCore, bool verbose ){

  string filebody( "sensM/smatrix" );

  vector < BaseElement * > para( mesh.findpCellAttribute( 2.0 , -1) );
//std::cout<< "Parasize " << para.size() << std::endl;
  
  vector < int > cellMapIndex( para.size() );

  int nModel = 0;
  for ( int i = 0, imax = para.size(); i < imax; i ++ ) {
    cellMapIndex[ i ] =(int)para[ i ]->attribute();
  //  std::cout << i << " " << para[ i ]->attribute() << std::endl;
    nModel = max( nModel, (int)para[ i ]->attribute() );
  }
  nModel -= 1;

  vector < vector < BaseElement * > > modelCells( nModel );
  for ( int i = 0, imax = para.size(); i < imax; i ++ ) {
    if ( (cellMapIndex[ i ] - 2) < 0 || (cellMapIndex[ i ] - 2) > nModel-1 ){
      cerr << WHERE_AM_I << " somenthing wrong." << (cellMapIndex[ i ] - 2) << " " << nModel << endl;
      exit( 0 );
    }
    modelCells[ cellMapIndex[ i ] - 2 ].push_back( para[ i ] );
  }

  int nData = data.size();
  int nElecs = data.electrodeCount();

  if ( !outOfCore ){
    if ( S.size() != (uint)nData ) S.resize( nData, RVector( nModel, 0.0 ) );
    for ( uint i = 0; i < S.size(); i ++ ) S[ i ] *= 0.0;
  }

  vector < double > kValues, weights;
  if ( mesh.dim() == 2 ){
    initKWaveList( mesh, kValues, weights, false );
  } else{
    kValues.push_back( 0.0 );
    weights.push_back( 0.5 );
  }

  Stopwatch swatch; swatch.start();

  int threadFactor = (int)ceil( modelCells.size() / 4.0);
  CalcOneSensCol s0( S, modelCells, data, pots, weights, outOfCore, verbose );
  s0.setRange( 0, threadFactor );

  CalcOneSensCol s1( S, modelCells, data, pots, weights, outOfCore, verbose );
  s1.setRange( threadFactor, threadFactor * 2 );

  CalcOneSensCol s2( S, modelCells, data, pots, weights, outOfCore, verbose );
  s2.setRange( threadFactor * 2 , threadFactor * 3 );
  
  CalcOneSensCol s3( S, modelCells, data, pots, weights, outOfCore, verbose );
  s3.setRange( threadFactor * 3 , modelCells.size() );
  
#ifdef HAVE_LIBBOOST_THREAD
  boost::thread t0( s0 );
  boost::thread t1( s1 );
  boost::thread t2( s2 );
  boost::thread t3( s3 );
  t0.join();
  t1.join();
  t2.join();
  t3.join();
#else
  s0.calc();
  s1.calc();
  s2.calc();
  s3.calc();
#endif

//   for ( int modelIdx = 0; modelIdx < modelCells.size(); modelIdx ++ ) {

//     RVector SColTmp( nData, 0.0 );
//     if ( verbose ) cout << "\r" << modelIdx << "/" << modelCells.size() - 1;

//     s.setCol( modelIdx, SColTmp );

    //    s.calc();


//     if ( outOfCore ){
//       SColTmp.save( ( filebody + "Col." + toStr( modelIdx ) ) );

//       if ( verbose ) {
// // 	cout << "\r" << sensData << "/" << lastSensValue
// // 	     << " k =  " << data[ sensData ].k()
// // 	     << " sumSens( all ) = " << sumSensSec
// // 	     << " sumSens( Para ) = " << sumSensPara
// // 	     << " t: " << swatch.duration() << " eta: "
// 	cout << "\t\t" << swatch.duration() / ( modelIdx )  * (modelCells.size() - modelIdx ) / 3600 << " h" ;
//       }
//     } else {
//       for ( int dataIdx = 0; dataIdx < nData; dataIdx ++  ){
//  	S[ dataIdx ][ modelIdx ] = SColTmp[ dataIdx ];
//       }
//     }
//   } // for each model
}

void load(int n, MyMatrix & Matrix, const string & filebody, const string & suffix, int kIdx, bool verbose ){
  load( n, 0, Matrix, filebody, suffix, kIdx, verbose );
}

void load(int rows, int columns, MyMatrix & S, const string & filebody, const string & suffix, int kIdx, bool verbose ){
  for ( size_t i = 0; i < S.size(); i ++ ){
    S[ i ].resize( 0 ); S[ i ].clear();
  }
  S.clear();
  //verbose = true;
  int nLoads = rows;

  string fileBody( filebody );
  bool colBased = false;

  RVector tmp;
  //** a little bit Q&D
  if ( columns != 0 ){
    //** try rowbased loading
    if ( !tmp.load( filebody + "." + toStr( 0 ) + suffix )
	 || !tmp.load( filebody + "." + toStr( rows - 1 ) + suffix ) ){
      if ( tmp.load( filebody + "Col." + toStr( 0 ) + suffix ) &&
	   tmp.load( filebody + "Col." + toStr( columns - 1 ) + suffix ) ){
	colBased = true;
	fileBody += "Col";
	nLoads = columns;
	verbose = true;
	if ( verbose ) cout << "load matrix col-based" << endl;
      } else {
	cerr << WHERE_AM_I << " no valid " << filebody << " row or col based found." << endl;
	exit( 1 );
      }
    } else {
      if ( verbose ) cout << "load matrix row-based" << endl;
    }
  }


  string fileName;
  for ( int i = 0, imax = nLoads; i < imax; i ++ ){
    if ( verbose ) cout << "\r" << i + 1 << "/" << nLoads;

    if ( kIdx != -1 ){
      fileName = fileBody + "." + toStr( i ) + "_" + toStr( kIdx ) + suffix;
    } else {
      fileName = fileBody + "." + toStr( i ) + suffix;
    }

    if ( !tmp.load( fileName ) ){
      cerr << fileName << ": " << strerror( errno ) << endl;
      exit( 1 );
    }

    if ( colBased ){
      if ( i == 0 ) S.resize( rows, RVector( columns ) );

      if ( tmp.size() == rows ){
	for ( int j = 0; j < tmp.size(); j ++ ) S[ j ][ i ] = tmp[ j ];
      } else {
	cerr << fileName << " invalid size: " << tmp.size() << endl;
	exit( 1 );
      }
    } else {

      if ( i == 0 ) S.resize( rows, tmp );

      S[ i ] = tmp;
    }
  }
  if ( verbose ) cout << endl;
}

void save( MyMatrix & sensitivityMatrix, const string & filebody, bool verbose ){
  for ( int i = 0, imax = sensitivityMatrix.size(); i < imax; i ++ ){
    if ( verbose ) cout << "\r" << i + 1 << "/" << sensitivityMatrix.size();
    sensitivityMatrix[ i ].save( filebody + "." + toStr( i ) );
  }
  if ( verbose ) cout << endl;
}

void loadSparse( int rows, int columns, LinSparseVector & S, const string & filebody, double tol, double maxMem, bool verbose ){
  S.clear();

  char strdummy[ 1024 ];
  sprintf( strdummy, "%.1esmatrix", tol );
  string SSparseFileName( strdummy );
  if ( S.load( SSparseFileName ) ) {
    return;
  }

  int nLoads = rows;

  string fileBody( filebody );
  bool colBased = false;

  RVector tmp;
  //** a little bit Q&D
  if ( columns != 0 ){
    //** try rowbased loading
    if ( !tmp.load( filebody + "." + toStr( 0 ) ) || !tmp.load( filebody + "." + toStr( rows - 1 ) ) ){
      //** try columnbased
      if ( tmp.load( filebody + "Col." + toStr( 0 ) ) &&
	   tmp.load( filebody + "Col." + toStr( columns - 1 ) ) ){
	colBased = true;
	fileBody += "Col";
	nLoads = columns;
      } else {
	cerr << WHERE_AM_I << " no valid " << filebody << " row or col based found." << endl;
      }
    }
  }

  if ( verbose ) cout << "Create sparse sensitivity matrix with droptolerance: " << tol << endl;

  linSparseMatElement el;
  size_t count = 0;
  S.resize( (long)( maxMem * 1024 * 1024 ) / sizeof( linSparseMatElement ) );
  S.setCols( columns );

  for ( int i = 0; i < nLoads; i++ ){

    tmp.load( fileBody + "." + toStr( i ), true );

    if ( columns == -1 && !colBased ){
      S.setCols( tmp.size() );
    }

    for ( int j = 0; j < tmp.size(); j ++ ){
      if ( colBased ){
	el.row = j, el.col = i, el.val = tmp[ j ];
      } else {
	el.row = i, el.col = j, el.val = tmp[ j ];
      }

      if ( isinf( el.val ) || isnan( el.val ) || fabs(el.val) > 1 ){
	cerr << "S.load " << el.row << " " << el.col << "  " << el.val << endl;
      } else if ( fabs( el.val ) > tol ){
	count++;
	if ( count > S.size() ) {
	  cerr << WHERE_AM_I << " memory limit reached." << endl;
	  exit(0);
	}
	S[ count - 1 ] = el;
	if ( count%100000 == 0 && verbose ){
	  cout << "\r" << i + 1 << "/ " << nLoads << " ("
	       << count * sizeof( linSparseMatElement ) / 1024 / 1024 << "/"
	       << S.memSize() / 1024 / 1024 << " MB)";
	}
      }
      //      if ( fabs( tmp[ j ] ) > tol ) S[ i ][ j ] = tmp[ j ];
    }
  }

  S.resize( count );
  if ( verbose ) cout << endl << "Number of non-zero elements = " << S.size()
		      << " " << S.memSize() / 1024/1024 << " MB" << endl;
  
  if ( S.size() == 0 ){
      std::cerr << "No matrix elements found. Try decrease drop tolerance."<< std::endl;
  }
  
  S.save( SSparseFileName );
}

void createSensitivityMatrixAnalyt( const BaseMesh & mesh,
				    const ElectrodeConfigVector & data,
				    const string & filebody,
				    bool holdInMem, bool verbose ){

//   Stopwatch swatch; swatch.start();
//   vector < RVector > sensMatrix;

//     vector < int > cellMapIndex = mesh.cellIndex();
// //   set < int > attributeSet;

// //   for ( int i = 0, imax = mesh.cellCount(); i < imax; i ++ ){
// //     if ( (int)mesh.cell( i ).attribute() > 1 ){
// //       attributeSet.insert( (int)mesh.cell( i ).attribute() );
// //     }
// //   }
// //   int nModel = attributeSet.size();
// //   int nData = data.size();


//   RVector modelSensitivity( nModel, 0.0 );
//   RVector allSensitivity( mesh.cellCount(), 0.0 );

//   char strdummy[ 128 ];

//   for ( size_t i = 0; i < nData; i ++){

//     TO_IMPL
// //     sensitivityAnalyt( mesh,
// // 		       data.posA( i ),
// // 		       data.posB( i ),
// // 		       data.posM( i ),
// // 		       data.posN( i ),
// // 		       allSensitivity );
//     allSensitivity *= data[ i ].k();

//     modelSensitivity.clear();
//     for ( int i = 0, imax = mesh.cellCount(); i < imax; i ++ ){
//       if ( cellMapIndex[ i ] > 1 ){
// 	modelSensitivity[ cellMapIndex[ i ] - 2 ] += allSensitivity[ i ];
//       }
//     }
//     sensMatrix.push_back( modelSensitivity );
//     if ( !holdInMem ) {
//       if ( filebody != NOT_DEFINED ){
// 	sprintf( strdummy, "%s.%d", filebody.c_str(), i );
// 	sensMatrix.back().save( strdummy );
//       }
//       sensMatrix.back().resize( 0 );
//     }
//     if (  fabs( sum( allSensitivity ) ) > 10 ) {
//       cout << endl << i << "/" << nData -1
// 	   << " k =  " << data[ i ].k()
// 	   << " sumSens( all ) = " << sum( allSensitivity )
// 	   << " sumSens( Para ) = " << sum( modelSensitivity );
//       cerr << "Sum > 10 "<< endl;
//       exit(-1);
//     }
//     if ( verbose ) {
//       cout << "\r" << i << "/" << nData -1
// 	   << " k =  " << data[ i ].k()
// 	   << " sumSens( all ) = " << sum( allSensitivity )
// 	   << " sumSens( Para ) = " << sum( modelSensitivity );
//     }
//   }

// //   for ( int i = 0; i < nData; i ++ ){
// //     if ( holdInMem ) {
// //       if ( filebody != NOT_DEFINED ){
// // 	sprintf( strdummy, "%s.%d", filebody.c_str(), i );
// // 	sensMatrix[ i ].save( strdummy );
// //       }
// //     }
// //   }
}

/*
$Log: sensitivity.cpp,v $
Revision 1.41  2010/02/05 17:22:00  carsten
transfer

Revision 1.40  2008/12/11 15:14:13  carsten
*** empty log message ***

Revision 1.39  2008/02/28 10:47:33  carsten
*** empty log message ***

Revision 1.38  2008/02/11 11:16:32  carsten
*** empty log message ***

Revision 1.37  2007/10/09 14:55:19  carsten
*** empty log message ***

Revision 1.36  2007/10/04 13:09:54  carsten
*** empty log message ***

Revision 1.35  2007/10/04 07:21:59  thomas
BERT + polytools codeblocks (dllexport)

Revision 1.34  2007/09/11 17:28:58  carsten
*** empty log message ***

Revision 1.33  2007/04/04 16:53:54  carsten
*** empty log message ***

Revision 1.32  2007/03/15 18:04:14  carsten
*** empty log message ***

Revision 1.30  2007/03/07 16:38:14  carsten
*** empty log message ***

Revision 1.29  2007/02/21 19:44:05  carsten
*** empty log message ***

Revision 1.28  2007/01/25 17:38:13  carsten
*** empty log message ***

Revision 1.27  2007/01/18 12:59:13  carsten
*** empty log message ***

Revision 1.26  2007/01/18 12:53:59  carsten
*** empty log message ***

Revision 1.25  2007/01/18 12:51:49  carsten
*** empty log message ***

Revision 1.24  2007/01/18 12:46:00  carsten
*** empty log message ***

Revision 1.23  2007/01/18 12:44:55  carsten
*** empty log message ***

Revision 1.22  2007/01/18 12:43:24  carsten
*** empty log message ***

Revision 1.21  2007/01/18 12:41:06  carsten
*** empty log message ***

Revision 1.20  2007/01/18 12:40:13  carsten
*** empty log message ***

Revision 1.19  2007/01/17 22:16:34  carsten
*** empty log message ***

Revision 1.17  2006/12/28 21:55:29  carsten
*** empty log message ***

Revision 1.16  2006/11/20 14:32:25  carsten
*** empty log message ***

Revision 1.15  2006/11/08 19:53:24  carsten
*** empty log message ***

Revision 1.14  2006/10/08 14:11:38  carsten
*** empty log message ***

Revision 1.13  2006/09/22 11:13:03  carsten
*** empty log message ***

Revision 1.12  2006/09/22 11:05:28  carsten
*** empty log message ***

Revision 1.11  2006/08/02 18:51:14  carsten
*** empty log message ***

Revision 1.9  2006/07/27 14:52:08  carsten
*** empty log message ***

Revision 1.8  2006/07/20 16:05:47  carsten
*** empty log message ***

Revision 1.6  2005/12/15 15:15:13  carsten
*** empty log message ***

Revision 1.5  2005/10/30 18:53:04  carsten
*** empty log message ***

Revision 1.4  2005/07/19 14:24:04  carsten
*** empty log message ***

Revision 1.3  2005/07/14 17:24:51  carsten
*** empty log message ***

Revision 1.2  2005/07/14 09:20:02  carsten
*** empty log message ***

Revision 1.1  2005/07/13 17:05:21  carsten
*** empty log message ***

*/
