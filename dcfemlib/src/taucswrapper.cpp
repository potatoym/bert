// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//  
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//  

#include "taucswrapper.h"

#include <cstdio>
#include <cstdlib>
#include <complex>
#include <iostream>

using namespace std;
using namespace MyVec;

#ifdef HAVE_TAUCS
#ifdef __cplusplus
extern "C" {
#include <taucs.h>

  //#define taucs_free( x ) free(x);
}

#endif // __cplusplus

void fill( const RDirectMatrix & A, taucs_ccs_matrix & ATaucs ){
  int dim = A.size();

  taucs_double * vals  = ATaucs.values.d;
  int * rowIndex = ATaucs.rowind;
  int * colPtr = ATaucs.colptr;
  
  int k = 0, index = 0;
  colPtr[ 0 ] = 0;
  for ( int i = 0; i < dim; i++ ) {
    for ( int j = 0, jmax = (int)A.mat[ i ][ 0 ]; j < jmax; j ++ ){
      index = (int)(A.mat[ i ][ 2 * j + 1 ] - 1);
      if ( index >= i ){
	rowIndex[ k ] = index;
	vals[ k ] = A.mat[ i ][ 2 * j + 2 ];
	k++;
      }
    }
    colPtr[ i + 1 ] = k;
  }
  //  taucs_ccs_write_ijv(&ATaucs, "A.taucsmatrix" );
}
#endif //HAVE_TAUCS

TaucsWrapper::TaucsWrapper( const RDirectMatrix & S, bool verbose ) 
  : SolverWrapper( S, verbose ) {
#ifdef HAVE_TAUCS
  preordering_ = false;
  preorderStyle_ = "none";
  logfileName_ = "none";

  initialize_( S );
  factorise();

  return;
#endif //HAVE_TAUCS
  cerr << WHERE_AM_I << " Taucs not installed" << endl;
}

TaucsWrapper::TaucsWrapper( const RDirectMatrix & S, const string & ordertype, bool verbose ) 
  : SolverWrapper( S, verbose ) {
#ifdef HAVE_TAUCS
  preorderStyle_ = ordertype;
  logfileName_ = "none";

  initialize_( S );
  preordering( );
  factorise( );

  return;
#endif //HAVE_TAUCS
  cerr << WHERE_AM_I << " Taucs not installed" << endl;
}

TaucsWrapper::TaucsWrapper( const RDirectMatrix & S, const string & ordertype, double dropTol, double tolerance, bool verbose ) : SolverWrapper( S, verbose ) {
  dropTol_ = dropTol;
  tolerance_ = tolerance;
  preorderStyle_ = ordertype;
  logfileName_ = "none";

  initialize_( S );
  preordering( );
  factorise( );
#ifdef HAVE_TAUCS
  return;
#endif //HAVE_TAUCS
  cerr << WHERE_AM_I << " Taucs not installed" << endl;
}

TaucsWrapper::~TaucsWrapper(){
#ifdef HAVE_TAUCS
  if ( ! dummy_ ){
    
    taucs_ccs_free( static_cast< taucs_ccs_matrix* >( A_ ) );
    
    if ( preordering_ ){
      taucs_ccs_free( static_cast< taucs_ccs_matrix* >( AP_ ) );
    }

    if ( dropTol_ > 0.0 ){
      taucs_ccs_free( static_cast< taucs_ccs_matrix* >( L_ ) );
    } else {
      taucs_supernodal_factor_free( L_ );
    }

//     delete [] b_;
//     delete [] x_; 
//     delete [] bP_;
//     delete [] xP_;

    free( b_ );
    free( bP_ );

    free( x_ );
    free( xP_ );

    free( perm_ );
    free( invperm_ );
//     taucs_free( x_ );
//     taucs_free( b_ );
//     taucs_free( bP_ );
//     taucs_free( xP_ );
  }
  return;
#endif //HAVE_TAUCS
  cerr << WHERE_AM_I << " Taucs not installed" << endl;
}

int TaucsWrapper::initialize_( const RDirectMatrix & S ){
#ifdef HAVE_TAUCS
  //  cout << WHERE_AM_I << " " << verbose_ << endl;
//   S.save( "S.mat" ); exit(0);

  if ( dummy_ ) return 0;

  if ( verbose_ ) logfileName_ = "stdout";

  taucs_logfile( const_cast< char *>(logfileName_.c_str()) );
  
  dim_ = S.size();
  nVals_ = S.valueCount();
  nVals_ -= (nVals_ - dim_) / 2 -1;

  b_ = calloc( dim_, ( TAUCS_DOUBLE ) ); assert( b_ );
  x_ = calloc( dim_, ( TAUCS_DOUBLE ) ); assert( x_ );
  bP_ = calloc( dim_,( TAUCS_DOUBLE ) ); assert( bP_ );
  xP_ = calloc( dim_,( TAUCS_DOUBLE ) ); assert( xP_ );
  //  b_ = new void * [ dim_ * sizeof( TAUCS_DOUBLE )]; assert( b_ );
//   x_ = new void *[ dim_ * sizeof( TAUCS_DOUBLE ) ]; assert( x_ );
//   bP_ = new void *[ dim_ * sizeof( TAUCS_DOUBLE ) ]; assert( bP_ );
//   xP_ = new void *[ dim_ * sizeof( TAUCS_DOUBLE ) ]; assert( xP_ );
  
  A_ = taucs_ccs_create( dim_, dim_, nVals_, TAUCS_DOUBLE | TAUCS_SYMMETRIC | TAUCS_LOWER);

  fill( S, *(taucs_ccs_matrix*)A_ );
  return 1;
#endif //HAVE_TAUCS
  cerr << WHERE_AM_I << " Taucs not installed" << endl;
  return 0;
}

int TaucsWrapper::preordering( ){
#ifdef HAVE_TAUCS

  if ( dummy_ ) return 0;

  preordering_ = true;

  
  taucs_ccs_order( static_cast< taucs_ccs_matrix* >(A_), 
 		   & perm_, & invperm_, const_cast< char *>( preorderStyle_.c_str() ) );

  //  taucs_ccs_order( static_cast< taucs_ccs_matrix* >(A_), & perm_, & invperm_, "metis" );

  if ( !perm_ ) taucs_printf( "taucs_factor: ordering failed\n" );
  AP_ = taucs_ccs_permute_symmetrically( static_cast< taucs_ccs_matrix* >(A_), perm_, invperm_ );

  return 0;
#endif //HAVE_TAUCS
  cerr << WHERE_AM_I << " Taucs not installed" << endl;
  return -1;
}

int TaucsWrapper::factorise(){
#ifdef HAVE_TAUCS
  if ( dummy_ ) return 0;

  if ( dropTol_ == 0.0 ){
    if ( preordering_ ){ 
      L_ = taucs_ccs_factor_llt_mf_maxdepth( static_cast< taucs_ccs_matrix* >(AP_), 0 );
    } else {
      L_ = taucs_ccs_factor_llt_mf_maxdepth( static_cast< taucs_ccs_matrix* >(A_), 0 );
    }
  } else {
    if ( preordering_ ){ 
      L_ = taucs_ccs_factor_llt( static_cast< taucs_ccs_matrix* >(AP_), dropTol_, 0 );
    } else {
      L_ = taucs_ccs_factor_llt( static_cast< taucs_ccs_matrix* >(A_), dropTol_, 0 );
    }
  }
  //  cout << WHERE_AM_I << endl;  
  return 0;
#endif //HAVE_TAUCS
  cerr << WHERE_AM_I << " Taucs not installed" << endl;
  return -1;
}

int TaucsWrapper::solve( const RVector & rhs, RVector & solution ){
#ifdef HAVE_TAUCS
  if ( dummy_ ) return 0;

  if ( solution.size() != dim_ ) solution.resize( dim_ );

  for ( int i = 0; i < dim_; i ++ ){
    ((taucs_double*)b_)[ i ] = rhs[ i ];
    if ( preordering_ ){ 
      ((taucs_double*)xP_)[ i ] = 0.0;
    }
  }

  if ( preordering_ ){ 
    taucs_vec_permute( dim_, static_cast< taucs_ccs_matrix* >(A_)->flags, (char*)b_, (char*)bP_, perm_ );
    if ( dropTol_ == 0.0 ){
      taucs_supernodal_solve_llt( L_, xP_, bP_ );
    } else {
      taucs_conjugate_gradients( static_cast< taucs_ccs_matrix* >(AP_), taucs_ccs_solve_llt, L_, xP_, bP_, dim_, tolerance_ );
    }
    taucs_vec_ipermute( dim_, static_cast< taucs_ccs_matrix* >(A_)->flags, (char*)xP_, (char*)x_, perm_ );
  } else {
    if ( dropTol_ == 0.0 ){
      taucs_supernodal_solve_llt( L_, x_, b_ );
    } else {
      taucs_conjugate_gradients( static_cast< taucs_ccs_matrix* >(A_), taucs_ccs_solve_llt, L_, x_, b_, dim_, tolerance_ );
    }
  }
  for ( int i = 0; i < dim_; i ++ ) solution[ i ] = ((taucs_double*)x_)[ i ];
  
  return 0;
#endif //HAVE_TAUCS
  cerr << WHERE_AM_I << " Taucs not installed" << endl;
  return -1;
}

// int solveWithTaucsICCG( RDirectMatrix & S, const string & filebody, const vector < int > & vecRhs, 
// 			double dropTol, double tol, int maxiter, bool verbose ){
// #ifdef HAVE_TAUCS

//   //  char * opt_log = "stdout";  taucs_logfile( opt_log );

//   Stopwatch swatch; swatch.start();
//   int dim = S.size();
  
//   int nVals = S.valueCount();
//   nVals -= (nVals - dim) / 2;
//   taucs_ccs_matrix * A = taucs_ccs_create( dim, dim, nVals, TAUCS_DOUBLE | TAUCS_SYMMETRIC | TAUCS_LOWER);
//   fill( const_cast< const RDirectMatrix & >( S ), *A );
//   S.clear();

//   char strdummy[ 128 ];
//   bool solveDirect = false, solveOOC = false;
//   if ( dim < 200000 || dropTol == 0.0) solveDirect = true;

//   int * perm, * invperm;
//   //taucs_ccs_order( A, &perm, &invperm, "metis" );
//   //    taucs_ccs_order( A, &perm, &invperm, "identity" );
//   taucs_ccs_order( A, &perm, &invperm, "genmmd" );
//   //  taucs_ccs_order( A, &perm, &invperm, "amd" );
//   //taucs_ccs_order( A, &perm, &invperm, "metis" );
//   //  taucs_ccs_order( A, &perm, &invperm, "colamd" ); // nicht f�r sym oder herm. Matrix
//   //  taucs_ccs_order( A, &perm, &invperm, "treeorder" );
//   //  taucs_ccs_order( A, &perm, &invperm, "md" );
//   //  taucs_ccs_order( A, &perm, &invperm, "mmd" );

//   if ( !perm ) taucs_printf( "taucs_factor: ordering failed\n" );
//   taucs_ccs_matrix * AP = taucs_ccs_permute_symmetrically( A, perm, invperm );

//   void * L = NULL;
//   if ( solveDirect ) {
//     if ( solveOOC ){
//       L = taucs_io_create_multifile( "taucsOOC" );
//       double opt_ooc_memory = taucs_available_memory_size();
//       cout << opt_ooc_memory << endl;
//       taucs_ooc_factor_llt( AP, (taucs_io_handle*)L, opt_ooc_memory * 1024 );
//     } else {
//       L = taucs_ccs_factor_llt_mf_maxdepth( AP, 0 );
//     }
//   } else {
//     L = taucs_ccs_factor_llt( AP, dropTol, 0 );
//   }

//   cout << "precond finish: " << swatch.duration() << endl;  swatch.stop();
  
//   RVector xVec( dim );
//   void * b = new void *[ dim * sizeof( TAUCS_DOUBLE ) ]; assert( b );
//   void * x = new void *[ dim * sizeof( TAUCS_DOUBLE ) ]; assert( x );
//   void * bP = new void *[ dim * sizeof( TAUCS_DOUBLE ) ]; assert( bP );
//   void * xP = new void *[ dim * sizeof( TAUCS_DOUBLE ) ]; assert( xP );
//   for ( size_t elec = 0; elec < vecRhs.size(); elec ++ ){
//     if ( vecRhs[ elec ] > -1 ){
//       swatch.start();

//       for ( int i = 0; i < dim; i ++ ){
// 	((taucs_double*)b)[ i ] = 0.0;
// 	((taucs_double*)xP)[ i ] = 0.0;
//       }
//       ((taucs_double*)b)[ vecRhs[ elec ] ] = 1.0;
      
//       taucs_vec_permute( dim, A->flags, (char*)b, (char*)bP, perm );
//       if ( solveDirect ) {
// 	if ( solveOOC ){
// 	  taucs_ooc_solve_llt( L, xP, bP );
// 	} else {
// 	  taucs_supernodal_solve_llt( L, xP, bP );
// 	}
//       } else {
// 	taucs_conjugate_gradients( AP, taucs_ccs_solve_llt, L, xP, bP, maxiter, tol );
//       }
//       taucs_vec_ipermute( dim, A->flags, (char*)xP, (char*)x, perm );
      
//       for ( int i = 0; i < dim; i ++ ) xVec[ i ] = ((taucs_double*)x)[ i ];
      
//       sprintf( strdummy, "%s.P.%d.pot", filebody.c_str(), elec ); xVec.save( strdummy );
      
//       cout << "\r" << elec << "/" << vecRhs.size()-1 << "\t " << swatch.duration() << "s ";
//       swatch.stop();
//     }
//   }  

//   if ( solveOOC )  taucs_io_delete( (taucs_io_handle*)L );
//   taucs_ccs_free( A );
//   taucs_ccs_free( AP );
//   taucs_free( L );
//   taucs_free( x );
//   taucs_free( b );
//   taucs_free( xP );
//   taucs_free( bP );

//   return 1;

// #endif //HAVE_TAUCS
//   cerr << WHERE_AM_I << " Taucs not installed" << endl;
//   return 0;
// }


// int solveWithTaucsTest( const RDirectMatrix & S, RVector & xVec, const RVector & bVec, 
// 			double tol, int maxiter, bool verbose ){
// #ifdef HAVE_TAUCS

//   cout << "Taucs testing" << endl;
//   char * opt_log = "stdout";  taucs_logfile( opt_log );

//   Stopwatch swatch; swatch.start();
//   int dim = bVec.size();

//   int nVals = S.valueCount();
  
//   nVals -= (nVals - dim) / 2;
//   taucs_ccs_matrix * A = taucs_ccs_create( dim, dim, nVals, TAUCS_DOUBLE | TAUCS_SYMMETRIC | TAUCS_LOWER);
//   fill( S, *A );

//   void * b = new void *[ dim * sizeof( TAUCS_DOUBLE ) ]; assert( b );
//   void * x = new void *[ dim * sizeof( TAUCS_DOUBLE ) ]; assert( x );
//   void * bP = new void *[ dim * sizeof( TAUCS_DOUBLE ) ]; assert( bP );
//   void * xP = new void *[ dim * sizeof( TAUCS_DOUBLE ) ]; assert( xP );
//   for ( int i = 0; i < dim; i ++ ) {
//     ((taucs_double*)b)[ i ] = (taucs_double)bVec[ i ];
//     ((taucs_double*)x)[ i ] = 0.0; 
//     ((taucs_double*)bP)[ i ] = 0.0; 
//     ((taucs_double*)xP)[ i ] = 0.0; 
//   }

//   enum TAUCSSOLVETYPE{Default,ICCG,ICCGPERM,DIRECTPERM,DIRECTPERMOOC,AMWB};
  
//   //TAUCSSOLVETYPE tosolve = Default;
//   TAUCSSOLVETYPE tosolve = ICCG;
//   //TAUCSSOLVETYPE tosolve = ICCGPERM;
//   //  TAUCSSOLVETYPE tosolve = DIRECTPERM;
//   //  TAUCSSOLVETYPE tosolve = DIRECTPERMOOC;
//   //  TAUCSSOLVETYPE tosolve = AMWB;

//   switch ( tosolve ){
//   case Default: {
//     cout << "Solve Taucs-default" << endl;
//     char * cgArg[] = { "taucs.factor.LLT=true", "taucs.factor.mf=true", NULL };
//     taucs_linsolve( A, NULL, 1, x, b, cgArg, NULL);
//   } break;
//   case ICCG: {
//     cout << "Solve Taucs-ICCG" << endl;
//     double dropTol= 1e-3;
//     void * L = taucs_ccs_factor_llt( A, dropTol, 0 );
//     taucs_conjugate_gradients( A, taucs_ccs_solve_llt, L, x, b, maxiter, tol );
//   } break;
//   case ICCGPERM:{
//     cout << "Solve Taucs-ICCG with permutaion" << endl;
//     double dropTol= 1e-3;
//     int * perm, * invperm;
//     taucs_ccs_order( A, &perm, &invperm, "metis" );
//     taucs_ccs_matrix * AP = taucs_ccs_permute_symmetrically( A, perm, invperm );
//     void * L = taucs_ccs_factor_llt( AP, dropTol, 0 );
//     taucs_vec_permute( dim, A->flags, (char*)b, (char*)bP, perm );
//     taucs_conjugate_gradients( AP, taucs_ccs_solve_llt, L, xP, bP, maxiter, tol );
//     taucs_vec_ipermute( dim, A->flags, (char*)xP, (char*)x, perm );
//   } break;
//   case DIRECTPERM:{
//     cout << "Solve Taucs-DIRECT with permutaion" << endl;
//     int * perm, * invperm;
//     taucs_ccs_order( A, &perm, &invperm, "metis" );
//     taucs_ccs_matrix * AP = taucs_ccs_permute_symmetrically( A, perm, invperm );
//     void * L = taucs_ccs_factor_llt_mf_maxdepth( AP, 0 );
//     taucs_vec_permute( dim, A->flags, (char*)b, (char*)bP, perm );
//     taucs_supernodal_solve_llt( L, xP, bP );
//     taucs_vec_ipermute( dim, A->flags, (char*)xP, (char*)x, perm );
//   } break;
//   case DIRECTPERMOOC:{
//     cout << "Solve Taucs-DIRECT-OOC with permutaion" << endl;
//     int * perm, * invperm;
//     taucs_ccs_order( A, &perm, &invperm, "metis" );
//     taucs_ccs_matrix * AP = taucs_ccs_permute_symmetrically( A, perm, invperm );

//     taucs_io_handle * L = taucs_io_create_multifile( "taucsOOC" );
//     double opt_ooc_memory = taucs_available_memory_size();
//     cout << opt_ooc_memory << endl;
//     taucs_ooc_factor_llt( AP, L, opt_ooc_memory * 1024 );

//     taucs_vec_permute( dim, A->flags, (char*)b, (char*)bP, perm );
//     taucs_ooc_solve_llt( L, xP, bP );
//     taucs_vec_ipermute( dim, A->flags, (char*)xP, (char*)x, perm );
//   } break;
//   case AMWB:{
//     cout << "Solve Taucs-AMBW precond not yet implemented" << endl;
// //     taucs_ccs_matrix * M = taucs_ccs_preconditioner_create( A, 170566, 1 );
// //     int * perm, * invperm;
// //     taucs_ccs_order( M, &perm, &invperm, "metis" );
// //     taucs_ccs_matrix * MP = taucs_ccs_permute_symmetrically( M, perm, invperm );
// //     double dropTol= 1e-3;
// //     void * L = taucs_ccs_factor_llt( MP, dropTol, 0 );
// //     taucs_conjugate_gradients( A, taucs_ccs_solve_llt, M, x, b, maxiter, tol );
//   } break;
//   default :
//     break;
//   }

//   //  void * xTaucs = taucs_vec_create( A->n,  );
//   //  double diag[ dim ];
//   //  taucs_ccs_matrix * PTaucs = taucs_ccs_factor_llt( A, 0, 1 );
//   //  taucs_ccs_write_ijv( PTaucs, "P.taucsmatrix" );  
//   //  taucs_ccs_write_ijv( A, "S.taucsmatrix" );  
  
//   //  void * optArg[] = { NULL };
  
//   //  char * cgArg[] = { "taucs.factor.LLT=true", NULL };
// //   char * cgArg[] = { "taucs.ooc=true", // out of core- solve large problems with storing on disk
// // 		     "taucs.ooc.basename=ooc.tmp", 
// // 		     "taucs.factor.LLT=true",
// // 		     NULL };

// // taucs_linsolve(A,R,nb,x,b,options,NULL);

// //void * R = NULL;
// //void * R = taucs_ccs_factor_llt( A, 0.001, 0 );

// // char * cgArg[] = { "taucs.factor.LLT=true",
// //  		     "taucs.factor.mf=true", 
// //  		     NULL };

// //   char * cgArg[] = { "taucs.approximate.amwb=true",
// // 		     "taucs.approximate.amwb.subgraphs=1000",
// // 		     "taucs.factor.LLT=true",
// // 		     "taucs.factor.mf=true",
// // 		     "taucs.solve.cg=true", 
// // 		     //		     "taucs.solve.minres=false", 
// // 		     "taucs.solve.maxits=1000", 
// // 		     //		     "taucs.maxdepth=0.001", 
// // 		     NULL };

// //   char * cgArg[] = {   "taucs.solve.cg=true", 
// // 		      "taucs.factor.droptol=0.01", 
// // 		      "taucs.solve.maxits=1000", 
// // 		      NULL };

// //  taucs_linsolve( A, NULL, b.size(), xTaucs, bTaucs, cgArg, optArg);

// //   void * R = taucs_ccs_factor_llt( A, 1e-3, 0 );
// //   taucs_conjugate_gradients( A, 
// // 			     taucs_ccs_solve_llt, 
// // 			     R,
// // 			     xTaucs,
// // 			     bTaucs,
// // 			     maxiter,
// // 			     tol);
  
// //   taucs_linsolve( NULL, &R, 0, NULL, NULL, NULL, NULL); // free factorisation

// //   taucs_io_handle * ooc = taucs_io_create_multifile( "./tmpTaucsOOC" );
// //   double mem = taucs_available_memory_size();
// //   cout << "taucs mem " << mem << endl;
// //   taucs_ooc_factor_llt( A, ooc, mem * 1024 );
// //   taucs_ooc_solve_llt( ooc, xTaucs, bTaucs );

//   for ( int i = 0; i < dim; i ++ ) xVec[ i ] = ((taucs_double*)x)[ i ];
  
//   taucs_ccs_free( A );
//   taucs_free( x );
//   taucs_free( b );

//   return 1;
// #endif //HAVE_TAUCS
//   cerr << WHERE_AM_I << " Taucs not installed" << endl;
//   return 0;

// }

// int solveWithTaucsDirect( const RDirectMatrix & S, RVector & x, const RVector & b, 
// 			  double tol, int maxiter, bool verbose ){
//   vector <RVector> xVec; xVec.push_back( x );
//   vector <RVector> bVec; bVec.push_back( b );
//   solveWithTaucsDirect( S, xVec, bVec, tol, maxiter, verbose );
//   x = xVec[0];
//   return 1;
// }

// int solveWithTaucsDirect( const RDirectMatrix & S, vector < RVector > & xMat, const vector< RVector> & bMat, 
// 			  double tol, int maxiter, bool verbose ){
  
// #ifdef HAVE_TAUCS
//   if ( verbose ) cout << "Solve with Taucs" << endl;

//   if ( verbose ){
//     char * opt_log = "stdout";  taucs_logfile( opt_log );
//   } else ;//taucs_logfile("solverTaucs.log");

//   Stopwatch swatch;   swatch.start();
//   int dim = bMat[ 0 ].size();
//   int nRhs = bMat.size();
//   int nVals = S.valueCount();
  
//   nVals -= (nVals - dim) / 2;
//   taucs_ccs_matrix * A = taucs_ccs_create( dim, dim, nVals, TAUCS_DOUBLE | TAUCS_SYMMETRIC | TAUCS_LOWER);

//   fill( S, *A );

//   void * b = new void *[ dim * nRhs * sizeof( TAUCS_DOUBLE ) ]; assert( b );
//   void * x = new void *[ dim * nRhs * sizeof( TAUCS_DOUBLE ) ]; assert( x );

//   for ( int j = 0; j < nRhs; j ++ ){
//     for ( int i = 0; i < dim; i ++ ) ((taucs_double*)b)[ j * dim + i ] = (taucs_double)bMat[ j ][ i ];
//   }

//   void * optArg[] = { NULL };
  
//   char * cgArg[] = { "taucs.factor.LLT=true", "taucs.factor.mf=true", NULL };
//   taucs_linsolve( A, NULL, nRhs, x, b, cgArg, optArg);

//   for ( int j = 0; j < nRhs; j ++ ){
//     for ( int i = 0; i < dim; i ++ ) xMat[ j ][ i ] = ((taucs_double*)x)[ j * dim + i ];
//   }

//   taucs_ccs_free( A );
//   taucs_free( x );
//   taucs_free( b );

//   return 1;
// #endif //HAVE_TAUCS
//   cerr << WHERE_AM_I << " Taucs not installed" << endl;
//   return 0;
// }

/*
$Log: taucswrapper.cpp,v $
Revision 1.19  2007/03/07 16:38:14  carsten
*** empty log message ***

Revision 1.18  2006/07/04 18:04:06  carsten
*** empty log message ***

Revision 1.17  2005/10/17 11:56:58  carsten
*** empty log message ***

Revision 1.16  2005/10/14 15:31:37  carsten
*** empty log message ***

Revision 1.15  2005/10/12 17:34:39  carsten
*** empty log message ***

Revision 1.14  2005/08/12 16:43:20  carsten
*** empty log message ***

Revision 1.13  2005/08/09 18:13:44  carsten
*** empty log message ***

Revision 1.12  2005/07/28 19:43:43  carsten
*** empty log message ***

Revision 1.11  2005/06/02 15:44:10  carsten
*** empty log message ***

Revision 1.10  2005/03/23 13:27:47  carsten
*** empty log message ***

Revision 1.9  2005/03/09 19:20:35  carsten
*** empty log message ***

Revision 1.8  2005/03/09 15:50:49  carsten
*** empty log message ***

Revision 1.6  2005/02/22 21:51:43  carsten
*** empty log message ***

Revision 1.5  2005/02/14 18:58:54  carsten
*** empty log message ***

Revision 1.4  2005/01/11 19:12:26  carsten
*** empty log message ***

Revision 1.3  2005/01/09 19:13:55  carsten
*** empty log message ***

Revision 1.2  2005/01/07 15:23:06  carsten
*** empty log message ***

Revision 1.1  2005/01/06 20:28:30  carsten
*** empty log message ***

*/
