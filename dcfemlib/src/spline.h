// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#ifndef SPLINE__H
#define SPLINE__H SPLINE__H

#include "dcfemlib.h"
using namespace DCFEMLib;

#include "elements.h"
using namespace MyMesh;

#include <vector>
using namespace std;

class CubicFunct{
public:
  CubicFunct( const double a, const double b, const double c, const double d ) : a_( a ), b_( b ), c_( c ), d_( d ) {}
  CubicFunct( const CubicFunct & C ) : a_( C.a_ ), b_( C.b_ ), c_( C.c_ ), d_( C.d_ ) {}

  double val( double t ) const { return t * (t * (t * d_ + c_) + b_) + a_; }

  double a_, b_, c_, d_;
};

vector < CubicFunct > calcNaturalCubic( const vector < double > & x );
vector < CubicFunct > calcNaturalCubicClosed( const vector < double > & x );

/*! Create a vector of RealPos from input points with cubic spline interpolation. The edge between the input points is subdivided into nSegments.*/
DLLEXPORT vector < RealPos > createSpline( const vector < RealPos > & input, int nSegments, bool close );

/*! Create a vector of RealPos from input points with cubic spline interpolation. The edge between the input points is subdivided into 3 segments within the lokal distance localDX to the inputpoints.*/
vector < RealPos > createSplineLocalDX( const vector < RealPos > & input, double localDX, bool close );

using namespace std;
#endif // SPLINE__H

/*
$Log: spline.h,v $
Revision 1.3  2007/10/04 07:21:59  thomas
BERT + polytools codeblocks (dllexport)

Revision 1.2  2005/09/12 18:50:50  carsten
*** empty log message ***

Revision 1.1  2005/09/08 19:08:48  carsten
*** empty log message ***

 */
