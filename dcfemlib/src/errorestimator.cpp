// Copyright (C) 2005 Carsten Rücker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#include "errorestimator.h"
#include "stlmatrix.h"

ErrorEstimator::ErrorEstimator( BaseMesh * mesh, int order ) : order_( order ), mesh_( mesh ) {
  switch ( mesh_->dim() ){
  case 2:
    init_( MYMESH_TRIANGLE_RTTI );
    break;
  case 3:
    init_( MYMESH_TETRAHEDRON_RTTI );
    break;
  }
}

ErrorEstimator::~ErrorEstimator(){
}

void ErrorEstimator::init_( int elementRTTI ){

  switch ( elementRTTI ){
  case MYMESH_TRIANGLE_RTTI: {
    switch( order_ ){
    case 1:{// Triangle 1.order Error=O(h^2)
      //** zienkiewicz
      nQuadrature_ = 1; weights_.resize( nQuadrature_ ); zetaMat_.resize( nQuadrature_, 3 );

      double a1 = 1.0 / 3.0;
      zetaMat_[ 0 ][ 0 ] = a1;  zetaMat_[ 0 ][ 1 ] = a1;  zetaMat_[ 0 ][ 2 ] = a1;
      weights_[ 0 ] = 1.0;
    } break;
    case 2:{// Triangle 2.order Error=O(h^3)
      //**    Joseph E. Flaherty -- Finite Element Analysis CSCI-6860 / MATH-6860
      nQuadrature_ = 3; weights_.resize( nQuadrature_ ); zetaMat_.resize( nQuadrature_, 3 );

      double a1 = 2.0 / 3.0, b1 = 1.0 / 6.0;
      zetaMat_[ 0 ][ 0 ] = a1; zetaMat_[ 0 ][ 1 ] = b1; zetaMat_[ 0 ][ 2 ] = b1;
      zetaMat_[ 1 ][ 0 ] = b1; zetaMat_[ 1 ][ 1 ] = a1; zetaMat_[ 1 ][ 2 ] = b1;
      zetaMat_[ 2 ][ 0 ] = b1; zetaMat_[ 2 ][ 1 ] = b1; zetaMat_[ 2 ][ 2 ] = a1;

      weights_[ 0 ] = weights_[ 1 ] = weights_[ 2 ] = 1.0 / 3.0;
    } break;
    case 3:{// Triangle 3.order Error=O(h^4)
      //** zienkiewicz
      nQuadrature_ = 4; weights_.resize( nQuadrature_ ); zetaMat_.resize( nQuadrature_, 3 );

      double a1 = 1.0 / 3.0;
      zetaMat_[ 0 ][ 0 ] = a1;  zetaMat_[ 0 ][ 1 ] = a1;  zetaMat_[ 0 ][ 2 ] = a1;

      double a2 = 0.6, b2 = 0.2;
      zetaMat_[ 1 ][ 0 ] = a2; zetaMat_[ 1 ][ 1 ] = b2; zetaMat_[ 1 ][ 2 ] = b2;
      zetaMat_[ 2 ][ 0 ] = b2; zetaMat_[ 2 ][ 1 ] = a2; zetaMat_[ 2 ][ 2 ] = b2;
      zetaMat_[ 3 ][ 0 ] = b2; zetaMat_[ 3 ][ 1 ] = b2; zetaMat_[ 3 ][ 2 ] = a2;

      weights_[ 0 ] = -27.0 / 48.0;
      weights_[ 1 ] = weights_[ 2 ] = weights_[ 3 ] = 25.0 / 48.0;
    } break;
    case 4:{// Triangle 4.order Error=O(h^5)
      //**    Joseph E. Flaherty -- Finite Element Analysis CSCI-6860 / MATH-6860
      nQuadrature_ = 6; weights_.resize( nQuadrature_ ); zetaMat_.resize( nQuadrature_, 3 );

      double a1 = 0.816847572980459, b1 = 0.091576213509771;
      zetaMat_[ 0 ][ 0 ] = a1; zetaMat_[ 0 ][ 1 ] = b1; zetaMat_[ 0 ][ 2 ] = b1;
      zetaMat_[ 1 ][ 0 ] = b1; zetaMat_[ 1 ][ 1 ] = a1; zetaMat_[ 1 ][ 2 ] = b1;
      zetaMat_[ 2 ][ 0 ] = b1; zetaMat_[ 2 ][ 1 ] = b1; zetaMat_[ 2 ][ 2 ] = a1;

      double a2 = 0.108103018168070, b2 = 0.445948490915965;
      zetaMat_[ 3 ][ 0 ] = a2; zetaMat_[ 3 ][ 1 ] = b2; zetaMat_[ 3 ][ 2 ] = b2;
      zetaMat_[ 4 ][ 0 ] = b2; zetaMat_[ 4 ][ 1 ] = a2; zetaMat_[ 4 ][ 2 ] = b2;
      zetaMat_[ 5 ][ 0 ] = b2; zetaMat_[ 5 ][ 1 ] = b2; zetaMat_[ 5 ][ 2 ] = a2;

      weights_[ 0 ] = weights_[ 1 ] = weights_[ 2 ] = 0.109951743655322;
      weights_[ 3 ] = weights_[ 4 ] = weights_[ 5 ] = 0.223381589678011;

    } break;
    case 5:{ // Triangle 5.order Error=O(h^7)
      //** Zienkiewicz
      nQuadrature_ = 7; weights_.resize( nQuadrature_ ); zetaMat_.resize( nQuadrature_, 3 );
      double sqrt15 = sqrt( 15.0 );

      zetaMat_[ 0 ][ 0 ] = 1.0 / 3.0; zetaMat_[ 0 ][ 1 ] = 1.0 / 3.0; zetaMat_[ 0 ][ 2 ] = 1.0 / 3.0;
      double b1 = 2.0 / 7.0 + sqrt15 / 21.0, a1 = 1.0 - 2.0 * b1;
      zetaMat_[ 1 ][ 0 ] = a1;        zetaMat_[ 1 ][ 1 ] = b1;        zetaMat_[ 1 ][ 2 ] = b1;
      zetaMat_[ 2 ][ 0 ] = b1;        zetaMat_[ 2 ][ 1 ] = a1;        zetaMat_[ 2 ][ 2 ] = b1;
      zetaMat_[ 3 ][ 0 ] = b1;        zetaMat_[ 3 ][ 1 ] = b1;        zetaMat_[ 3 ][ 2 ] = a1;
      double b2 = 2.0 / 7.0 - sqrt15 / 21.0, a2 = 1.0 - 2.0 * b2;
      zetaMat_[ 4 ][ 0 ] = a2;        zetaMat_[ 4 ][ 1 ] = b2;        zetaMat_[ 4 ][ 2 ] = b2;
      zetaMat_[ 5 ][ 0 ] = b2;        zetaMat_[ 5 ][ 1 ] = a2;        zetaMat_[ 5 ][ 2 ] = b2;
      zetaMat_[ 6 ][ 0 ] = b2;        zetaMat_[ 6 ][ 1 ] = b2;        zetaMat_[ 6 ][ 2 ] = a2;

      weights_[ 0 ] = 270.0;
      weights_[ 1 ] = weights_[ 2 ] = weights_[ 3 ] = 155.0 + sqrt15;
      weights_[ 4 ] = weights_[ 5 ] = weights_[ 6 ] = 155.0 - sqrt15;
      weights_ /= 1200.0;

    } break;
    case 6:{ // Triangle 6.order Error=O(h^7)
      //**    Joseph E. Flaherty -- Finite Element Analysis CSCI-6860 / MATH-6860
      nQuadrature_ = 12; weights_.resize( nQuadrature_ ); zetaMat_.resize( nQuadrature_, 3 );

      double a1 = 0.873821971016996, b1 = 0.063089014491502;
      zetaMat_[ 0 ][ 0 ] = a1; zetaMat_[ 0 ][ 1 ] = b1; zetaMat_[ 0 ][ 2 ] = b1;
      zetaMat_[ 1 ][ 0 ] = b1; zetaMat_[ 1 ][ 1 ] = a1; zetaMat_[ 1 ][ 2 ] = b1;
      zetaMat_[ 2 ][ 0 ] = b1; zetaMat_[ 2 ][ 1 ] = b1; zetaMat_[ 2 ][ 2 ] = a1;
      double a2 = 0.501426509658179, b2 = 0.249286745170910;
      zetaMat_[ 3 ][ 0 ] = a2; zetaMat_[ 3 ][ 1 ] = b2; zetaMat_[ 3 ][ 2 ] = b2;
      zetaMat_[ 4 ][ 0 ] = b2; zetaMat_[ 4 ][ 1 ] = a2; zetaMat_[ 4 ][ 2 ] = b2;
      zetaMat_[ 5 ][ 0 ] = b2; zetaMat_[ 5 ][ 1 ] = b2; zetaMat_[ 5 ][ 2 ] = a2;
      double a3 = 0.636502499121399, b3 = 0.310352451033785, c3 = 0.053145049844816;
      zetaMat_[ 6 ][ 0 ] = a3; zetaMat_[ 6 ][ 1 ] = b3; zetaMat_[ 6 ][ 2 ] = c3;
      zetaMat_[ 7 ][ 0 ] = a3; zetaMat_[ 7 ][ 1 ] = c3; zetaMat_[ 7 ][ 2 ] = b3;
      zetaMat_[ 8 ][ 0 ] = b3; zetaMat_[ 8 ][ 1 ] = a3; zetaMat_[ 8 ][ 2 ] = c3;
      zetaMat_[ 9 ][ 0 ] = b3; zetaMat_[ 9 ][ 1 ] = c3; zetaMat_[ 9 ][ 2 ] = a3;
      zetaMat_[ 10 ][ 0 ] = c3; zetaMat_[ 10 ][ 1 ] = a3; zetaMat_[ 10 ][ 2 ] = b3;
      zetaMat_[ 11 ][ 0 ] = c3; zetaMat_[ 11 ][ 1 ] = b3; zetaMat_[ 11 ][ 2 ] = a3;

      weights_[ 0 ] = weights_[ 1 ] = weights_[ 2 ] = 0.050844906370207;
      weights_[ 3 ] = weights_[ 4 ] = weights_[ 5 ] = 0.116786275726379;
      weights_[ 6 ] = weights_[ 7 ] = weights_[ 8 ] = 0.082851075618374;
      weights_[ 9 ] = weights_[ 10 ] = weights_[ 11 ] = 0.082851075618374;
    } break;
    case 7:{ // Triangle 7.order Error=O( h^8 )
      //**    Joseph E. Flaherty -- Finite Element Analysis CSCI-6860 / MATH-6860
      nQuadrature_ = 13; weights_.resize( nQuadrature_ ); zetaMat_.resize( nQuadrature_, 3 );

      double a1 = 1.0 / 3.0;
      zetaMat_[ 0 ][ 0 ] = a1; zetaMat_[ 0 ][ 1 ] = a1; zetaMat_[ 0 ][ 2 ] = a1;
      double a2 = 0.479308067841923, b2 = 0.260345966079038;
      zetaMat_[ 1 ][ 0 ] = a2; zetaMat_[ 1 ][ 1 ] = b2; zetaMat_[ 1 ][ 2 ] = b2;
      zetaMat_[ 2 ][ 0 ] = b2; zetaMat_[ 2 ][ 1 ] = a2; zetaMat_[ 2 ][ 2 ] = b2;
      zetaMat_[ 3 ][ 0 ] = b2; zetaMat_[ 3 ][ 1 ] = b2; zetaMat_[ 3 ][ 2 ] = a2;
      double a3 = 0.869739794195568, b3 = 0.065130102902216;
      zetaMat_[ 4 ][ 0 ] = a3; zetaMat_[ 4 ][ 1 ] = b3; zetaMat_[ 4 ][ 2 ] = b3;
      zetaMat_[ 5 ][ 0 ] = b3; zetaMat_[ 5 ][ 1 ] = a3; zetaMat_[ 5 ][ 2 ] = b3;
      zetaMat_[ 6 ][ 0 ] = b3; zetaMat_[ 6 ][ 1 ] = b3; zetaMat_[ 6 ][ 2 ] = a3;
      //** c4 in Tabelle von Flaherty falsch, (Zahlendreher)
      double a4 = 0.638444188569809, b4 = 0.312865496004875, c4 = 0.048690315425316;
      zetaMat_[ 7 ][ 0 ] = a4; zetaMat_[ 7 ][ 1 ] = b4; zetaMat_[ 7 ][ 2 ] = c4;
      zetaMat_[ 8 ][ 0 ] = a4; zetaMat_[ 8 ][ 1 ] = c4; zetaMat_[ 8 ][ 2 ] = b4;
      zetaMat_[ 9 ][ 0 ] = b4; zetaMat_[ 9 ][ 1 ] = a4; zetaMat_[ 9 ][ 2 ] = c4;
      zetaMat_[ 10][ 0 ] = b4; zetaMat_[ 10][ 1 ] = c4; zetaMat_[ 10][ 2 ] = a4;
      zetaMat_[ 11][ 0 ] = c4; zetaMat_[ 11][ 1 ] = a4; zetaMat_[ 11][ 2 ] = b4;
      zetaMat_[ 12][ 0 ] = c4; zetaMat_[ 12][ 1 ] = b4; zetaMat_[ 12][ 2 ] = a4;

      weights_[ 0 ] = -0.14957004467670;
      weights_[ 1 ] = weights_[ 2 ] = weights_[ 3 ] = 0.175615257433204;
      weights_[ 4 ] = weights_[ 5 ] = weights_[ 6 ] = 0.053347235608839;
      weights_[ 7 ] = weights_[ 8 ] = weights_[ 9 ] = weights_[ 10 ] = weights_[ 11 ] = weights_[ 12 ] = 0.077113760890257;
    } break;
    default:
      TO_IMPL;
    }
  } break;
  case MYMESH_TETRAHEDRON_RTTI:{
    switch ( order_ ){
    case 1:{// Tetrahedron 1.order, exakt for linears, Error=O(h^2)
      //** zienkiewicz
      nQuadrature_ = 1; weights_.resize( nQuadrature_ ); zetaMat_.resize( nQuadrature_, 4 );

      double a1 = 0.25;
      zetaMat_[ 0 ][ 0 ] = a1; zetaMat_[ 0 ][ 1 ] = a1; zetaMat_[ 0 ][ 2 ] = a1; zetaMat_[ 0 ][ 3 ] = a1;

      weights_[ 0 ] = 1.0;
    } break;
    case 2:{// Tetrahedron 2.order, exakt for quads, Error=O( h^3 )
      //**    Joseph E. Flaherty -- Finite Element Analysis CSCI-6860 / MATH-6860
      nQuadrature_ = 4; weights_.resize( nQuadrature_ ); zetaMat_.resize( nQuadrature_, 4 );

      double a1 = 0.585410196624969, b1 = 0.138196601125011;
      zetaMat_[ 0 ][ 0 ] = a1; zetaMat_[ 0 ][ 1 ] = b1; zetaMat_[ 0 ][ 2 ] = b1; zetaMat_[ 0 ][ 3 ] = b1;
      zetaMat_[ 1 ][ 0 ] = b1; zetaMat_[ 1 ][ 1 ] = a1; zetaMat_[ 1 ][ 2 ] = b1; zetaMat_[ 1 ][ 3 ] = b1;
      zetaMat_[ 2 ][ 0 ] = b1; zetaMat_[ 2 ][ 1 ] = b1; zetaMat_[ 2 ][ 2 ] = a1; zetaMat_[ 2 ][ 3 ] = b1;
      zetaMat_[ 3 ][ 0 ] = b1; zetaMat_[ 3 ][ 1 ] = b1; zetaMat_[ 3 ][ 2 ] = b1; zetaMat_[ 3 ][ 3 ] = a1;

      weights_[ 0 ] = weights_[ 1 ] = weights_[ 2 ] = weights_[ 3 ] = 1.0 / 4.0;
    } break;
    case 3:{// Tetrahedron 3.order, exakt for cubics, Error=O( h^4 )
      //**    Joseph E. Flaherty -- Finite Element Analysis CSCI-6860 / MATH-6860
      nQuadrature_ = 5; weights_.resize( nQuadrature_ ); zetaMat_.resize( nQuadrature_, 4 );

      double a1 = 0.25;
      zetaMat_[ 0 ][ 0 ] = a1; zetaMat_[ 0 ][ 1 ] = a1; zetaMat_[ 0 ][ 2 ] = a1; zetaMat_[ 0 ][ 3 ] = a1;
      double a2 = 1.0/3.0, b2 = 1.0 / 6.0;
      zetaMat_[ 1 ][ 0 ] = a2; zetaMat_[ 1 ][ 1 ] = b2; zetaMat_[ 1 ][ 2 ] = b2; zetaMat_[ 1 ][ 3 ] = b2;
      zetaMat_[ 2 ][ 0 ] = b2; zetaMat_[ 2 ][ 1 ] = a2; zetaMat_[ 2 ][ 2 ] = b2; zetaMat_[ 2 ][ 3 ] = b2;
      zetaMat_[ 3 ][ 0 ] = b2; zetaMat_[ 3 ][ 1 ] = b2; zetaMat_[ 3 ][ 2 ] = a2; zetaMat_[ 3 ][ 3 ] = b2;
      zetaMat_[ 4 ][ 0 ] = b2; zetaMat_[ 4 ][ 1 ] = b2; zetaMat_[ 4 ][ 2 ] = b2; zetaMat_[ 4 ][ 3 ] = a2;

      weights_[ 0 ] = -0.8;
      weights_[ 1 ] = weights_[ 2 ] = weights_[ 3 ] = weights_[ 4 ] = 0.45;
    } break;
    case 4:{//** Tetrahedron 4.order, Error=O( h^5 )
      //**    Joseph E. Flaherty -- Finite Element Analysis CSCI-6860 / MATH-6860
      nQuadrature_ = 11; weights_.resize( nQuadrature_ ); zetaMat_.resize( nQuadrature_, 4 );

      double a1 = 0.25;
      zetaMat_[ 0 ][ 0 ] = a1; zetaMat_[ 0 ][ 1 ] = a1; zetaMat_[ 0 ][ 2 ] = a1; zetaMat_[ 0 ][ 3 ] = a1;
      double a2 = 0.785714285714286, b2 = 0.071428571428571;
      zetaMat_[ 1 ][ 0 ] = a2; zetaMat_[ 1 ][ 1 ] = b2; zetaMat_[ 1 ][ 2 ] = b2; zetaMat_[ 1 ][ 3 ] = b2;
      zetaMat_[ 2 ][ 0 ] = b2; zetaMat_[ 2 ][ 1 ] = a2; zetaMat_[ 2 ][ 2 ] = b2; zetaMat_[ 2 ][ 3 ] = b2;
      zetaMat_[ 3 ][ 0 ] = b2; zetaMat_[ 3 ][ 1 ] = b2; zetaMat_[ 3 ][ 2 ] = a2; zetaMat_[ 3 ][ 3 ] = b2;
      zetaMat_[ 4 ][ 0 ] = b2; zetaMat_[ 4 ][ 1 ] = b2; zetaMat_[ 4 ][ 2 ] = b2; zetaMat_[ 4 ][ 3 ] = a2;
      double a3 = 0.399403576166799, b3 = 0.100596423833201;
      zetaMat_[ 5 ][ 0 ] = a3; zetaMat_[ 5 ][ 1 ] = a3; zetaMat_[ 5 ][ 2 ] = b3; zetaMat_[ 5 ][ 3 ] = b3;
      zetaMat_[ 6 ][ 0 ] = a3; zetaMat_[ 6 ][ 1 ] = b3; zetaMat_[ 6 ][ 2 ] = b3; zetaMat_[ 6 ][ 3 ] = a3;
      zetaMat_[ 7 ][ 0 ] = b3; zetaMat_[ 7 ][ 1 ] = b3; zetaMat_[ 7 ][ 2 ] = a3; zetaMat_[ 7 ][ 3 ] = a3;
      zetaMat_[ 8 ][ 0 ] = b3; zetaMat_[ 8 ][ 1 ] = a3; zetaMat_[ 8 ][ 2 ] = a3; zetaMat_[ 8 ][ 3 ] = b3;
      zetaMat_[ 9 ][ 0 ] = a3; zetaMat_[ 9 ][ 1 ] = b3; zetaMat_[ 9 ][ 2 ] = a3; zetaMat_[ 9 ][ 3 ] = b3;
      zetaMat_[ 10][ 0 ] = b3; zetaMat_[ 10][ 1 ] = a3; zetaMat_[ 10][ 2 ] = b3; zetaMat_[ 10][ 3 ] = a3;

      //** in der Tabelle von Flaherty sind die Werte \xi und \eta gegeben, also * det(J)/V = 6.0.
      weights_[ 0 ] = -0.01315555555555555555555556 * 6.0;
      weights_[ 1 ] = weights_[ 2 ] = weights_[ 3 ] = weights_[ 4 ] = 0.007622222222222222222 * 6.0;
      weights_[ 5 ] = weights_[ 6 ] = weights_[ 7 ] = weights_[ 8 ] = weights_[ 9 ] = weights_[ 10 ] = 0.02488888888888888888888 * 6.0;
    } break;
    case 5:{//** Tetrahedron 4.order, Error=O( h^5 )
      //**    Joseph E. Flaherty -- Finite Element Analysis CSCI-6860 / MATH-6860
      nQuadrature_ = 15; weights_.resize( nQuadrature_ ); zetaMat_.resize( nQuadrature_, 4 );

      double a1 = 0.25;
      zetaMat_[ 0 ][ 0 ] = a1; zetaMat_[ 0 ][ 1 ] = a1; zetaMat_[ 0 ][ 2 ] = a1; zetaMat_[ 0 ][ 3 ] = a1;
      double a2 = 0.0, b2 = 1.0 / 3.0;
      zetaMat_[ 1 ][ 0 ] = a2; zetaMat_[ 1 ][ 1 ] = b2; zetaMat_[ 1 ][ 2 ] = b2; zetaMat_[ 1 ][ 3 ] = b2;
      zetaMat_[ 2 ][ 0 ] = b2; zetaMat_[ 2 ][ 1 ] = a2; zetaMat_[ 2 ][ 2 ] = b2; zetaMat_[ 2 ][ 3 ] = b2;
      zetaMat_[ 3 ][ 0 ] = b2; zetaMat_[ 3 ][ 1 ] = b2; zetaMat_[ 3 ][ 2 ] = a2; zetaMat_[ 3 ][ 3 ] = b2;
      zetaMat_[ 4 ][ 0 ] = b2; zetaMat_[ 4 ][ 1 ] = b2; zetaMat_[ 4 ][ 2 ] = b2; zetaMat_[ 4 ][ 3 ] = a2;
      double a3 = 0.7272727272727272727, b3 = 0.090909090909090909;
      zetaMat_[ 5 ][ 0 ] = a3; zetaMat_[ 5 ][ 1 ] = b3; zetaMat_[ 5 ][ 2 ] = b3; zetaMat_[ 5 ][ 3 ] = b3;
      zetaMat_[ 6 ][ 0 ] = b3; zetaMat_[ 6 ][ 1 ] = a3; zetaMat_[ 6 ][ 2 ] = b3; zetaMat_[ 6 ][ 3 ] = b3;
      zetaMat_[ 7 ][ 0 ] = b3; zetaMat_[ 7 ][ 1 ] = b3; zetaMat_[ 7 ][ 2 ] = a3; zetaMat_[ 7 ][ 3 ] = b3;
      zetaMat_[ 8 ][ 0 ] = b3; zetaMat_[ 8 ][ 1 ] = b3; zetaMat_[ 8 ][ 2 ] = b3; zetaMat_[ 8 ][ 3 ] = a3;
      double a4 = 0.066550153573664, b4 = 0.433449846426336;
      zetaMat_[ 9 ][ 0 ] = a4; zetaMat_[ 9 ][ 1 ] = a4; zetaMat_[ 9 ][ 2 ] = b4; zetaMat_[ 9 ][ 3 ] = b4;
      zetaMat_[ 10][ 0 ] = a4; zetaMat_[ 10][ 1 ] = b4; zetaMat_[ 10][ 2 ] = b4; zetaMat_[ 10][ 3 ] = a4;
      zetaMat_[ 11][ 0 ] = b4; zetaMat_[ 11][ 1 ] = b4; zetaMat_[ 11][ 2 ] = a4; zetaMat_[ 11][ 3 ] = a4;
      zetaMat_[ 12][ 0 ] = b4; zetaMat_[ 12][ 1 ] = a4; zetaMat_[ 12][ 2 ] = a4; zetaMat_[ 12][ 3 ] = b4;
      zetaMat_[ 13][ 0 ] = a4; zetaMat_[ 13][ 1 ] = b4; zetaMat_[ 13][ 2 ] = a4; zetaMat_[ 13][ 3 ] = b4;
      zetaMat_[ 14][ 0 ] = b4; zetaMat_[ 14][ 1 ] = a4; zetaMat_[ 14][ 2 ] = b4; zetaMat_[ 14][ 3 ] = a4;

      //** in der Tabelle von Flaherty sind die Werte \xi und \eta gegeben, also * det(J)/V = 6.0.
      weights_[ 0 ] = 0.030283678097089 * 6.0;
      weights_[ 1 ] = weights_[ 2 ] = weights_[ 3 ] = weights_[ 4 ] = 0.006026785714286 * 6.0;
      weights_[ 5 ] = weights_[ 6 ] = weights_[ 7 ] = weights_[ 8 ] = 0.011645249086029 * 6.0;
      weights_[ 9 ] = weights_[ 10 ] = weights_[ 11 ] = weights_[ 12 ] = weights_[ 13 ] = weights_[ 14 ] = 0.010949141561386 * 6.0;
    } break;
    }
  } break;
  default:
    cerr << WHERE_AM_I << " no quadrature rule for " << elementRTTI << endl;
  }
  bool check = false;
  if ( check ){
    cout << "Sum weights must be 1.0: " << sum( weights_ ) << endl;
    for ( int i = 0 ; i < nQuadrature_; i ++ ){
      cout << i << " Sum zeta(i) be 1.0: " << sum( zetaMat_[ i ] ) << endl;
    }
  }
}

vector< RealPos > ErrorEstimator::quadPoints( ){
  vector< RealPos > quadPoints( mesh_->cellCount() * weights_.size() );

  for ( int i = 0; i < mesh_->cellCount(); i ++ ){
    for ( uint j = 0; j < weights_.size(); j ++ ){
      quadPoints[ i * weights_.size() + j ] = RealPos( 0.0, 0.0, 0.0 );

      for ( int k = 0; k < mesh_->cell( i ).shapeNodeCount(); k ++ ){
	quadPoints[ i * weights_.size() + j ] += mesh_->cell( i ).node( k ).pos() * zetaMat_[ j ][ k ];
      }
      //      mesh_->createNode( quadPoints[ i * weights_.size() + j ] );
    }
  }
  return quadPoints;
}

int ErrorEstimator::quadVals( const RVector & u, RVector & uQuad, vector < RealPos > & uGradQuad ){
  if ( u.size() != mesh_->nodeCount() ){
    cerr << WHERE_AM_I << "u.size() != mesh_->nodeCount():" << u.size() << " " << mesh_->nodeCount() << endl;
  }
  if ( (uint)uQuad.size() != mesh_->cellCount() * weights_.size() ) uQuad.resize( mesh_->nodeCount() * weights_.size() );
  if ( uGradQuad.size() != mesh_->cellCount() * weights_.size() ) uGradQuad.resize( mesh_->nodeCount() * weights_.size() );

  double zeta1 = 0.0, zeta2 = 0.0, zeta3 = 0.0, zeta4 = 0.0;
  RealPos dXidXYZ( 0.0, 0.0, 0.0), dEtadXYZ( 0.0, 0.0, 0.0), dZetadXYZ( 0.0, 0.0, 0.0);
  vector < RealPos > dNdXiEtaZeta;
  RVector uCell; RVector N;

  int nNodes;
  for ( int i = 0; i < mesh_->cellCount(); i ++ ){
    nNodes = mesh_->cell( i ).nodeCount();
    dNdXiEtaZeta.resize( nNodes, RealPos( 0.0, 0.0, 0.0 ) );
    uCell.resize( nNodes ); N.resize( nNodes );

    for ( int j = 0; j < nNodes; j ++ ) uCell[ j ] = u[ mesh_->cell( i ).node( j ).id() ];

    STLMatrix J( mesh_->cell( i ).jacobianMat( ) );
    STLMatrix Ji = inv( J );

    for ( uint j = 0; j < weights_.size(); j ++ ){

      uGradQuad[ i * weights_.size() + j ] = RealPos( 0.0, 0.0, 0.0 );

      RealPos quadPoint( 0.0, 0.0, 0.0 );

      for ( int k = 0; k < mesh_->cell( i ).shapeNodeCount(); k ++ ){
	quadPoint += mesh_->cell( i ).node( k ).pos() * zetaMat_[ j ][ k ];
      }

      quadPoint -= mesh_->cell( i ).node( 0 ).pos();

      zeta2 = Ji[ 0 ] * quadPoint;
      zeta3 = Ji[ 1 ] * quadPoint;
      dXidXYZ  = Ji[ 0 ];
      dEtadXYZ = Ji[ 1 ];

      if( mesh_->dim() == 3 ){
	zeta4 = Ji[ 2 ] * quadPoint;
	dZetadXYZ = Ji[ 2 ];
      }
      zeta1 = 1 - zeta2 - zeta3 - zeta4;

      switch( mesh_->cell( i ).rtti() ){

      case MYMESH_TRIANGLE_RTTI:{
	N[ 0 ] = zeta1;
	N[ 1 ] = zeta2;
	N[ 2 ] = zeta3;

	dNdXiEtaZeta[ 0 ][ 0 ] = -1.0; //  dN_1 / d\xi
	dNdXiEtaZeta[ 1 ][ 0 ] = 1.0; //  dN_2 / d\xi
	dNdXiEtaZeta[ 2 ][ 0 ] = 0.0; //  dN_2 / d\xi

	dNdXiEtaZeta[ 0 ][ 1 ] = -1.0; //  dN_1 / d\eta
	dNdXiEtaZeta[ 1 ][ 1 ] = 0.0; //  dN_2 / d\eta
	dNdXiEtaZeta[ 2 ][ 1 ] = 1.0; //  dN_3 / d\eta
      } break;
      case MYMESH_TRIANGLE6_RTTI:{

	N[ 0 ] = ( zeta1 * ( 2.0 * zeta1 - 1 ) );
	N[ 1 ] = ( zeta2 * ( 2.0 * zeta2 - 1 ) );
	N[ 2 ] = ( zeta3 * ( 2.0 * zeta3 - 1 ) );
	N[ 3 ] = ( 4.0 * zeta1 * zeta2 );
	N[ 4 ] = ( 4.0 * zeta2 * zeta3 );
	N[ 5 ] = ( 4.0 * zeta3 * zeta1 );

	double xi = zeta2, eta = zeta3;

	dNdXiEtaZeta[ 0 ][ 0 ] = ( -3.0 + 4.0 * xi + 4.0 * eta ); // dN_1 / d\xi
	dNdXiEtaZeta[ 1 ][ 0 ] = ( -1.0 + 4.0 * xi );             // dN_2 / d\xi
	dNdXiEtaZeta[ 2 ][ 0 ] = ( 0.0 );                         // dN_3 / d\xi
	dNdXiEtaZeta[ 3 ][ 0 ] = ( 4.0 - 8.0 * xi - 4.0 * eta );   // dN_4 / d\xi
	dNdXiEtaZeta[ 4 ][ 0 ] = ( 4.0 * eta );                   // dN_5 / d\xi
	dNdXiEtaZeta[ 5 ][ 0 ] = ( - 4.0 * eta );                 // dN_6 / d\xi

	dNdXiEtaZeta[ 0 ][ 1 ] = ( -3.0 + 4.0 * eta + 4.0 * xi ); // dN_1 / d\eta
	dNdXiEtaZeta[ 1 ][ 1 ] = ( 0.0 );                         // dN_2 / d\eta
	dNdXiEtaZeta[ 2 ][ 1 ] = ( -1.0 + 4.0 * eta );            // dN_3 / d\eta
	dNdXiEtaZeta[ 3 ][ 1 ] = ( - 4.0 * xi );                  // dN_4 / d\eta
	dNdXiEtaZeta[ 4 ][ 1 ] = ( 4.0 * xi );                    // dN_5 / d\eta
	dNdXiEtaZeta[ 5 ][ 1 ] = ( 4.0 - 4.0 * xi - 8.0 * eta );  // dN_6 / d\eta

      } break;
      case MYMESH_TETRAHEDRON_RTTI:{
	N[ 0 ] = zeta1;
	N[ 1 ] = zeta2;
	N[ 2 ] = zeta3;
	N[ 3 ] = zeta4;

	dNdXiEtaZeta[ 0 ][ 0 ] = -1.0; //  dN_1 / d\xi
	dNdXiEtaZeta[ 1 ][ 0 ] = 1.0; //  dN_2 / d\xi
	dNdXiEtaZeta[ 2 ][ 0 ] = 0.0; //  dN_2 / d\xi
	dNdXiEtaZeta[ 3 ][ 0 ] = 0.0; //  dN_3 / d\eta

	dNdXiEtaZeta[ 0 ][ 1 ] = -1.0; //  dN_1 / d\eta
	dNdXiEtaZeta[ 1 ][ 1 ] = 0.0; //  dN_2 / d\eta
	dNdXiEtaZeta[ 2 ][ 1 ] = 1.0; //  dN_3 / d\eta
	dNdXiEtaZeta[ 3 ][ 1 ] = 0.0; //  dN_3 / d\eta

	dNdXiEtaZeta[ 0 ][ 2 ] = -1.0; //  dN_1 / d\eta
	dNdXiEtaZeta[ 1 ][ 2 ] = 0.0; //  dN_2 / d\eta
	dNdXiEtaZeta[ 2 ][ 2 ] = 0.0; //  dN_3 / d\eta
	dNdXiEtaZeta[ 3 ][ 2 ] = 1.0; //  dN_3 / d\eta
      } break;
      case MYMESH_TETRAHEDRON10_RTTI:{
	//** new order TO_IMPL
// 	N[ 0 ] = ( zeta1 * ( 2.0 * zeta1 - 1 ) );
// 	N[ 1 ] = ( zeta2 * ( 2.0 * zeta2 - 1 ) );
// 	N[ 2 ] = ( zeta3 * ( 2.0 * zeta3 - 1 ) );
// 	N[ 3 ] = ( zeta4 * ( 2.0 * zeta4 - 1 ) );
// 	N[ 4 ] = ( 4.0 * zeta1 * zeta2 );
// 	N[ 5 ] = ( 4.0 * zeta2 * zeta3 );
// 	N[ 6 ] = ( 4.0 * zeta3 * zeta1 );
// 	N[ 7 ] = ( 4.0 * zeta1 * zeta4 );
// 	N[ 8 ] = ( 4.0 * zeta2 * zeta4 );
// 	N[ 9 ] = ( 4.0 * zeta3 * zeta4 );

	N[ 0 ] = ( zeta1 * ( 2.0 * zeta1 - 1 ) );
	N[ 1 ] = ( zeta2 * ( 2.0 * zeta2 - 1 ) );
	N[ 2 ] = ( zeta3 * ( 2.0 * zeta3 - 1 ) );
	N[ 3 ] = ( zeta4 * ( 2.0 * zeta4 - 1 ) );
	N[ 4 ] = ( 4.0 * zeta1 * zeta2 );
	N[ 5 ] = ( 4.0 * zeta1 * zeta3 );
	N[ 6 ] = ( 4.0 * zeta1 * zeta4 );
	N[ 7 ] = ( 4.0 * zeta2 * zeta3 );
	N[ 8 ] = ( 4.0 * zeta3 * zeta4 );
	N[ 9 ] = ( 4.0 * zeta1 * zeta4 );
      } break;
      default:
	TO_IMPL;
	break;
      }

      uQuad[ i * weights_.size() + j ] = MyVec::dot( N , uCell );

      for ( int k = 0; k < mesh_->cell( i ).nodeCount(); k ++ ){
	uGradQuad[ i * weights_.size() + j ][ 0 ] += ( ( dNdXiEtaZeta[ k ][ 0 ] * dXidXYZ[ 0 ] +
							 dNdXiEtaZeta[ k ][ 1 ] * dEtadXYZ[ 0 ] +
							 dNdXiEtaZeta[ k ][ 2 ] * dZetadXYZ[ 0 ] ) *
						       u[ mesh_->cell( i ).node( k ).id() ] );

	uGradQuad[ i * weights_.size() + j ][ 1 ] += ( ( dNdXiEtaZeta[ k ][ 0 ] * dXidXYZ[ 1 ] +
							 dNdXiEtaZeta[ k ][ 1 ] * dEtadXYZ[ 1 ] +
							 dNdXiEtaZeta[ k ][ 2 ] * dZetadXYZ[ 1 ] ) *
						       u[ mesh_->cell( i ).node( k ).id() ] );

	uGradQuad[ i * weights_.size() + j ][ 2 ] += ( ( dNdXiEtaZeta[ k ][ 0 ] * dXidXYZ[ 2 ] +
							 dNdXiEtaZeta[ k ][ 1 ] * dEtadXYZ[ 2 ] +
							 dNdXiEtaZeta[ k ][ 2 ] * dZetadXYZ[ 2 ] ) *
						       u[ mesh_->cell( i ).node( k ).id() ] );
      }
    }
  }
return 1;
    
}

RVector ErrorEstimator::quadVals( const RVector & u ){
  RVector uQuad( mesh_->cellCount() * weights_.size() );
  vector < RealPos > uQuadGrad( mesh_->cellCount() * weights_.size() );
  quadVals( u, uQuad, uQuadGrad );
  return uQuad;
}
vector < RealPos > ErrorEstimator::quadValsGrad( const RVector & u ){
  RVector uQuad( mesh_->cellCount() * weights_.size() );
  vector < RealPos > uQuadGrad( mesh_->cellCount() * weights_.size() );
  quadVals( u, uQuad, uQuadGrad);
  return uQuadGrad;
}


//vector < RealPos > ErrorEstimator::quadValsGrad( const RVector & u ){
//   if ( u.size() != mesh_->nodeCount() ){
//     cerr << WHERE_AM_I << "u.size() != mesh_->nodeCount():" << u.size() << " " << mesh_->nodeCount() << endl;
//   }
//   vector< RealPos > uGradQuad( mesh_->cellCount() * weights_.size() );

//   for ( int i = 0; i < mesh_->cellCount(); i ++ ){

//     STLMatrix J( mesh_->cell( i ).jacobianMat( ) );
//     STLMatrix Ji = inv( J );

//     zeta1x =
//     zeta2x
//     zeta3x
//     zeta4x
//     for ( int j = 0; j < weights_.size(); j ++ ){

//       RealPos quadPoint( 0.0, 0.0, 0.0 );
//       for ( int k = 0; k < mesh_->cell( i ).shapeNodeCount(); k ++ ){
// 	quadPoint += mesh_->cell( i ).node( k ).pos() * zetaMat_[ j ][ k ];
//       }

//       double x0 = mesh_->cell( i ).node( 0 ).pos().x();
//       double y0 = mesh_->cell( i ).node( 0 ).pos().y();
//       double zeta2x = Ji[ 0 ][ 0 ];
//       double zeta3 = Ji[ 1 ][ 0 ] * ( quadPoint[ 0 ] - x0 ) + Ji[ 1 ][ 1 ] * ( quadPoint[ 1 ] - y0 );

//       if ( mesh_->dim() == 3 ){
// 	double zeta4 = Ji[ 1 ][ 0 ] * ( quadPoint[ 0 ] - x0 ) + Ji[ 1 ][ 1 ] * ( quadPoint[ 1 ] - y0 );
//       }
//       double zeta1 = 1 - zeta2 - zeta3;

//       double uTmp = 0.0;
//       switch( mesh_->cell( i ).rtti() ){
//       case MYMESH_TRIANGLE_RTTI:{
// 	uTmp = ( zeta1 * u[ mesh_->cell( i ).node( 0 ).id() ] +
// 		 zeta2 * u[ mesh_->cell( i ).node( 1 ).id() ] +
// 		 zeta3 * u[ mesh_->cell( i ).node( 2 ).id() ] );
//       } break;
//       case MYMESH_TRIANGLE6_RTTI:{
// 	uTmp = ( zeta1 * ( 2.0 * zeta1 - 1 ) * u[ mesh_->cell( i ).node( 0 ).id() ] +
// 		 zeta2 * ( 2.0 * zeta2 - 1 ) * u[ mesh_->cell( i ).node( 1 ).id() ] +
// 		 zeta3 * ( 2.0 * zeta3 - 1 ) * u[ mesh_->cell( i ).node( 2 ).id() ] +
// 		 4.0 * zeta1 * zeta2 * u[ mesh_->cell( i ).node( 3 ).id() ] +
// 		 4.0 * zeta2 * zeta3 * u[ mesh_->cell( i ).node( 4 ).id() ] +
// 		 4.0 * zeta3 * zeta1 * u[ mesh_->cell( i ).node( 5 ).id() ] );
//       } break;
//       default:
// 	TO_IMPL;
// 	break;
//       }
//       uQuad[ i * weights_.size() + j ] = uTmp;

// //       for ( int k = 0; k < mesh_->cell( i ).nodeCount(); k ++ ){
// // 	uQuad[ i * weights_.size() + j ] += u[ mesh_->cell( i ).node( k ).id() ] * zetaMat_[ j ][ k ];
// //       }

//     }
//   }
//   return uGradQuad;
// }

// vector< RealPos > ErrorEstimator::valsGrad( const RVector & u ){
//   if ( u.size() != mesh_->nodeCount() ){
//     cerr << WHERE_AM_I << "u.size() != mesh_->nodeCount():" << u.size() << " " << mesh_->nodeCount() << endl;
//   }

//   vector< RealPos > uGrad( mesh_->cellCount(), RealPos( 0.0, 0.0, 0.0 ) );
//   int nZeta = 0;
//   STLVector zetaX, zetaY, zetaZ;

//   for ( int i = 0; i < mesh_->cellCount(); i ++ ){
//     double J = mesh_->cell( i ).jacobianDeterminant();

//     switch( mesh_->cell( i ).rtti() ){
//     case MYMESH_TRIANGLE_RTTI:{
//       nZeta = 3;
//       zetaX.resize( nZeta ); zetaY.resize( nZeta ); zetaZ.resize( nZeta );

//       STLMatrix J( mesh_->cell( i ).jacobianMat( ) );
//       STLMatrix Ji = inv( J );

//       zetaX[ 1 ] = Ji[ 0 ][ 0 ]; zetaX[ 2 ] = Ji[ 1 ][ 0 ];
//       zetaX[ 0 ] = - zetaX[ 1 ] - zetaX[ 2 ];
//       zetaY[ 1 ] = Ji[ 0 ][ 1 ]; zetaY[ 2 ] = Ji[ 1 ][ 1 ];
//       zetaY[ 0 ] = - zetaY[ 1 ] - zetaY[ 2 ];
//       zetaZ[ 1 ] = 0.0; zetaZ[ 2 ] = 0.0;
//       zetaZ[ 0 ] = - zetaZ[ 1 ] - zetaZ[ 2 ];

// //       cout << J << Ji << endl;
// //       cout << mesh_->cell( i ).jacobianDeterminant() << endl;
// //       cout << zetaX[ 0 ] << " " <<  zetaY[ 0 ] << endl;
// //       cout << zetaX[ 1 ] << " " <<  zetaY[ 1 ] << endl;
// //       cout << zetaX[ 2 ] << " " <<  zetaY[ 2 ] << endl;

//     } break;
//     case MYMESH_TETRAHEDRON_RTTI:{
//       nZeta = 4;
//       zetaX.resize( nZeta ); zetaY.resize( nZeta ); zetaZ.resize( nZeta );

//       STLMatrix J( mesh_->cell( i ).jacobianMat( ) );
//       STLMatrix Ji= inv( J );

//       zetaX[ 1 ] = Ji[ 0 ][ 0 ]; zetaX[ 2 ] = Ji[ 1 ][ 0 ]; zetaX[ 3 ] = Ji[ 2 ][ 0 ];
//       zetaX[ 0 ] = - zetaX[ 1 ] - zetaX[ 2 ] - zetaX[ 3 ];

//       zetaY[ 1 ] = Ji[ 0 ][ 1 ]; zetaY[ 2 ] = Ji[ 1 ][ 1 ]; zetaY[ 3 ] = Ji[ 2 ][ 1 ];
//       zetaY[ 0 ] = - zetaY[ 1 ] - zetaY[ 2 ] - zetaY[ 3 ];

//       zetaZ[ 1 ] = Ji[ 0 ][ 2 ]; zetaZ[ 2 ] = Ji[ 1 ][ 2 ]; zetaZ[ 3 ] = Ji[ 2 ][ 2 ];
//       zetaZ[ 0 ] = - zetaZ[ 1 ] - zetaZ[ 2 ] - zetaZ[ 3 ];

// //       cout << J << Ji << endl;
// //       cout << mesh_->cell( i ).jacobianDeterminant() << endl;
// //       cout << zetaX[ 0 ] << " " <<  zetaY[ 0 ] << " " << zetaZ[ 0 ] << endl;
// //       cout << zetaX[ 1 ] << " " <<  zetaY[ 1 ] << " " << zetaZ[ 1 ] << endl;
// //       cout << zetaX[ 2 ] << " " <<  zetaY[ 2 ] << " " << zetaZ[ 2 ] << endl;
// //       cout << zetaX[ 3 ] << " " <<  zetaY[ 3 ] << " " << zetaZ[ 3 ] << endl;
//     } break;
//     default:
//       TO_IMPL
//       break;
//     }

//     for ( int j = 0; j < mesh_->cell( i ).nodeCount(); j ++ ){
//       uGrad[ i ][ 0 ] += zetaX[ j ] * u[ mesh_->cell( i ).node( j ).id() ];
//       uGrad[ i ][ 1 ] += zetaY[ j ] * u[ mesh_->cell( i ).node( j ).id() ];
//       uGrad[ i ][ 2 ] += zetaZ[ j ] * u[ mesh_->cell( i ).node( j ).id() ];
//     }
//   }
//   return uGrad;
// }

RVector ErrorEstimator::normH1semiVec( const vector< RealPos > & uGrad ){
  if ( uGrad.size() != mesh_->cellCount() * weights_.size() ){
    cerr << WHERE_AM_I << "uGrad.size() != mesh_->cellCount() * weights_.size():"
	 << uGrad.size() << " " << mesh_->cellCount() * weights_.size() << endl;
  }
  RVector absUGrad( uGrad.size() );
  for ( uint i = 0; i < uGrad.size(); i ++ ) absUGrad[ i ] = ( uGrad[ i ][ 0 ] + uGrad[ i ][ 1 ] + uGrad[ i ][ 2 ]);

  //** nach Carsten Carstensen siehe AFEM( Fehler.m ) die Wurzel wegen der kompatibilität zu normL2( gradU );
  for ( uint i = 0; i < uGrad.size(); i ++ ) absUGrad[ i ] = sqrt( ( uGrad[ i ][ 0 ] * uGrad[ i ][ 0 ] +
   								    uGrad[ i ][ 1 ] * uGrad[ i ][ 1 ] +
								    uGrad[ i ][ 2 ] * uGrad[ i ][ 2 ] ) );

  //  for ( int i = 0; i < uGrad.size(); i ++ ) cout << i << " " << absUGrad[ i ]<< endl;
  return normLpVec( absUGrad, 2 );
}

RVector ErrorEstimator::normLpVec( const RVector & u, int p ){
  int nCells = mesh_->cellCount();

  if ( (uint)u.size() != nCells * weights_.size() ){
    cerr << WHERE_AM_I << " to few values " << u.size() << " " << nCells * weights_.size() << endl;
  }

  double J = 0.0;
  RVector Lp( nCells );

  for ( int i = 0; i < nCells; i ++ ){
    J = mesh_->cell( i ).jacobianDeterminant();
    Lp[ i ] = 0.0;
    //** \int_cell |u| \d cell;
    for ( uint j = 0; j < weights_.size(); j ++ ){
      Lp[ i ] += weights_[ j ] * pow( u[ i * weights_.size() + j  ], p );
    }
    Lp[ i ] = pow( Lp[ i ] * ( J / 2.0), 1.0 / p );
  }

  return Lp;
}


/*
$Log: errorestimator.cpp,v $
Revision 1.9  2010/03/10 22:35:37  carsten
add marker to polyAddProfile -i

Revision 1.8  2005/10/12 16:37:43  carsten
*** empty log message ***

Revision 1.5  2005/10/09 18:54:44  carsten
*** empty log message ***

Revision 1.4  2005/10/07 17:19:49  carsten
*** empty log message ***

Revision 1.2  2005/10/04 18:05:51  carsten
*** empty log message ***

Revision 1.1  2005/10/04 11:10:25  carsten
*** empty log message ***

*/
