// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//  
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//  

#ifndef LDLWRAPPER__H
#define LDLWRAPPER__H

#include "myvec/matrix.h"
#include "myvec/vector.h"
#include "solver.h"

class LDLWrapper : public SolverWrapper{
public:
  LDLWrapper( ) : SolverWrapper() { }
  LDLWrapper( const RDirectMatrix & S, bool verbose = false );
  ~LDLWrapper();
 
  int factorise();
  int solve( const RVector & rhs, RVector & solution );

protected:
  int initialize_( const RDirectMatrix & S );

  int * AcolPtr_;
  int * ArowIdx_;
  double * Avals_;

  int * Li_;
  int * Lp_;
  double * Lx_;
  double * D_;

  bool preordering_;
  int * P_;
  int * Pinv_;

//   void * A_;
//   void * AP_;
//   void * L_;
//   void * b_, * x_, * bP_, * xP_;
  
//   int nVals_;
//   int * perm_, * invperm_;
};

#endif // LDLWRAPPER__H

/*
$Log: ldlwrapper.h,v $
Revision 1.2  2005/08/09 18:13:44  carsten
*** empty log message ***

Revision 1.1  2005/01/11 19:12:26  carsten
*** empty log message ***

*/
