// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#ifndef SENSITIVITY__H
#define SENSITIVITY__H

#include "dcfemlib.h"
#include "stlmatrix.h"
using namespace DCFEMLib;

#include "basemesh.h"
#include "fem.h"

using namespace MyMesh;
using namespace MyVec;

#include "elements.h"
#include "elementmatrix.h"

#include "datamap.h"
#include "electrodeconfig.h"


typedef vector < RVector > MyMatrix;

using namespace std;

void save( MyMatrix & S, const string & filebody, bool verbose = false);

DLLEXPORT void load( int n, MyMatrix & Matrix, const string & filebody, const string & suffix = "", int kIdx = -1, bool verbose = false);

DLLEXPORT void load(int rows, int columns, MyMatrix & Matrix, const string & filebody, const string & suffix = "", int kIdx = -1, bool verbose = false);

DLLEXPORT void loadSparse( int rows, int columns, LinSparseVector & S, const string & filebody, double tol, double maxMem, bool verbose = false);


//template < class Vector > int sensitivityDCFEM( const BaseMesh & mesh, const Vector & p1, const Vector & p2,
//						Vector & solution, bool verbose = false);

template < class Matrix > void clearSensM( Matrix & S );

DLLEXPORT int findModelCount( const BaseMesh & mesh );

/*! main wrapper function */
DLLEXPORT void createSensitivityMatrixCapsulate( MyMatrix & S, const BaseMesh & mesh, const MyMatrix & pot,
				       const string & primaryPotFilename,
				       const ElectrodeConfigVector & data,
				       const RVector & model,
				       const string & filebody,
				       bool holdInMem, double maxMem, bool verbose,
				       int firstSensValue, int lastSensValue );

/*! main working function */
void createSensitivityMatrix( MyMatrix & S, const BaseMesh & mesh, const MyMatrix & pot,
			      const ElectrodeConfigVector & data,
			      const RVector & model, int nModel,
			      const string & filebody,
			      bool holdInMem, bool verbose,
			      int firstSensValue, int lastSensValue, double weight = 1.0 );

void createSensitivityMatrix( MyMatrix & S, const BaseMesh & mesh, const MyMatrix & pot,
			      const ElectrodeConfigVector & data, const RVector & model, bool verbose){
  createSensitivityMatrixCapsulate( S, mesh, pot, NOT_DEFINED, data, model, NOT_DEFINED, true, -1, verbose, -1, -1);
}

void createSensitivityMatrix( MyMatrix & S, const BaseMesh & mesh, const MyMatrix & pot,
			      const ElectrodeConfigVector & data, bool verbose ){
  createSensitivityMatrix( S, mesh, pot, data, *new RVector( 0 ), verbose );
}

void createSensitivityCol( const BaseMesh & mesh, const ElectrodeConfigVector & data,
			   const vector < RVector > & pots, vector < RVector > & S, bool outOfCore, bool verbose );

void createSensitivityColMT( const BaseMesh & mesh, const ElectrodeConfigVector & data,
			     const vector < RVector > & pots, vector < RVector > & S, bool outOfCore, bool verbose );

//** not yet testet or implemented
DLLEXPORT void createSensitivityMatrixAnalyt( MyMatrix & S, const BaseMesh & mesh,
				    const ElectrodeConfigVector & data,
				    const string & filebody,
				    bool holdInMem, bool verbose );

template < class Matrix > void clearSensM( Matrix & S ){
  S.clear();
}


template < class Vector > int sensitivityDCFEM( const BaseMesh & mesh, const Vector & p1, const Vector & p2,
						Vector & solution, bool verbose){

  if ( solution.size() != mesh.cellCount() ) solution.resize( mesh.cellCount() );

  if ( p1.size() == p2.size() && p2.size() >= mesh.nodeCount() ){
    ElementMatrix S_i;

    int m = 0, n = 0;
    double sum = 0.0, a_jk = 0.0, tmppot = 0.0;

    if ( verbose ) cout << "Start calculate ...";

    for ( int i = 0, imax = mesh.cellCount(); i < imax; i ++ ){
      //      cout << "Nr. " << i << endl;
      S_i.ux2uy2uz2( mesh.cell( i ) ); // 60%

      sum = 0.0;

      for ( int j = 0, jmax = mesh.cell( i ).nodeCount(); j < jmax; j++ ){
	for ( int k = 0, kmax = mesh.cell( i ).nodeCount(); k < kmax; k++ ){
	  a_jk = S_i.at( j, k );
	  tmppot = p1[ S_i.i( j ) ] * p2[ S_i.i( k ) ];
	  sum += a_jk * tmppot;
	  //	    cout << "\tS_mn: " << a_jk << "\tp1*p2: " << tmppot << "\tp*S_mn: " << a_jk * tmppot << "\tsum: " << sum << endl;
	}
      }
      solution[ i ] = sum;
    }
    if ( verbose ) cout << "... finish" << endl;
  } else {
    cerr << WHERE_AM_I << " Dimensions wrong ! pot1.size() = " << p1.size()
	 << "; pot2.size() = " << p2.size()
	 << "; mesh.nodeCount(): " << mesh.nodeCount() << endl; return -1;
  }
  return 1;
}

// double chis[ 8 ]={0.0, 0.873821971016996, 0.063089014491502, 0.501426509658179,
// 		  0.249286745170910, 0.636502499121399, 0.310352451033785,
// 		  0.053145049844816};

// double weights[4]={0.0, 0.050844906370207, 0.116786275726379, 0.082851075618374};

// int iChi1[ 12 ] = {1,2,2, 3,4,4, 5,5,6,6,7,7};
// int iChi2[ 12 ] = {2,1,2, 4,3,4, 6,7,5,7,5,6};
// int iChi3[ 12 ] = {2,2,1, 4,4,3, 7,6,7,5,6,5};
// int iWeigths[ 12 ] = {1,1,1,2,2,2,3,3,3,3,3,3};

// void ellEK( double m, double & E, double & K );

// double f( double chi1, double chi2, double chi3, const BaseElement & tri, const RealPos & A, const RealPos & M);
// void sensitivityAnalyt( const BaseMesh & mesh, const RealPos & A, const RealPos & M, RVector & solution );
// void sensitivityAnalyt( const BaseMesh & mesh, const RealPos & A, const RealPos & B, const RealPos & M, const RealPos & N, RVector & solution );




#endif //SENSITIVITY__H

/*
$Log: sensitivity.h,v $
Revision 1.17  2007/09/11 17:28:58  carsten
*** empty log message ***

Revision 1.16  2007/03/15 16:17:11  carsten
*** empty log message ***

Revision 1.15  2007/03/15 16:14:01  carsten
*** empty log message ***

Revision 1.14  2007/02/21 19:44:05  carsten
*** empty log message ***

Revision 1.13  2007/01/17 18:16:24  carsten
*** empty log message ***

Revision 1.12  2006/11/08 19:53:24  carsten
*** empty log message ***

Revision 1.11  2006/11/03 13:25:40  carsten
*** empty log message ***

Revision 1.10  2006/09/26 09:48:38  carsten
*** empty log message ***

Revision 1.9  2006/09/22 10:42:18  thomas
DLLEXPORT added

Revision 1.8  2006/02/06 17:30:42  carsten
*** empty log message ***

Revision 1.7  2005/11/18 17:57:05  carsten
*** empty log message ***

Revision 1.6  2005/10/06 17:38:36  carsten
*** empty log message ***

Revision 1.5  2005/07/14 12:30:26  carsten
*** empty log message ***

Revision 1.4  2005/07/13 17:05:21  carsten
*** empty log message ***

Revision 1.3  2005/03/29 16:57:46  carsten
add inversion tools

Revision 1.2  2005/01/14 14:35:46  carsten
*** empty log message ***

Revision 1.1  2005/01/13 20:10:42  carsten
*** empty log message ***

*/
