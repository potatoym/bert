ifLess(){
    val1=$1; val2=$2
    result=`echo "$val1 $val2" | awk '{if ($1<$2) printf("1"); else printf("0")}'`
    [ "$result" -eq "1" ] && return 1 || return 0
    #[ "$result" -eq "1" ] && echo "1" && return 1 || echo "0" || return 0
}

testCollect(){
    collectFile=$1; A=$2; M=$3; soll=$4; tol=$5
    nElecs=`head -n1 $collectFile`
    ist=`head -n $[ $A + $nElecs + 1 ] $collectFile | tail -1 | cut -f $M`
#     echo "ist $ist" 
#     echo "soll $soll"
    err=`echo "$ist $soll" | awk '{printf("%g", sqrt( ( $1/$2-1.0 )^2) *100.0)}'`
#     echo $err $tol
    ifLess $err $tol
    success=$?
    if [ "$success" -eq "1" ]; then
        return 1
    else
        echo "Test collect file: A M ist soll = (ist/soll-1)*100 (%), TOLERANCE( $tol )"
        echo "$A $M `echo "$ist $soll $err $tol" | awk '{printf("%.6g %.6g = %.6g'%' > TOL=%g\n", $1, $2, $3, $4) }'` "
        return 0
    fi

#     echo "$A $M `echo "$ist $soll" | awk '{printf("%.6g %.6g = %.6g'%'\n", $1, $2, ($1/$2-1)*100)}'` "
#     #err=0.1
#     ifLess $err $tol
#     success=$?
#     echo "success:" $success
    #[ "`ifLess $ist $tol`" -eq "1" ] && echo "1" && return 1 || echo "0" || return 0
}

testDataFile(){
    dataFile=$1
    str="$2 $3 $4 $5"
    soll=$6, tol=$7
    ist=`cat $dataFile | tr -s '[:blank:]' ' ' | grep -e "$str" | cut -d ' ' -f 5`
    err=`echo "$ist $soll" | awk '{printf("%g", sqrt( ( $1/$2-1.0 )^2) *100.0)}'`
    ifLess $err $tol
    success=$?
    if [ "$success" -eq "1" ]; then
        return 1
    else
        echo "Test data: A B M N ist soll = (ist/soll-1)*100 (%)"
        echo "$str `echo "$ist $soll $err $tol" | awk '{printf("%.6g %.6g = %.6g'%' > TOL=%g\n", $1, $2, $3, $4) }'` "
        return 0
    fi
}

testPotential(){
    collectFile=$1
    TOL=$2
    # U1-2 (halfspace) = 1/2pi = 0.159154943
    testCollect $collectFile 1 2 1.59154943091895e-01 $TOL; [ $? -eq 0 ] && return 0
    # U1-5 (halfspace) = 1/2pi = 0.159154943
    testCollect $collectFile 1 5 1.59154943091895e-01 $TOL; [ $? -eq 0 ] && return 0
    # U5-1 (mirrorspace) = 1/4pi + 1/4pi = 0.159154943
    testCollect $collectFile 5 1 1.59154943091895e-01 $TOL; [ $? -eq 0 ] && return 0
    # U5-6 (mirrorspace) = ( 1 + 1/(sqrt(5) )/4pi = 0.115165598716807
    testCollect $collectFile 5 6 0.115165598716807 $TOL; [ $? -eq 0 ] && return 0
    
#     # U8-9 (nodeE-freeE) = ( 1 + 1/(sqrt(5) )/4pi = 0.115165598716807
#     testCollect $collectFile 8 9 0.115165598716807
#     # U9-8 (freeE-nodeE) = ( 1 + 1/(sqrt(5) )/4pi = 0.115165598716807
#     testCollect $collectFile 9 8 0.115165598716807
#     # U9-10 (freeE-freeE) = ( 1 + 1/(sqrt(5) )/4pi = 0.115165598716807
#     testCollect $collectFile 9 10 0.115165598716807
    
# # U2-1 (halfspace) = 1/2pi = 0.159154943
# testCollect $collectFile 2 1 1.59154943091895e-01
# # U2-3 (halfspace) = 1/( sqrt(2) 2pi ) = 0.112539539
# testCollect $collectFile 2 3 0.112539539
# # U2-4 (halfspace) = 1/2pi = 0.159154943
# testCollect $collectFile 2 4 1.59154943091895e-01
# # U3-1 (mirrorspace) = 1/4pi + 1/4pi = 0.159154943
# testCollect $collectFile 3 1 1.59154943091895e-01
# # U3-2 (mirrorspace) = 1/(sqrt(2)*4pi) + 1/(sqrt(2)*4pi) = 0.112539539
# testCollect $collectFile 3 2 0.112539539
# # U3-4 (mirrorspace) = 1/4pi + 1/(sqrt(5)*4pi) = 0.115165598716807
# testCollect $collectFile 3 4 0.115165598716807
# # U3-4 (mirrorspace) = 1/4pi + 1/(sqrt(5)*4pi) = 0.115165598716807
# testCollect $collectFile 4 3 0.115165598716807
# # U5-6 (free node) = 1/2pi = 0.159154943
# echo "greater than 0.0 cause of interpolation error within the cell. "
# testCollect $collectFile 4 6 0.115165598716807
# testCollect $collectFile 6 4 0.115165598716807
}

testDipolData(){
    dataFile=$1
    TOL=$2
    # U1-2-3-4 (halfspace)= - 1/6pi
    testDataFile $dataFile 1 2 3 4 -5.30516476972984e-02 $TOL; [ $? -eq 0 ] && return 0
    testDataFile $dataFile 3 4 1 2 -5.30516476972984e-02 $TOL; [ $? -eq 0 ] && return 0
    # U5-6-7-8 (mirrorspace)= - 1/4pi (1/sqrt(2) - 1/sqrt(13) -1/sqrt(5) - 1/3)
    testDataFile $dataFile 5 6 7 8 -2.79150008005113e-02 $TOL; [ $? -eq 0 ] && return 0
    testDataFile $dataFile 7 8 5 6 -2.79150008005113e-02 $TOL; [ $? -eq 0 ] && return 0
    # U1-0-3-4 (pol-Dipol) = 1/12pi
    testDataFile $dataFile 3 4 1 0 2.65258238486492e-02 $TOL; [ $? -eq 0 ] && return 0
    # U5-0-7-8 (pol-Dipol) = ( 1/3 + 1/sqrt(2) -2/sqrt(13) ) / 8pi
    testDataFile $dataFile 8 7 0 5 1.93269772634118e-02 $TOL; [ $? -eq 0 ] && return 0
}

testPolData(){
    dataFile=$1
    TOL=$2
    # U5-0-7-8 (pol-Dipol) = ( 1/3 + 1/sqrt(2) -2/sqrt(13) ) / 8pi
    testDataFile $dataFile 0 5 8 7 1.93269772634118e-02 $TOL; [ $? -eq 0 ] && return 0
    # U1-0-3-4 (pol-Dipol) = 1/12pi
    testDataFile $dataFile 1 0 3 4 2.65258238486492e-02 $TOL; [ $? -eq 0 ] && return 0
#   U 1 0 2 0 ( pol-pol ) = 1/2pi
    testDataFile $dataFile 1 0 2 0 1.59154943091895e-01 $TOL; [ $? -eq 0 ] && return 0
    testDataFile $dataFile 0 1 0 2 1.59154943091895e-01 $TOL; [ $? -eq 0 ] && return 0
}