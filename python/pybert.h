#ifndef PYTHON_PYBERT__H
#define PYTHON_PYBERT__H

//See best practices section in Py++ documentation

#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <list>
#include <map>

#ifndef PACKAGE_NAME
        #define PACKAGE_NAME "bert (python build)"
        #define PACKAGE_VERSION "1.9.0"
        #define PACKAGE_BUGREPORT "carsten@resistivity.net"
#endif // PACKAGE_NAME

#include "bert.h"
#include "bertMisc.h"
#include "bertJacobian.h"
#include "datamap.h"
#include "dcfemmodelling.h"
#include "bertDataContainer.h"
#include "electrode.h"

namespace BERT{

} // namespace BERT

//** define some aliases to avoid insane method names
namespace pyplusplus{ namespace aliases{
    typedef std::vector< std::pair< unsigned long, unsigned long> > stdVectorPairULUL;
    typedef std::vector<BERT::ElectrodeShape * > stdVectorElectrodeShape;
    typedef std::vector< GIMLI::MeshEntity * >       stdVectorMeshEntity;
//     typedef std::vector< GIMLI::Node * >             stdVectorNodesBert;
//     typedef std::vector< GIMLI::Cell * >             stdVectorCellsBert;
//     typedef std::vector< GIMLI::Boundary * >         stdVectorBoundsBert;
}} //pyplusplus::aliases

#endif // PYTHON_PYBERT__H
