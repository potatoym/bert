#! /usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import string
from environment_for_pybert_build import settings

from optparse import OptionParser
optionParser = OptionParser("usage: %prog [options]")
optionParser.add_option("", "--extra-includes", dest="extraIncludes")
optionParser.add_option("", "--extra-path", dest="extraPath")
optionParser.add_option("", "--caster", dest="caster")
optionParser.add_option("", "--gimli-build", dest="gimliBuild")
optionParser.add_option("", "--clang", dest="clang", default="")

(options, args) = optionParser.parse_args()

if options.caster:
    settings.caster_path = options.caster

if options.extraPath:
    print("insert extra path: ", options.extraPath)
    sys.path.insert(0, options.extraPath)

from pygccxml import parser
import logging
from pygccxml import utils
from pygccxml import declarations
from pygccxml.declarations import access_type_matcher_t
from pyplusplus import code_creators, module_builder, messages, decl_wrappers
from pyplusplus.module_builder import call_policies
from pyplusplus.decl_wrappers.doc_extractor import doc_extractor_i

MAIN_NAMESPACE = 'BERT'

logger = utils.loggers.cxx_parser
#logger.setLevel(logging.DEBUG)


class decl_starts_with (object):
    def __init__ (self, prefix):
        self.prefix = prefix
    def __call__ (self, decl):
        return self.prefix in decl.name

def exclude(method, return_type = '', name = '', symbol = ''):
    for funct in return_type:
        if len(funct):
            fun = method(return_type = funct, allow_empty = True)

            for f in fun:
                logger.debug(("exclude return type", f))
                f.exclude()

    for funct in name:
        if len(funct):
            fun = method(name = funct, allow_empty = True)

            for f in fun:
                logger.debug(("exclude name", f))
                f.exclude()

    for funct in symbol:
        if len(funct):
            fun = method(symbol = funct, allow_empty = True)

            for f in fun:
                logger.debug(("exclude symbol", f))
                f.exclude()

def setMemberFunctionCallPolicieByReturn(mb, MemberRetRef, callPolicie):
    for ref in MemberRetRef:
        memFuns = mb.global_ns.member_functions(return_type=ref, allow_empty=True)
        logger.debug((ref, len(memFuns)))

        for memFun in memFuns:
            if memFun.call_policies:
                print("continue")
                continue
            else:
                memFun.call_policies = \
                call_policies.return_value_policy(callPolicie)

def addAutoConversions(mb):
    rvalue_converters = (
        #'register_pylist_to_rvector_conversion',
         'register_pytuple_to_rvector3_conversion')


    mb.add_declaration_code('void register_pytuple_to_rvector3_conversion();')
    mb.add_registration_code('register_pytuple_to_rvector3_conversion();')

    #for converter in rvalue_converters:
        #mb.add_declaration_code('void %s();' % converter)
        #mb.add_registration_code('%s();' % converter)

    custom_rvalue_path = os.path.join(
                            os.path.abspath(os.path.dirname(__file__))
                            , 'custom_rvalue.cpp')

class docExtractor(doc_extractor_i):
    def __init__(self):
        doc_extractor_i.__init__(self)
        pass

    #def __call__(self, decl):
        #print "__call__(self, decl):", decl
        #print decl.location.file_name
        #print decl.location.line
        #return ""Doku here""

    def escape_doc(self, doc):
        return '"'+doc+'"'

    def extract(self, decl):
        print((decl.location.file_name))
        print((decl.location.line))
        print(("extract(self, decl):", decl))
        return "Doku coming soon"

def generate(defined_symbols, extraIncludes, gimliBuild=""):
    messages.disable(
        messages.W1005  # using a non public variable type for argucments or returns
##           Warnings 1020 - 1031 are all about why Py++ generates wrapper for class X
#        , messages.W1009 # check this
#        , messages.W1014 # check this
#        , messages.W1020
#        , messages.W1021
#        , messages.W1022
        , messages.W1023 # warning W1023: `Py++` will generate class wrapper - there are few functions that should be
# redefined in class wrapper. The functions > are: domainSize, mID, setSingValue.
#        , messages.W1024
#        , messages.W1025
#        , messages.W1026
#        , messages.W1027
#        , messages.W1028
#        , messages.W1029
#        , messages.W1030
        , messages.W1031 # warning W1031: `Py++` will generate class wrapper - user asked to expose non - public member function "createJacobian_"
        , messages.W1040 # execution error W1040: The declaration is unexposed, but there are other declarations, which refer to it. This could cause "no > to_python converter found"
        , messages.W1042 # warning W1042: `Py++` can not find out container
#value_type( mapped_type ). The container class is template
#instantiation declaration > and not definition. This container class
#will be exported, but there is a possibility, that generated code will
#not compile or will > lack some functionality. The solution to the
#problem is to create a variable of the class.
        # This is serious and lead to RuntimeError: `Py++` is going to write different content to the same file
        , messages.W1047 # There are two or more classes that use same > alias("MatElement"). Duplicated aliases causes
                         # few problems, but the main one > is that some of the classes will not
                         # be exposed to Python.Other classes : >
)

    sourcedir = os.path.dirname(os.path.abspath(__file__))
    gimliDir = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + "/../../../gimli/")

    if gimliBuild == "":
        gimliBuild = os.path.abspath(gimliDir + "/build/")
    else:
        gimliBuild = os.path.abspath(gimliBuild)

    print(gimliBuild)
    #print(gimliDir)

    sourceHeader = os.path.abspath(sourcedir + "/" + r"pybert.h")
    bertInclude = os.path.dirname(os.path.abspath(sourcedir + "/../src/" + r"bert.h"))
    #gimliInclude = os.path.dirname(os.path.abspath(gimliDir + "/trunk/src/" + r"gimli.h"))

    settings.includesPaths.append(bertInclude)
    #settings.includesPaths.append(gimliInclude)
    settings.includesPaths.insert(0,os.path.abspath(extraIncludes))

    xml_cached_fc = parser.create_cached_source_fc(sourceHeader,
                                                   settings.module_name + '.cache')

    import platform

    defines = ['PYGIMLI_CAST', 'HAVE_BOOST_THREAD_HPP']
    caster = 'gccxml'
    compiler_path = options.clang


    if platform.system() == 'Windows':
        if platform.architecture()[0] == '64bit':
            #compiler_path='C:/msys64/mingw64/bin/clang++'
            if sys.platform == 'darwin':
                pass
            else:
                defines.append('_WIN64')
                defines.append('MS_WIN64')

                logger.info('Marking win64 for gccxml')
        else:
            #compiler_path='C:/msys32/mingw32/bin/clang++'
            pass

    if len(compiler_path) == 0:
        compiler_path = None

    for define in [settings.bert_defines, defined_symbols]:
        if len(define) > 0:
            defines.append(define)

    try:
        if sys.platform == 'win32':
            # os.name == 'nt' (default on my mingw) results in wrong commandline
            # for gccxml
            os.name = 'mingw'
            casterpath = settings.caster_path.replace('\\', '\\\\')
            casterpath = settings.caster_path.replace('/', '\\')

            if not 'gccxml' in casterpath:
                caster = 'castxml'

            if not '.exe' in casterpath:
                casterpath += '\\' + caster + '.exe'

        else:
            casterpath = settings.caster_path
            if not 'gccxml' in casterpath:
                caster = 'castxml'

    except Exception as e:
        logger.info("caster_path=%s" % casterpath)
        logger.info(str(e))
        raise Exception("Problems determine castxml binary")

    logger.info("caster_path=%s" % casterpath)
    logger.info("working_directory=%s" % settings.bert_path)
    logger.info("include_paths=%s" % settings.includesPaths)
    logger.info("define_symbols=%s" % defines)
    logger.info("compiler_path=%s" % compiler_path)
    logger.info("indexing_suite_version=2")

    xml_generator_config = parser.xml_generator_configuration_t(
                                                xml_generator=caster,
                                                xml_generator_path=casterpath,
                                                working_directory=settings.bert_path,
                                                include_paths=settings.includesPaths,
                                                define_symbols=defines,
                                                ignore_gccxml_output=False,
                                                cflags="",
                                                compiler_path=compiler_path)

    mb = module_builder.module_builder_t([xml_cached_fc],
                                         indexing_suite_version=2,
                                         xml_generator_config=xml_generator_config
                                         )


    #mb = module_builder.module_builder_t([xml_cached_fc],
                                         #gccxml_path=casterpath,
                                         #working_directory=settings.bert_path,
                                         #include_paths=settings.includesPaths,
                                         #define_symbols=defines,
                                         #indexing_suite_version=2,
                                         #compiler_path=compiler_path,
                                         #caster=caster
                                         #)
    logger.info("Reading of c++ sources done.")

    mb.register_module_dependency(os.path.abspath(gimliBuild + '/python/generated/'))
    #sys.exit()

    mb.classes().always_expose_using_scope = True
    mb.calldefs().create_with_signature = True

    # maybe we will use this later for fast rvector <-> np.array conversion, maybe not.
    # hand_made_wrappers.apply(mb)

    global_ns = mb.global_ns
    global_ns.exclude()
    main_ns = global_ns.namespace(MAIN_NAMESPACE)
    main_ns.include()

    exclude(main_ns.member_functions,  name = ['begin', 'end', 'val'],  return_type = [''])
    exclude(main_ns.member_operators,  symbol = [''])

    #mb.calldefs(access_type_matcher_t('protected')).exclude()
    mb.calldefs(access_type_matcher_t('private')).exclude()

    setMemberFunctionCallPolicieByReturn(mb, ['::std::string *', 'float *', 'double *', 'int *' ]
                                            , call_policies.return_pointee_value)

    #setMemberFunctionCallPolicieByReturn(mb, ['::GIMLI::VectorIterator<double> &']
                                        #, call_policies.copy_const_reference)

    setMemberFunctionCallPolicieByReturn(mb, ['::std::string &'
                                                ,  'double &' ]
                                                , call_policies.return_by_value)


    excludeRest = True

    if excludeRest:
        mem_funs = mb.calldefs ()

        for mem_fun in mem_funs:
            if mem_fun.call_policies:
                continue
            if not mem_fun.call_policies and \
                (declarations.is_reference(mem_fun.return_type) or declarations.is_pointer (mem_fun.return_type)):
                #print mem_fun
                #mem_fun.exclude()
                mem_fun.call_policies = \
                    call_policies.return_value_policy(call_policies.reference_existing_object)
                #mem_fun.call_policies = \
                #    call_policies.return_value_policy(call_policies.return_pointee_value)
                #mem_fun.call_policies = \
                #    call_policies.return_value_policy(call_policies.return_opaque_pointer)
                #mem_fun.call_policies = \
                 #   call_policies.return_value_policy(call_policies.copy_non_const_reference)


    # Now it is the time to give a name to our module
    from doxygen import doxygen_doc_extractor
    extractor = doxygen_doc_extractor()

    mb.build_code_creator(settings.module_name, doc_extractor=extractor)

    #I don't want absolute includes within code
    mb.code_creator.user_defined_directories.append(os.path.abspath('.'))

    #And finally we can write code to the disk
    def ignore(val):
        pass
    mb.split_module('./generated', on_unused_file_found=ignore)

if __name__ == '__main__':

    defined_symbols = ''

    generate(defined_symbols, options.extraIncludes, options.gimliBuild)


