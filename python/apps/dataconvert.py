#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    This program is part of pybert
    Visit http://www.resistivity.net for further information or the latest version.
"""

import sys, traceback
import getopt
from os import system

import numpy as np

try:
    import pygimli as pg
    import pybert as pb
except ImportError:
    sys.stderr.write('''ERROR: cannot import the library 'pybert'. Ensure that pybert is in your PYTHONPATH ''')
    sys.exit(1)

from pybert.importer import *

def usage(exitcode = 0, comment = ''):
    print(comment)
    print(__doc__)
    exit(exitcode)

def strToRVector3(s):
    vals = s.split(',')
    if len(vals) == 1:
        return pg.RVector3(float(vals[0]), 0.0)
    if len(vals) == 2:
        return pg.RVector3(float(vals[0]), float(vals[1]))
    if len(vals) == 3:
        return pg.RVector3(float(vals[0]), float(vals[1]), float(vals[2]))


def main(argv):

    # OptionParser is deprecated since python 2.7, use argparse
    from optparse import OptionParser

    parser = OptionParser("usage: %prog [options] data-file"
                            , version="%prog: " + pg.versionStr()
                            , description = "Convert data ert formats:\n " +
                            '                                                       The import data function provide the following data formats:                  ' + importData.__doc__ +
                            '                                                       The export data function provide the following data formats:                  ' + exportData.__doc__
                           )
    parser.add_option("-v", "--verbose", dest="verbose",
                      action="store_true", default = False,
                      help="Be verbose.")
    parser.add_option("-o", "--output", dest = "outFileName", metavar = "File",
                      default = None,
                      help = "Filename for the resulting data file.")
    parser.add_option("", "--of", dest = "outputFormat", metavar = "str", default = 'Gimli'
                            , help = "Fileformat for export.")
    parser.add_option("", "--if", dest = "inputFormat", metavar = "str", default = 'auto'
                            , help = "input fileformat [auto].")
    parser.add_option("", "--debug", dest="debug", action = "store_true", default = False
                            , help = "debug mode.")
    parser.add_option("-t", "--translate", dest="translate",
                            help="translate (move) sensor positions. "'"x,y,z"'" ")
    parser.add_option("-s", "--scale", dest="scale",
                            help="Scale the sensor positions. "'"x,y,z"'" Warning! Electrode positions will be scaled. " +
                            "Existing apparent resistivity values will be rescaled only if there are (wrongly) geometric factor in the file.")
    parser.add_option("-e", "--electrodes", dest="electrodes",
                      type=str, default=None,
                      help="File with list of new electrode positions. Interpolate along."
                      "either: [x], [x z], [x y z] or [dx x y z]")
    parser.add_option("", "--local2dCoordinates", dest="local2dCoordinates",
                      action="store_true", default = False,
                      help="Translate coordinates into a local 2D system starting from (x=0)")

    (options, args) = parser.parse_args()

    importFileName = None

    if len(args) == 0:
        parser.print_help()
        print("Please add a data file.")
        sys.exit(2)
    else:
        importFileName = args[ 0 ];

    if options.outFileName is None:
        if importFileName.find('.shm') > -1:
            options.outFileName = importFileName[0:importFileName.find('.shm')] + '.out'
        else:
            options.outFileName = importFileName + '.out'

    if options.verbose:
        print((options, args))
        print(("verbose =", options.verbose))
        print(("project =", importFileName))
        print(("output =", options.outFileName))
        print(("input format =", options.inputFormat))
        print(("export format =", options.outputFormat))

    data = importData(importFileName, format=options.inputFormat,
                      verbose=options.verbose, debug=options.debug)

    if len(data.additionalPoints()) > 1:
        if options.verbose:
            print("Found", len(data.additionalPoints()),
                  "additional topography point .. roll them out to electrode positions")
        topo = pg.meshtools.interpolateAlongCurve(data.additionalPoints(),
                                                  pg.utils.cumDist(data.sensorPositions()))
        for i, t in enumerate(topo):
            data.setSensorPosition(i, t)


    if options.electrodes is not None:
        data.sortSensorsX()

        curve = np.loadtxt(options.electrodes)
        tIn = None
        if len(curve[0]) == 1: # assume x
            IMPLEMENTME
        if len(curve[0]) == 3: # assume x y z
            curve = curve[:,0:]
        elif len(curve[0]) == 4: # assume dx x y z
            tIn = curve[:,0]
            curve = curve[:,1:]

        tOut = pg.utils.cumDist(data.sensorPositions())
        p = pg.meshtools.interpolateAlongCurve(curve, tOut, tCurve=tIn)
        data.setSensorPositions(p)

    if options.local2dCoordinates:
        #d = pg.utils.cumDist(np.array([pg.x(data.sensorPositions()),
                                       #pg.y(data.sensorPositions()),
                                       #np.zeros(data.sensorCount())]).T)
        d = pg.utils.cumDist(data.sensorPositions())

        data.setSensorPositions(np.array([d,
                                          np.zeros(data.sensorCount()),
                                          pg.z(data.sensorPositions())]).T)

    if options.translate is not None:
        tra = strToRVector3(options.translate)
        if options.verbose: print(("translate: " , tra))
        data.translate(tra)

    if options.scale is not None:
        sca = strToRVector3(options.scale)
        if options.verbose:
            print(("scale: ", sca))

        data.scale(sca)

        if data.haveData('rhoa') and data.haveData('k'):
            data.set('r', data('rhoa') / data('k'))
            data.set('k', pg.geometricFactor(data))
            data.set('rhoa', data.get('r') * data.get('k'))

    # cross check to protect original file
    if options.outFileName != importFileName:
        if options.verbose:
            print('export: ', options.outFileName)
        exportData(data, options.outFileName,
                   fmt=options.outputFormat,
                   verbose=options.verbose)
    else:
        raise Exception("Warning! will not overwrite input data: " + str(importFileName) + " desired outname: " + str(options.outFileName))

if __name__ == "__main__":
    main(sys.argv[ 1: ])
