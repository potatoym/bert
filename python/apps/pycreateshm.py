#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This program is part of pybert
Visit http://www.resistivity.net for further information or the latest version.
"""
import sys

from os import system, path

try:
    import pygimli as pg
    import pygimli.mplviewer
except ImportError:
    import traceback
    traceback.print_exc(file=sys.stdout)
    sys.stderr.write('''ERROR: cannot import the library 'pygimli'. Ensure that pygimli is in your PYTHONPATH ''')

try:
    import pybert as pb
except ImportError:
    import traceback
    traceback.print_exc(file=sys.stdout)
    sys.stderr.write('''ERROR: cannot import the library 'pybert'. Ensure that pybert is in your PYTHONPATH ''')
    
import pybert
from pybert.importer import importData

import matplotlib.pyplot as plt
import numpy as np

def main():
    
    import argparse
    
    parser = argparse.ArgumentParser(description = "usage: %prog [options]. Create a shm file.")
        
    parser.add_argument("-v", "--verbose",
                        dest="verbose", action="store_true",
                        help = "Be verbose.", default = False)
    parser.add_argument("-o", "--output",
                        dest="outFileName", type=str, default="data",
                        help="Filename for the resulting scheme file. Suffix .shm will be add is non is given.")
    parser.add_argument("-I", "--inverse",
                        dest="inverse", action="store_true",
                        help="Use the inverse configurations.")
    parser.add_argument("-n", "--number-electrodes", 
                        dest="nElecs", type=int, default=24,
                        help="Number of electrodes. [24]")
    parser.add_argument("-m", "--mesh", 
                        dest="mesh", metavar="File", default=None,
                        help="Take the electrode positions from this mesh. Will overwrite -n option.")
    parser.add_argument("-s", "--spacing", 
                        dest="spacing", type=float, default=1,
                        help="Spacing between electrodes in meter [1]")
    parser.add_argument("-A", "--addInverse", 
                        dest="addinverse", action="store_true",
                        help="Add inverse value to create a full dataset.[0].")
    parser.add_argument("--scheme",
                        dest="scheme", default="none",
                        help="Array configuration. Select default to show all supported configurations.")
    
    options=parser.parse_args()

    if options.verbose: print(options)

    #strange!!!!!! plt.figure() overwrites our locale settings to the system default.
    pg.checkAndFixLocaleDecimal_point(options.verbose)   

    fileName = options.outFileName
    
    if not '.' in options.outFileName:
        fileName += '.shm'

    elecs = options.nElecs
    if options.mesh:
        mesh = pg.Mesh(options.mesh)
        if options.verbose:
            print(mesh)
        elecs = mesh.findNodesIdxByMarker(-99)
    
    data = pybert.createData(elecs,
                             options.scheme,
                             inverse=options.inverse,
                             addinverse=options.addinverse,
                             spacing=options.spacing)
    
    if options.verbose:
        print(data)
        print("writing: ", fileName)
        
    data.save(fileName, "a b m n")
    

if __name__ == "__main__":
    main()