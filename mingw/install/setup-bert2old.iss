; Script generated by the Inno Setup Script Wizard.
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING INNO SETUP SCRIPT FILES!

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{E2321A8B-337F-4E57-855C-7DE52790AB11}
AppName=BERT 2.0RC7
AppVerName=BERT 2.0beta7(build101026)
AppPublisher=resistivity.net
AppPublisherURL=http://www.resistivity.net
AppSupportURL=http://www.resistivity.net
AppUpdatesURL=http://www.resistivity.net
DefaultDirName={pf}\BERT
DefaultGroupName=BERT2
OutputDir=.
OutputBaseFilename=setup-bert2-beta7
SetupIconFile=D:\Guenther.T\src\bert\trunk\install\resnet.ico
Compression=lzma
SolidCompression=yes
PrivilegesRequired=none

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Files]
Source: "D:\Guenther.T\src\bert\trunk\doc\publist.txt"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\Guenther.T\src\bert\trunk\doc\bert12\bert1-2-migration.pdf"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\Guenther.T\src\bert\trunk\doc\poster\poster-bert2.pdf"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\Guenther.T\src\gimli\trunk\apps\bert\bertopts.cfg"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\Guenther.T\src\gimli\trunk\apps\bert\bert"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\Guenther.T\src\gimli\trunk\apps\bert\bertInfo"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\Guenther.T\src\gimli\trunk\apps\bert\bertNew2D"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\Guenther.T\src\gimli\trunk\apps\bert\bertNew2DCirc"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\Guenther.T\src\gimli\trunk\apps\bert\bertNew2DTopo"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\Guenther.T\src\gimli\trunk\apps\bert\bertNew3D"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\Guenther.T\src\gimli\trunk\apps\bert\bertNew3DCyl"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\Guenther.T\src\gimli\trunk\apps\bert\bertNew3DTopo"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\Guenther.T\src\gimli\trunk\mingw\dcmod.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\Guenther.T\src\gimli\trunk\mingw\dcedit.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\Guenther.T\src\gimli\trunk\mingw\dcinv.exe"; DestDir: "{app}"; Flags: ignoreversion
; gimli stuff
;Source: "D:\Guenther.T\src\gimli\branches\pygimli\pygimli\libgimli.dll"; DestDir: "{app}\pygimli"; Flags: ignoreversion
Source: "D:\Guenther.T\src\gimli\branches\pygimli\pygimli\__init__.py"; DestDir: "{app}\pygimli"; Flags: ignoreversion
Source: "D:\Guenther.T\src\gimli\branches\pygimli\pygimli\_pygimli_.pyd"; DestDir: "{app}\pygimli"; Flags: ignoreversion
Source: "D:\Guenther.T\src\gimli\branches\pygimli\pygimli\libboost_python-mt.dll"; DestDir: "{app}\pygimli"; Flags: ignoreversion
Source: "D:\Guenther.T\src\gimli\branches\pygimli\pygimli\curvefit\*.py"; DestDir: "{app}\pygimli\curvefit"; Flags: ignoreversion
Source: "D:\Guenther.T\src\gimli\branches\pygimli\pygimli\misc\*.py"; DestDir: "{app}\pygimli\misc"; Flags: ignoreversion
Source: "D:\Guenther.T\src\gimli\branches\pygimli\pygimli\mplviewer\*.py"; DestDir: "{app}\pygimli\mplviewer"; Flags: ignoreversion
Source: "D:\Guenther.T\src\gimli\branches\pygimli\pygimli\paratools\*.py"; DestDir: "{app}\pygimli\paratools"; Flags: ignoreversion
Source: "D:\Guenther.T\src\gimli\branches\pygimli\pygimli\utils\*.py"; DestDir: "{app}\pygimli\utils"; Flags: ignoreversion
Source: "D:\Guenther.T\src\gimli\branches\pygimli\pygimli\viewer\*.py"; DestDir: "{app}\pygimli\viewer"; Flags: ignoreversion
Source: "D:\Guenther.T\src\gimli\branches\pygimli\pyTools\*.py"; DestDir: "{app}"; Flags: ignoreversion
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
Name: "{group}\{cm:UninstallProgram,BERT 2.0 beta7}"; Filename: "{uninstallexe}"

